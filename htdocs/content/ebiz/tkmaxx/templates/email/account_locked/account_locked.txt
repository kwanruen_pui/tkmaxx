================================

Oops!, locked out?

================================



<venda_inctemplate name=textEmailHeader2,type=includes>

Your email Address: <venda_block label=1,usemail=notnull><venda_usemail></venda_block label=1><venda_block label=2,usemail=null,uslogin=notnull><venda_uslogin></venda_block label=2>


Sorry, because of your recent failed attempts to log in, your TK Maxx account has been locked.

But don't worry, you'll soon be logged in and shopping again. All you have to do is request a password reminder at <venda_serveradd>/password-reset&bsref=<venda_ebizref> and we'll re-send your details immediately and unlock your account. It's as simple as that!


If you have any queries, get in touch with Customer Services by emailing customerservice@tjxeurope.com.





Best wishes,



The TK Maxx Team
