// Used for checkout and my account.
jQuery('body').bind('contact-loaded', function () {
    jQuery("#contactForm").validate({
        rules: {
            field1: {
                required: true,
                email: true
            },
            field2: {
                required: true
            },
            field3: {
                required: true
            }
        },
        messages: {
            field1: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            },
            field2: {
                required: Venda.Validate.msg.nametxt
            },
            field3: Venda.Validate.msg.message
        },
        submitHandler: function () {
            Venda.Ebiz.submitFormModal('form');
        }
    });
});

jQuery(function () {

    jQuery('form[name=emailsonly1],#emailpasswordresetform').validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            }
        }
    });

    jQuery('#form-passwordreset').validate({
        rules: {
            email   : {
                required: true,
                email: true
            }
        },
        messages: {
            email   : {
                required    : Venda.Validate.msg.email,
                email       : Venda.Validate.msg.email_vaild
            }
        },
        submitHandler: function(form) {
            Venda.Checkout.passwordReset();
        }
    });

    jQuery('#passwordresetform').validate({
        rules: {
            password  : {
                required: true,
                minlength: 5,
                maxlength: 10
            },
            password2 : {
                required: true,
                minlength: 5,
                maxlength: 10,
                equalTo: '#password'
            }
        },
        messages: {
            uspswd  : {
                    required: Venda.Validate.msg.password,
                    minlength: Venda.Validate.msg.verify_password_minlength,
                    maxlength: Venda.Validate.msg.verify_password_maxlength
            },
            uspswd2 : {
                    required: Venda.Validate.msg.confirm_password,
                    equalTo: Venda.Validate.msg.verify_password_match
            }
        }
    });

    jQuery('#formsolrsearch').validate({
        rules: {
            q: {
                required: true
            }
        },
        messages: {
            q: {
                required: Venda.Validate.msg.search
            }
        },
        errorPlacement: function (error, element) {}
    });

    jQuery('form[name=multipledeliveryaddressesform]').validate({
        errorPlacement: function (error, element) {}
    });

    jQuery('form[name=shopcartform]').validate({
        errorPlacement: function (error, element) {}
    });

    jQuery("form[name=ordersummaryform-giftcert]").validate({
        rules: {
            giftcode: {
                required: true
            }
        },
        messages: {
            giftcode: Venda.Validate.msg.giftcert_code
        },
        submitHandler: function (form) {
            Venda.Checkout.manualsubmit(form, jQuery('#js-giftcode-waitMsg').text());
        }
    });

    jQuery('form[name=promotionform]').validate({
        rules: {
            vcode: {
                required: true,
                specialChars: true
            }
        },
        messages: {
            vcode: {
                required: Venda.Validate.msg.promo_code
            }
        },
        submitHandler: function () {
            Venda.Widget.Promotion.checkVoucherForm('add');
        }
    });

    jQuery('#existingcustomer').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            },
            password: Venda.Validate.msg.password
        }
    });

    jQuery('#reminderform').validate({
        rules: {
            usemail: {
                required: true,
                email: true
            }
        },
        messages: {
            usemail: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            }
        }
    });

    jQuery('#dtsform').validate({
        rules: {
            fname: {
                required: true,
                maxlength: 30,
                ValidateInput: true
            },
            lname: {
                required: true,
                maxlength: 30,
                ValidateInput: true
            }
        },
        messages: {
            fname: {
                required: Venda.Validate.msg.fname
            },
            lname: {
                required: Venda.Validate.msg.lname
            }
        }
    });

    jQuery('form[name=billingaddressaddform],#tl_user-myform form,form[name=addressbookform],form[name=billingaddresseditform],form[name=deliveryaddressaddform],form[name=deliveryaddresseditform]').validate({
        rules: {
            title: {
                required: true
            },
            fname: {
                required: true,
                maxlength: 30,
                onlyChars: true
            },
            lname: {
                required: true,
                maxlength: 30,
                onlyChars: true
            },
            cntry: {
                required: true
            },
            company: {
                ValidateInput: true
            },
            num: {
                required: true,
                ValidateInput: true
            },
            addr1: {
                required: true,
                ValidateInput: true
            },
            addr2: {
                ValidateInput: true
            },
            city: {
                required: true,
                ValidateInput: true
            },
            state: {
                required: true,
                ValidateInput: true
            },
            zipc: {
                required: true,
                findaddress: true,
                ValidateInput: true,
                ukpostcode: (jQuery("#cntrylist").val()==="United Kingdom")?true:false,
            },
            zcdropdown: {
                required: true
            },
            phone: {
                required: true,
                phonenumber: true,
                maxlength: 20
            },
            usxtmobile: {
                phonenumber: true,
                maxlength: 20
            },
            usemail: {
                required: true,
                email: true,
                ValidatePipe: true
            },
            uspswd: {
                required: true,
                minlength: 5,
                maxlength: 16
            },
            uspswd2: {
                required: true,
                minlength: 5,
                maxlength: 16,
                equalTo: '#uspswd'
            },
            uspswd0: {
                required: true,
                minlength: 5,
                maxlength: 16
            }
        },
        messages: {
            title: {
                required: Venda.Validate.msg.title
            },
            fname: {
                required: Venda.Validate.msg.fname
            },
            lname: {
                required: Venda.Validate.msg.lname
            },
            cntry: {
                required: Venda.Validate.msg.country
            },
            addr1: {
                required: Venda.Validate.msg.addr1
            },
            city: {
                required: Venda.Validate.msg.city
            },
            state: {
                required: Venda.Validate.msg.state
            },
            zipc: {
                required: Venda.Validate.msg.postcode,
                findaddress: Venda.Validate.msg.postcode_populate,
                ukpostcode: Venda.Validate.msg.postcode_lookupnotfound
            },
            zcdropdown: {
                required: Venda.Validate.msg.postcode_dropdown
            },
            phone: {
                required: Venda.Validate.msg.phone,
                phonenumber: Venda.Validate.msg.phone_valid
            },
            usxtmobile: {
                phonenumber: Venda.Validate.msg.phone_valid
            },
            usemail: {
                required: Venda.Validate.msg.email
            },
            uspswd: {
                required: Venda.Validate.msg.password,
                minlength: Venda.Validate.msg.verify_password_minlength,
                maxlength: Venda.Validate.msg.verify_password_maxlength
            },
            uspswd2: {
                required: Venda.Validate.msg.confirm_password,
                equalTo: Venda.Validate.msg.verify_password_match
            },
            uspswd0: {
                required: Venda.Validate.msg.verify_current_password
            }
        },
        errorPlacement: Venda.Validate.errorPlacement,
        highlight: function(element, inputfield) {
            //remove green mark sign
            $(element)
                .closest('.columns')
                .removeClass('successField')
                .addClass('errorField');
        },
        success: function (element, inputfield) {
            //add green mark sign
            if ($(inputfield).val().length > 0) {
                $(element)
                    .closest('.columns')
                    .removeClass('errorField')
                    .addClass('successField');
            } else {
                //remove green mark sign
                $(element)
                    .closest('.columns')
                    .removeClass('successField')
                    .addClass('errorField');
            }
        }
    });

    if (jQuery('form[name=ordersummaryform]').length > 0) {

        Venda.Validate.paytypes();
        Venda.Validate.storeInfoClickCollect();

        jQuery('input[name=payment_id], input[name=ohpaytype]').click(function () {
            Venda.Validate.paytypes();
        });
    }

    if (jQuery('form[name=storelocatorform]').length > 0) {

        Venda.Validate.storelocator();

        jQuery('#storelocatorform').on('click', '#pcsubmit', function () {
            Venda.Validate.storelocator();
        });
    }

    if (jQuery('form[name=giftwrappingform]').length > 0) {
        jQuery('form[name=giftwrappingform]').validate(); //sets up the validator
        jQuery('form[name=giftwrappingform] textarea[name*=cm-]').each(function () {
            jQuery(this).rules('add', {
                rangelength: [0, 80],
                charactersCount: [80, '.js-textMsgCount-' + jQuery(this).data('oirfnbr')]
            });
        });
    }

    jQuery('form[name=ordersummaryinstoreform]').validate({
        rules: {
            orxorxteposref: {
                required: true
            }
        }
    });
});

Venda.Validate.storelocator = function () {
    var $this = jQuery('#address');
    if ($this.val() === $this.attr('placeholder')) { $this.val(''); }
    jQuery('form[name=storelocatorform]').validate({
        rules: {
            address: {
                required: true
            }
        }
    });
};

Venda.Validate.storeInfoClickCollect = function() {
    if (jQuery('#storefree_0').is(':checked') === true) {
        jQuery('form[name=storeInfo]').validate({
            rules: {
                storename: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false
                },
                orxorxtcusfname: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false,
                    specialChars: true
                },
                orxorxtcuslname: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false,
                    specialChars: true
                },
                orxorxtcusphone: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false,
                    phonenumber: true
                }
            },
            messages: {
                storename: {
                    required: Venda.Validate.msg.storename
                },
                orxorxtcusfname: {
                    required: Venda.Validate.msg.fname
                },
                orxorxtcuslname: {
                    required: Venda.Validate.msg.lname
                },
                orxorxtcusphone: {
                    required: Venda.Validate.msg.phone,
                    phonenumber: Venda.Validate.msg.phone_valid
                }
            },
            errorPlacement: Venda.Validate.errorPlacement
        });
    }
};

Venda.Validate.paytypes = function () {
    if (jQuery('#creditcard').is(':checked') === true) {
        jQuery('form[name=ordersummaryform]').data('validator', null); // Clear rules
        jQuery('form[name=ordersummaryform]').validate({
            rules: {
                ohccnum: {
                    required: true,
                    CCNumber: true
                },
                ohccname: {
                    required: true,
                    onlyChars: true
                },
                month: {
                    required: true
                },
                year: {
                    required: true,
                    cardexpiry: true
                },
                ohcccsc: {
                    required: true,
                    number: true,
                    rangelength: [3, 4]
                },
                storename: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false
                },
                orxorxtcusfname: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false,
                    specialChars: true
                },
                orxorxtcuslname: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false,
                    specialChars: true
                },
                orxorxtcusphone: {
                    required: (jQuery("#storefree_0").is(":checked"))?true:false,
                    phonenumber: true
                }
            },
            groups: {
                expirydate: 'month year'
            },
            messages: {
                ohccnum: {
                    required: Venda.Validate.msg.credit_card_number,
                    CCNumber: Venda.Validate.msg.credit_card_number
                },
                ohccname: Venda.Validate.msg.credit_card_name,
                month: {
                    required: Venda.Validate.msg.credit_card_expired,
                    min: Venda.Validate.msg.credit_card_expired
                },
                year: {
                    required: Venda.Validate.msg.credit_card_expired,
                    cardexpiry: Venda.Validate.msg.credit_card_expired
                },
                ohcccsc: {
                    required: Venda.Validate.msg.security_code,
                    rangelength: Venda.Validate.msg.security_code_length
                },
                storename: {
                    required: Venda.Validate.msg.storename
                },
                orxorxtcusfname: {
                    required: Venda.Validate.msg.fname
                },
                orxorxtcuslname: {
                    required: Venda.Validate.msg.lname
                },
                orxorxtcusphone: {
                    required: Venda.Validate.msg.phone,
                    phonenumber: Venda.Validate.msg.phone_valid
                }
            },
            errorPlacement: Venda.Validate.errorPlacement,
            submitHandler: function (form) {
                if (!jQuery(form).is('.submitted')) {
                    jQuery(form).addClass('submitted');
                    form.submit();
                }
            }
        });

        jQuery('form[name=ordersummaryform]').validate().currentForm = jQuery('form[name=ordersummaryform]')[0];

    } else if (jQuery('#giftcardpayment').is(':checked') === true) {

        jQuery('form[name=ordersummaryform]').data('validator', null); // Clear rules
        jQuery('form[name=ordersummaryform]').validate({
            rules: {
                card_id: {
                    required: true,
                    number: true
                },
                security_code: {
                    required: true,
                    number: true
                },
                payment_amount: {
                    required: true,
                    number: true
                }
            },
            messages: {
                card_id: {
                    required: Venda.Validate.msg.giftcard_id
                },
                security_code: {
                    required: Venda.Validate.msg.giftcard_code
                },
                payment_amount: {
                    required: Venda.Validate.msg.giftcard_amount
                }
            },
            errorPlacement: Venda.Validate.errorPlacement
        });

        jQuery('form[name=ordersummaryform]').validate().currentForm = jQuery('form[name=ordersummaryform]')[0];

    } else if (jQuery('#bankpayment').is(':checked') === true) {

        jQuery('form[name=ordersummaryform]').data('validator', null); // Clear rules
        jQuery('form[name=ordersummaryform]').validate({
            rules: {
                accountnumber: {
                    required: true,
                    number: true
                },
                accountname: {
                    required: true
                },
                sortcode: {
                    required: true,
                    number: true
                }
            },
            messages: {
                accountnumber: {
                    required: Venda.Validate.msg.bank_accountnumber
                },
                accountname: {
                    required: Venda.Validate.msg.bank_accountname
                },
                sortcode: {
                    required: Venda.Validate.msg.bank_sortcode
                }
            },
            errorPlacement: Venda.Validate.errorPlacement
        });

        jQuery('form[name=ordersummaryform]').validate().currentForm = jQuery('form[name=ordersummaryform]')[0];
    } else {
        var validatedform = jQuery('form[name=ordersummaryform]').validate();
        validatedform.resetForm();
        validatedform.currentForm = '';
    }
};
