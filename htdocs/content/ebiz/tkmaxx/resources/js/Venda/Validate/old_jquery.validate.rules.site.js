// used for non-workflow and my account forms

jQuery(function () {

    jQuery('form[name=emailsonly1]').validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            }
        }
    });


    jQuery('#formsolrsearch').validate({
        rules: {
            q: {
                required: true,
                searchinput: true
            }
        },
        messages: {
            q: {
                required: Venda.Validate.msg.search,
                searchinput: Venda.Validate.msg.search_valid
            }
        },
        errorPlacement: function (error, element) {
            //jQuery('.js-searchMessage').text(jQuery(error).text());
            jQuery('.js-searchMessage').append(error);
        }
    });

    jQuery('#contactForm').validate({
        rules: {
            field1: {
                required: true,
                email: true
            },
            field2: {
                required: true
            },
            field3: {
                required: true
            }
        },
        messages: {
            field1: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            },
            field2: {
                required: Venda.Validate.msg.nametxt
            },
            field3: Venda.Validate.msg.message
        }
    });

    /* Coming Soon */
    $.validator.addMethod('require-one', function (value) {
        return $('.require-one:checked').size() > 0;
    }, 'Please check at least one box.');

    var checkboxes = $('.require-one');
    var checkbox_names = $.map(checkboxes, function(e,i) { return $(e).attr("name")}).join(" ");
    jQuery('form[name=comingsoon]').validate({
        groups: {
            checks: checkbox_names
        },
        rules: {
            usemail: {
                required: true,
                email: true
            },
            confirmmail: {
                required: true,
                email: true,
                equalTo: '#usemail'
            },
            fname: {
                required: true,
                maxlength: 30,
                ValidateInput: true
            },
            lname: {
                required: true,
                maxlength: 30,
                ValidateInput: true
            },
            cntry: {
                required: true
            },
            zipc: {
                ukpostcode: (jQuery("#cntrylist").val()==="United Kingdom")?true:false,
                required: true
            }
        },
        messages: {
            usemail: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            },
            confirmmail: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild,
                equalTo: Venda.Validate.msg.verify_email
            },
            fname: {
                required: Venda.Validate.msg.fname
            },
            lname: {
                required: Venda.Validate.msg.lname
            },
            cntry: {
                required: Venda.Validate.msg.country
            },
            zipc: {
                required: Venda.Validate.msg.postcode,
                ukpostcode: Venda.Validate.msg.postcode_lookupnotfound
            }
        },
            errorPlacement: Venda.Validate.errorPlacement
    });

    jQuery('#giftcertificatesform').validate({
        rules: {
            to: {
                specialChars: true,
                required: true,
                maxlength: 30
            },
            from: {
                required: true,
                maxlength: 30
            },
            email: {
                required: true,
                email: true
            },
            comment: {
                required: true,
                rangelength: [0, 250],
                trimtext: [250]
            },
            amount: {
                required: true,
                number: true,
                range: [jQuery('#tag-invtatrminsubsell').text(), jQuery('#tag-invtatrmaxsubsell').text()],
                maxlength: 10
            }
        },
        messages: {
            to: {
                specialChars: "Field must not contain extended characters",
                required: Venda.Validate.msg.to,
                maxlength: jQuery.format(Venda.Validate.msg.max_length + " {0}")
            },
            from: {
                required: Venda.Validate.msg.nametxt,
                maxlength: jQuery.format(Venda.Validate.msg.max_length + " {0}")
            },
            email: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            },
            comment: {
                rangelength: jQuery.format(Venda.Validate.msg.least + " {0} " + Venda.Validate.msg.least_two),
            },
            amount: {
                required: Venda.Validate.msg.giftcert_amount,
                range: jQuery.format("Mininmum value is {0} Maximum value is {1}"),
                maxlength: jQuery.format(Venda.Validate.msg.max_length + " {0}")
            }
        }
    });

    jQuery('#addproductform').validate({
        errorPlacement: function (error, element) {}
    });

    if (jQuery('form[name=storelocatorform]').length > 0) {

        Venda.Validate.storelocator();

        jQuery('#storelocatorform').on('click', '#pcsubmit', function () {
            Venda.Validate.storelocator();
        });
    }
});

Venda.Validate.storelocator = function () {
    var $this = jQuery('#address');
    if ($this.val() === $this.attr('placeholder')) {$this.val(''); }
};

jQuery('body').bind('quickview-loaded', function () {
    jQuery("#addproductform").validate({

    });
});

jQuery('body').bind('emailmebackform-loaded', function () {

    jQuery('#emailmebackform').validate({
        rules: {
            bisemail: {
                required: true,
                email: true
            }
        },
        messages: {
            bisemail: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            }
        },
        submitHandler: function () {
            Venda.Ebiz.ExecuteDialogOpen();
        }
    });
});

jQuery('body').bind('writereviewform-loaded', function () {

    jQuery("#writereviewform").validate({
        rules: {
            field1: {
                specialChars: true,
                required: true,
                maxlength: 150
            },
            from: {
                required: true,
                email: true
            },
            field2: {
                required: true,
                phonenumber: true
            },
            field6: {
                required: true
            },
            field3: {
                required: true
            }
        },
        messages: {
            field1: {
                specialChars: "Field must not contain extended characters",
                required: Venda.Validate.msg.nametxt,
                maxlength: jQuery.format(Venda.Validate.msg.max_length + " {0}")
            },
            from  : {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            },
            field2: {
                required: Venda.Validate.msg.phone,
                phonenumber: Venda.Validate.msg.phone_valid
            },
            field6: Venda.Validate.msg.rank,
            field3: Venda.Validate.msg.review
        },
        submitHandler: function () {
            Venda.Ebiz.ExecuteDialogOpen();
        },
        errorPlacement: Venda.Validate.errorPlacement
    });
});

jQuery('body').bind('tellafriendform-loaded', function () {

    jQuery('#tellafriendform').validate({
        rules: {
            fname: {
                required: true,
                maxlength: 30,
                ValidateInput: true
            },
            name: {
                required: true,
                maxlength: 30,
                ValidateInput: true
            },
            email: {
                required: true,
                email: true
            },
            field1: {
                required: true,
                maxlength: 100,
                charactersCount: [100, '.js-textMsgCount-' + jQuery('#field1').data('textmsg')]
            }
        },
        messages: {
            fname: {
                required: Venda.Validate.msg.name,
                maxlength: jQuery.format(Venda.Validate.msg.max_length + " {0}")
            },
            name: {
                required: Venda.Validate.msg.to,
                maxlength: jQuery.format(Venda.Validate.msg.max_length + " {0}")
            },
            email: {
                required: Venda.Validate.msg.email,
                email: Venda.Validate.msg.email_vaild
            },
            field1: jQuery.format(Venda.Validate.msg.max_length + " {0}")
        },
        submitHandler: function () {
            Venda.Ebiz.ExecuteDialogOpen();
        }
    });
});
