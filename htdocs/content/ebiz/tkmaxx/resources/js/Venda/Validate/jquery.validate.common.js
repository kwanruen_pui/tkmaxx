/*jshint bitwise: true */
/*jshint browser: true */
/*jshint camelcase: false */
/*jshint curly: true */
/*jshint eqeqeq: true */
/*jshint expr: true */
/*jshint immed: false */
/*jshint indent: 2 */
/*jshint latedef: true */
/*jshint laxbreak: true */
/*jshint noarg: true */
/*jshint plusplus: false */
/*jshint quotmark: single */
/*jshint undef: true */
/*jshint unused: true */
/*jshint trailing: true */

(function (Venda, $) {
    'use strict';

    /**
     * Set up the namespace.
     */
    Venda.namespace('Validate');

    $.validator.setDefaults({
        'errorElement': 'span',
        'errorClass': 'js-validateError',
        'ignore': 'input[type=hidden],.ignore',
        'onfocusin': false
      });

    Venda.Validate.msg = {
        'title': $('#tag-validation_title').text(),
        'fname': $('#tag-validation_fname').text(),
        'lname': $('#tag-validation_lname').text(),
        'giftcert_amount': $('#tag-validation_giftcert_amount').text(),
        'country': $('#tag-validation_country').text(),
        'house_num': $('#tag-validation_house_num').text(),
        'addr1': $('#tag-validation_addr1').text(),
        'city': $('#tag-validation_city').text(),
        'state': $('#tag-validation_state').text(),
        'county': $('#tag-validation_county').text(),
        'postcode': $('#tag-validation_postcode').text(),
        'postcode_populate': $('#tag-validation_postcode_populate').text(),
        'postcode_dropdown': $('#tag-validation_postcode_dropdown').text(),
        'postcode_lookupnotfound': $('#tag-validation_postcode_lookupnotfound').text(),
        'phone': $('#tag-validation_phone').text(),
        'phone_valid': $('#tag-validation_phone_valid').text(),
        'email': $('#tag-validation_email').text(),
        'email_vaild': $('#tag-validation_email_vaild').text(),
        'password': $('#tag-validation_password').text(),
        'verify_password_length': $('#tag-validation_verify_password_length').text(),
        'confirm_password': $('#tag-validation_confirm_password').text(),
        'verify_password_match': $('#tag-validation_verify_password_match').text(),
        'verify_current_password': $('#tag-validation_verify_current_password').text(),
        'credit_card_number': $('#tag-validation_credit_card_number').text(),
        'credit_card_name': $('#tag-validation_credit_card_name').text(),
        'security_code': $('#tag-validation_security_code').text(),
        'security_code_length': $('#tag-validation_security_code_length').text(),
        'credit_card_expired': $('#tag-validation_credit_card_expired').text(),
        'datenow': $('#tag-validation_datenow').text(),
        'giftcard_id': $('#tag-validation_giftcard_id').text(),
        'giftcard_code': $('#tag-validation_giftcard_code').text(),
        'giftcard_amount': $('#tag-validation_giftcard_amount').text(),
        'bank_accountnumber': $('#tag-validation_bank_accountnumber').text(),
        'bank_accountname': $('#tag-validation_bank_accountname').text(),
        'bank_sortcode': $('#tag-validation_bank_sortcode').text(),
        'search': $('#tag-validation_search').text(),
        'nametxt': $('#tag-validation_name').text(),
        'message': $('#tag-validation_message').text(),
        'to': $('#tag-validation_to').text(),
        'from': $('#tag-validation_from').text(),
        'max_length': $('#tag-validation_maxLength').text(),
        'min_length': $('#tag-validation_minLength').text(),
        'max_value': $('#tag-validation_maxValue').text(),
        'min_value': $('#tag-validation_minValue').text(),
        'rank': $('#tag-validation_rank').text(),
        'review': $('#tag-validation_review').text(),
        'least': $('#tag-validation_least').text(),
        'least_two': $('#tag-validation_least_two').text(),
        'giftcert_code': $('#tag-validation_giftcert_code').text(),
        'promo_code': $('#tag-validation_promo_code').text(),
        'qty': $('#tag-validation_qty').text(),
        'specialChars': $('#tag-validation_special_characters').text(),
        'verify_email': $('#tag-validation_verify_email').text(),
        'newsletter_checkboxes': $('#tag-validation_newsletter_checkboxes').text(),
        'search_valid': $('#tag-validation_search_valid').text(),
        'storename': $('#tag-validation_storename').text(),
        'onlyChars': $('#tag-validation_only_characters').text()
    };

    $.validator.addClassRules('js-qty', {
        required: true,
        number: true,
        digits: true,
        qty: true
    });

    /**
     * A method that matches quantity rules
     * Basket page - allow enter 0 to clear all items
     * Product page - not allow 0
     */
    $.validator.addMethod('qty', function (value) {
        var regx;
        regx = new RegExp(/(^-?[1-9](\d{1,2}(\,\d{3})*|\d*)|^0{1})$/);

        if ($('#tag-workflow').text() === 'shopcart') {
            return (regx.test(value) === false) ? false : true;
        }
        return (parseInt(value, 10) <= 0 || regx.test(value) === false) ? false : true;

    }, Venda.Validate.msg.qty);

    /**
     * A method to trim characters
     */
    $.validator.addMethod('trimtext', function (value, element, params) {
        var max = params[0];
        $(element).keypress(function() {
            if ($("#comment").val().length > max) {
            // trim if too long
                $("#comment").val($("#comment").val().substring(0, max));
            }
        });

        return true;
    });

    /**
     * A method that matches searchinput rules
     * Do not allow searched by 0 - +
     */
    $.validator.addMethod('searchinput', function (value) {
        if (value.length === 1) {
            return ((value === '0') || (value === '-') || (value === '+') || (value === '<javascript>')) ? false : true;
        } else {
            return (value === '<javascript>') ? false : true;
        }
        return true;
    });

    /**
     * A method that matches phone rules
     * Allow number and spaces
     */
    // $.validator.addMethod('phonenumber', function (value) {
    //     var regx;
    //     regx = new RegExp(/^[0-9 ]+$/);

    //     return regx.test(value);
    // });

    $.validator.addMethod('CCNumber', function (value) {
        var regx;
        regx = new RegExp(/^[* 0-9\b]+$/);
        return regx.test(value) ;
    });

    jQuery.validator.addMethod('phonenumber', function(phone_number, element) {
        phone_number = phone_number.replace(/\s+|-/g,'');
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^(?:(?:(?:00\s?|\+)44\s?|0)(?:1\d{8,9}|[23]\d{9}|7(?:[45789]\d{8}|624\d{6})))$/);
    }, 'Please specify a valid uk phone number');

    /**
     * A method that matches special characters rules
     * Do not allow any special characters
     */
    $.validator.addMethod('specialChars', function (value) {
        var regx;
        regx = new RegExp(/^[a-zA-Z0-9'. -]+$/);

        return regx.test(value);

    }, Venda.Validate.msg.specialChars);

    /* Allow only alphabet */
    $.validator.addMethod('onlyChars', function(value){
        var regx = new RegExp(/^[a-zA-Z'. -]+$/);
        return regx.test(value);
    }, Venda.Validate.msg.onlyChars);

    // FirstName, LastName, AddressLine1 validation in jQuery using Regular Expressions
    // Now including Company, Addres Line 2, Postcode
    jQuery.validator.addMethod("ValidateInput", function(value) {

        var regx = new RegExp("^$|^[a-zA-Z0-9\d'. -]+$");

        return regx.test(value);

    }, "Field must not contain extended characters");

    // Validate pipe sign
    jQuery.validator.addMethod("ValidatePipe", function(value) {

        var regx = new RegExp("\\|");

        return !regx.test(value);

    }, "Field must not contain pipe sign");

    /**
     * A method that matches card expiry date rules
     * The date must be the future date
     */
    $.validator.addMethod('cardexpiry', function () {
        var monthObj, monthVal, yearVal;
        monthObj =  $('#month');
        monthVal =  monthObj.val();
        yearVal =  $('#year').val();

        if ((Venda.Validate.msg.datenow.split('/')[2] !== yearVal) && (monthVal !== '')) {
            monthObj.rules('remove', 'min');
            return true;
        }
        monthObj.rules('add', { min: parseFloat(Venda.Validate.msg.datenow.split('/')[0]) });
        return (monthVal < Venda.Validate.msg.datenow.split('/')[0]) ? false : true;
    });

    /**
     * A method that matches findaddress rules
     * If using UK postcode lookup
     */
    $.validator.addMethod('findaddress', function () {
        if ($('select[name=zcdropdown]').length === 0 && $('#js-lookup-submit-btn').is(':visible')) {
            return false;
        }
        return true; //validation rule is ok (passed) - true
    });

    /**
     * A method that matches custom alert rules
     *
     */
    $.validator.addMethod('customAlert', function (value) {
        if (value === '') {
            $('.js-validationMessage').addClass('js-active').slideDown('slow');
            return false;
        }
        $('.js-validationMessage').slideUp().removeClass('js-active');
        return true;
    });

    /**
     * A method that matches characters count rules
     * Calculate and display the number of characters remaining
     */
    $.validator.addMethod('charactersCount', function (value, element, params) {
        var characterLimit, charactersUsed, charactersRemaining, $elem;
        characterLimit = params[0];
        charactersUsed = value.length;
        $elem = $(params[1]);

        if (charactersUsed > characterLimit) {
            charactersUsed = characterLimit;
            $(element).val(value.substr(0, characterLimit));
        }
        charactersRemaining = characterLimit - charactersUsed;
        $elem.html(charactersRemaining);

        return true;
    });

    var validatePostcode = true;
    $('input[name="zipc"]').on('focusout', function(){
        var zipc = $(this);
        zipc.after('<i class="icon-spinner icon-spin zipcode-loading"></i>');

        $.ajax ({
            dataType: 'json',
            url: '/ajax/postcode?country=UK&postcode=' + zipc.val()
        })
        .done(function (data) {
            if (data.errors && data.errors[0].key === 'postcode_not_found') {
                validatePostcode = false;
                 zipc.parents('form').validate().element(zipc);
            } else {
                validatePostcode = true;
                 zipc.parents('form').validate().element(zipc);
            };
            zipc.parent().find('i.icon-spinner').remove();
        })
        .fail(function () {
           validatePostcode = false;
           zipc.parent().find('i.icon-spinner').remove();
        });
    });

    /**
     * A method that matches UK postcode
     * with some custom patterns
     */
    $.validator.addMethod('ukpostcode', function (value, element, params) {
        return validatePostcode;
    });


    /**
     * A function that specific errorPlacement
     * To control where the group message is placed
     */
    Venda.Validate.errorPlacement = function (error, element) {
        if (element.is('select')) {
            if (element.is('[name="month"]') || element.is('[name="year"]')) {
                $('#expiryshow').after(error);
                $('.js-changecard:visible').trigger('click');
            } else {
                error.insertAfter($(element).next().last());
            }
        }
        else if (element.attr('type')=='checkbox') {
            error.addClass("checkbox-error").insertAfter(".require-one");
        }
        else {
            error.insertAfter(element);
        }
    };

    // Set up the DOM events.
    $(function () {
        // Removes the HTML5 required attribute so $ inline validation is used consistently
        $('[required]').removeAttr('required');
    });

}(Venda, jQuery));
