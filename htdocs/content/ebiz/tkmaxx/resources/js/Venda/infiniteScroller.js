/* global History:true */
/**
 * Path /content/ebiz/shop/resources/js/Venda/infiniteScroller.js
 * This file enables the infinite scrolling in product search section
 */

Venda.namespace('Venda.Scroll');

var is_loading = 0, load = true,
    contentDisplayType = false,
    next_url = $('.pagn-next').length > 0
        ? $('.pagn-next a').attr('href')
        : 'end',
    product_per_load = 12,
    arr = next_url.slice(next_url.indexOf('?') + 1).split('&'),
    scrollTopHTML = [
        '<div class="js-scroll-to-top hide">',
        '<div class="fs1 icon-3x" aria-hidden="true" data-icon="&#xe031;">',
        '</div></div>'
    ].join(''),
    rightMargin = parseFloat($('.wrapper').css('margin-right')),

    // Activate pages limit, after how many pages to stop scroll and use load more button and use slide top icon
    pagesLimit = true, pagesToLoad = 4, slideTop = true;

Venda.Scroll.displayLoadingImage = function() {
    if ($('#tempLoading').length === 0) {
        $('<div id="tempLoading" class="js-loadingimg" style="height: 100px "></div>').insertAfter($('#js-list-scroll'));
    } else {
        $('#tempLoading').show();
    }
};

Venda.Scroll.changeState = function(u) {
    contentDisplayType = true;
    History.replaceState(null, null, u);
};

Venda.Scroll.loadAPage = function() {
    Venda.Scroll.displayLoadingImage();
    $.get(next_url, function(response) {
        $('#pagnTop').html($(response).find('#pagnTop').html());
        $('#pagnBtm').html($(response).find('#pagnBtm').html());
        $('#js-list-scroll').append($(response).find('#js-list-scroll').html());
        $('.js-scroll-hide, .js-scroll-hide .pagn-item-info').hide();
        Venda.Scroll.breakPages();
        if (next_url !== 'end') {
            $('.pagn-next').length > 0
                ? next_url = $('.pagn-next a').attr('href')
                : next_url = 'end';
        }
        $('#tempLoading').hide();
        is_loading = 0;
    });
};

Venda.Scroll.pageNumber = function(s, v) {
    var num;
    s === 'current'
        ? num = parseInt(parseInt(v, 10)-1, 10)
        : num = parseInt(v, 10);
    return num;
};

// Get page number
// pass 'current' or 'next' parameters to find the current page or next page numer
Venda.Scroll.getPageNumber = function(s){
    var uri = next_url.split('?')[0];
    var hash, num;
    var hashes = next_url.slice(next_url.indexOf('?') + 1).split('&');
    for(var i = 1; i < hashes.length; i++) {
         hash = hashes[i].split('=');
         if (hash[0] === 'setpagenum') {
            s === 'current'
                ? num = Venda.Scroll.pageNumber('current', hash[1])
                : num = Venda.Scroll.pageNumber('next', hash[1]);
         }
    }
    return num;
};

Venda.Scroll.toggle = function(e) {
    $(e).slideToggle();
};

// Display the load more button
Venda.Scroll.displayLoadMoreButton = function() {
    if ($('#js-load-more').is(':hidden')) {
        $('#js-load-more').show();
    }
};

Venda.Scroll.processLoading = function() {
    if (pagesLimit) {
        var number_of_pages = $('#js-list-scroll li').length/product_per_load;
        if(number_of_pages%pagesToLoad === 0) {
            Venda.Scroll.displayLoadMoreButton();
        } else  {
            Venda.Scroll.loadAPage();
        }
    } else {
        Venda.Scroll.loadAPage();
    }
};

Venda.Scroll.setCSS = function() {
    $('.js-scroll-to-top').css({
        'position': 'fixed',
        'bottom': '50px',
        'right': rightMargin + 'px',
        'background-color': '#666666',
        'color': 'white',
        'z-index': '1000',
        'cursor': 'pointer'
    });
};

$( document).on('click', '#js-load-more', function() {
    if (next_url !== 'end') {
        Venda.Scroll.loadAPage();
        if ($('#js-load-more').is(':visible')) {
            $('#js-load-more').hide();
        }
        return false;
    }
});

$( document).on('click', '.js-scroll-to-top', function() {
    $('html, body').animate(
        { scrollTop: '0px' },
        {
            complete : function() {
                Venda.Scroll.changeState($('.itemPage:first').attr('data-url'));
            }
        }
    );
});

// make sure that we are in the search template so we don't mess any other pages.
if ($('#searchResults').length > 0) {
    if ($('#js_useScroll').length > 0) {
        $('.js-scroll-hide, .js-scroll-hide .pagn-item-info').hide();

        $(document).on('mouseover', '.js-scroll-to-top', function(){
            $(this).animate({
              backgroundColor: '#454545',
              color: '#fff',
            }, 300 );
        });

        $(document).on('mouseleave', '.js-scroll-to-top', function(){
            $(this).animate({
              backgroundColor: '#666666',
              color: '#fff',
            }, 300 );
        });

        $(window).resize(function() {

          //resize just happened, pixels changed
          rightMargin = parseFloat($('.wrapper').css('margin-right'));
          Venda.Scroll.setCSS();
        });

        if (slideTop) {
            $('body').prepend(scrollTopHTML);
            Venda.Scroll.setCSS();
        }

        // how many products per page
        for(var i = 0; i < arr.length; i++){
            var cur = arr[i].split('=');
            if (cur[0] === 'perpage'){ product_per_load = cur[1];}

        }

        // add class and url attribute to the first page only if there are enough products
        if ($('.pagn-next').length > 0) {
            Venda.Scroll.firstPageURL = function() {

                // use next page to get the first page
                var UrlFirsPart = next_url.split('?')[0];
                var UrlSecPart = '';
                var vars = [], hash;
                var hashes = next_url.slice(next_url.indexOf('?') + 1).split('&');
                UrlSecPart = hashes[0];
                for(var i = 1; i < hashes.length; i++) {
                     hash = hashes[i].split('=');
                     hash[0] === 'setpagenum'
                        ? UrlSecPart += '&' + hash[0] + '=' + parseInt(parseInt(hash[1], 10) - 1, 10)
                        : UrlSecPart += '&' + hashes[i];
                }
                return UrlFirsPart + '?' + UrlSecPart;
            };
            Venda.Scroll.updateFirstPageURL = function() {
                $('#js-list-scroll li:last').attr('data-url', Venda.Scroll.firstPageURL());
                $('#js-list-scroll li:last').addClass('itemPage');
            };
            Venda.Scroll.updateFirstPageURL();
        }

        // no more items to laod
        Venda.Scroll.endPages = function() {
            $('<div id="temp" class="panel text-center"></div>').insertAfter($('#js-list-scroll'));
            $('#temp').html('No more products to load');
            $('#tempLoading').hide();
            $('#js-load-more').hide();
            is_loading = 1;
        };

        $(window).scroll(function() {

            var scrollTop = $(window).scrollTop();
            var scrollBottom = $(window).scrollTop()+$(window).height();
            var elementBottom = ($('#js-list-scroll').offset().top) + ($('#js-list-scroll').height());


            Venda.Scroll.nearPageReplaceURL = function() {
                $('.itemPage').each(function() {
                    if (!$('#loadingsearch').is(':visible')) {
                        if (Venda.Scroll.mostlyInvisible(this)) { Venda.Scroll.changeState($(this).attr('data-url'));  }
                    }
                    load = false;
                });
            };

            if (next_url === 'end') {
                if ($('#temp').length === 0) { Venda.Scroll.endPages(); }
                Venda.Scroll.nearPageReplaceURL();
            } else {
                if (scrollBottom > elementBottom-500) {
                    if (is_loading === 0) {
                        if (!$('#loadingsearch').is(':visible')) {
                            load = true;
                            Venda.Scroll.changeState(next_url);
                            is_loading = 1;
                        }
                    }
                } else {
                    Venda.Scroll.nearPageReplaceURL();
                }
            }
            if (slideTop) {
                if ($(this).scrollTop() > 10) {
                    $('.js-scroll-to-top').fadeIn(500);
                } else {
                    $('.js-scroll-to-top').fadeOut(500);
                }
            }
        });

        // if page section is nearly visible
        Venda.Scroll.mostlyInvisible = function(element){
            var scrollTop = $(window).scrollTop();
            var scrollBottom = scrollTop + $(window).height();
            var elementBottom = ($(element).offset().top) + ($(element).height());
            return ((scrollBottom > elementBottom-100) && ((scrollBottom < elementBottom)));
        };

        // assign a class to the end of each page so we trach each page
        Venda.Scroll.breakPages = function(){
            $('#js-list-scroll li:last').attr('data-url', next_url);
            $('#js-list-scroll li:last').addClass('itemPage');
        };

        Venda.Scroll.localNextURL = function(data){
            var len = $(data).find('.pagn-next').length;
            if (len === 0) {
                next_url = 'end';
            } else {
                next_url = $(data).find('.pagn-next a').attr('href');
                Venda.Scroll.updateFirstPageURL();
                is_loading = 0;
            }
        };

        // load content if requured
        History.Adapter.bind(window,'statechange',function() {

            // Scroller in use
            if (contentDisplayType) {
                if (load) {
                    Venda.Scroll.processLoading();
                }
            } else {

                // refine is in use
                var theState = History.getState(), newURL = theState.data.realUri || theState.url;
                $.get(newURL, Venda.Scroll.localNextURL);
                Venda.Scroll.breakPages();
            }

        });

    }
}
