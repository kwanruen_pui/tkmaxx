// JavaScript Document/* global Venda, jQuery, console */

/**
 * Venda.Widget.Promotion.js
 *
 * ---------
 * REVISIONS
 * ---------
 * 200514 - Bow <bowc@venda.com> - Modified and add ability to delete applied promotion
 * Legacy - Hayley Easton <HayleyEaston@venda.com> - Created
 * ---------
 * DEPENDANCIES
 * ---------
 * jQuery.1.10.2.js
 * ---------
 */

(function (Venda, $) {
  'use strict';

  /**
   * Set up the namespace.
   */
  Venda.namespace('Widget.Promotion');

  /**
   * Init globals.
   */
  var options;

  options = {
    protocol: $('#tag-protocol').html(),
    codeHttp: $('#tag-codehttp').html(),
    addMessage: $('#js-promo-waitMsg').text(),
    delMessage: $('#js-promo-deleteMsg').text()
  };

  /**
   * Media Code
   * Validate and submit media code using ajax if not on basket for in-page display
   * Update minicart figures with ajax too if not on basket
   */
  Venda.Widget.Promotion.checkVoucherForm = function (action, ele) {
    var strCode, url, ajaxURL, triggerAction;

    strCode = $.trim($('#vcode').val());
    $('#vcode').val(strCode);
    
    //tjx promocode hack
    //tjx promocode hack: set regex
    var patt=/^[A-Za-z]{3}[0-9]{5}[0-9]+$/;
	// tjx promocode hack: test regex
	var result=patt.test(strCode);
	if(result===true){
		// tjx promocode hack: if regex passed only submit first three letters
		var firstThreeLetters = strCode.slice(0,3).toUpperCase();
		 $('#vcode').val(firstThreeLetters);
	}
    
    url = options.codeHttp + '?ex=co_wizr-promocode' + options.protocol +
          '&curstep=vouchercode&step=next&mode=process&action=' + action;

    if (action === 'add') {
      ajaxURL = url + '&vcode=' + $('#vcode').val();
      triggerAction = 'promotion-applied';
      $('#js-modal-style span').html(options.addMessage);
      //tjx promo hack: reset initial code after code has been submitted- used incase promo code is incorrect
      $('#vcode').val(strCode);

    } else {
      ajaxURL = url + '&basketref=' + $(ele).data('basketref') + '&vcode=' + $(ele).data('vcode');
      triggerAction = 'promotion-deleted';
      $('#js-modal-style span').html(options.delMessage);
    }

    $('#js-modal-style').foundation('reveal', 'open');
    $.ajax({
      url: ajaxURL,
      dataType: 'html',
      global: false //Prevent request being tracked in Google Analytics
    })
    .done(function (result) {
      Venda.Widget.Promotion.response(result, triggerAction);
    })
    .error(function (jqXHR, status, error) {
      console.log(error);
    });
  };

  Venda.Widget.Promotion.response = function (result, triggerAction) {
    var errors;

    errors = $(result).filter('#tag-errors').text();
    if (errors === '1') {
      $('#ajax-error').html(result);
      $('#js-modal-style').foundation('reveal', 'close');
    } else {
      document.promotionform.submit();
      $('body').trigger(triggerAction);
    }
  };

  // Set up the DOM events.
  $(function () {
    $('form[name=promotionform]').on('keypress', '#vcode', function (e) {
      if (e.which === '13') {
        $('#vcode_submit_shopcart').trigger('click');
        return false;
      }
    });
    $('form[name=promotionform]').on('click', '.js-delete', function (e) {
      e.preventDefault();
      Venda.Widget.Promotion.checkVoucherForm('delete', this);
    });
  });

}(Venda, jQuery));
