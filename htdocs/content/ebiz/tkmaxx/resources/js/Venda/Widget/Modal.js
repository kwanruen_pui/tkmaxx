/**
 * Venda.Widget.Modal.js
 * ---------
 * REVISIONS
 * ---------
 * Legacy
 * ---------
 * DEPENDANCIES
 * ---------
 * jQuery.1.10.2.js
 * ---------
 */

/**
 * Set up the namespace.
 */
Venda.namespace('Widget.Modal');

Venda.Widget.Modal.doModal = function() {
	jQuery(document).on('click','.js-doDialog',function(event) {
        jQuery('.inner-wrapper').addClass('openModal');
		if (event.ctrlKey || event.metaKey) {
			window.open(jQuery(this).attr("href"),'_blank');
			//Ctrl+Click popup will open in new tab
		}
		else {
	        // ONLY do if screen size is equal/more than 768 or is less than IE9
	        if (Modernizr.mq('only all and (min-width: 768px)') || jQuery('html').hasClass('lt-ie9')) {
				event.preventDefault();
				var url         = new Uri(Venda.Ebiz.doProtocal(jQuery(this).attr("href"))),
					real_url    = Venda.Ebiz.doProtocal(jQuery(this).attr("href")),
					layout      = jQuery(this).attr("data-layout") || "noheaders",
					size      = jQuery(this).attr("data-size") || "xlarge",
					anchorName  = jQuery(this).attr("data-anchor") || "",
					openID = this.id || "";
				var loadURL = url.toString() + '&layout='+layout;
				var modal = jQuery('#vModal').addClass(size).addClass(openID);

				modal.foundation('reveal', 'open');

				jQuery.get(loadURL, function(data) {
					jQuery('.js-modalContent').hide().empty().html(data).fadeIn(500);

					//if (jQuery('.js-toggleContent').length > 0){
						Venda.Widget.Modal.expandContent();
					//}
					if(jQuery("#helpNavigation").length > 0){
						Venda.Widget.Modal.doModalContent();
					}
					Venda.Widget.Modal.anchor(anchorName);
				});
				return false;
			}
	    }
    });
	jQuery(window).on('resize', function(e) {
		if (Modernizr.mq('only all and (max-width: 767px)')) {
			jQuery('#vModal').foundation('reveal', 'close');
		}
	});
	jQuery(document).on('click','#back_link',function() {
		jQuery("#vModal").foundation('reveal', 'close');
		return false;
	});
	jQuery(document).on('closed', '#vModal', function () {
		jQuery('#vModal').attr('class','reveal-modal');
		jQuery('.js-modalContent').html('<div class="text-center"><i class="icon-spinner icon-spin icon-4x"></i></div>');
        jQuery('.inner-wrapper').removeClass('openModal');
	});
};

Venda.Widget.Modal.doModalContent = function() {
	jQuery("#helpNavigation a").click(function() {
		var url = Venda.Ebiz.doProtocal(jQuery(this).attr("href"));
		jQuery(".js-staticContent").load(url +'&layout=noheaders' , function(){
			if (jQuery('.js-toggleContent').length > 0){
				Venda.Widget.Modal.expandContent();
			}
			if (jQuery('#contactForm').length > 0){
				 jQuery("body").trigger("contact-loaded");
			}
		});
		return false;
	});
};

/**
 * Expand contents
 */
Venda.Widget.Modal.expandContent = function () {
    var txtShow = (jQuery("#txtShow").length != 0) ? jQuery("#txtShow").html() : "";
    var txtHide = (jQuery("#txtHide").length != 0) ? jQuery("#txtHide").html() : "";

    jQuery(".js-toggleContent > div").hide();
    jQuery(".js-modalContent .js-toggleContent > .js-expander, .js-modalContent .js-slidetoggle").each(function () {
        jQuery(this).attr("title", txtShow)
    });
    jQuery(".js-modalContent .js-toggleContent > .js-expander, .js-modalContent .js-slidetoggle").on('click tap', function () {
        var toggleClass = '.'+  jQuery(this).attr('data-slidetoggle');
        jQuery(toggleClass).slideToggle('fast');
        jQuery(this).toggleClass('active');

        //for slide toggle
        if(jQuery(this).attr('data-slidetoggle-icon')) {
            if(jQuery(this).hasClass('active')) {
                jQuery(this).find('i').removeClass(jQuery(this).attr('data-default-icon')).addClass(jQuery(this).attr('data-slidetoggle-icon'));
            } else {
                jQuery(this).find('i').addClass(jQuery(this).attr('data-default-icon')).removeClass(jQuery(this).attr('data-slidetoggle-icon'));
            }
        } else {
            //for non-slide-toggle
            if (jQuery(this).find(".icon-down-marker").length > 0) {
                jQuery(this).find(".icon-down-marker").removeClass('icon-down-marker').addClass('icon-up-marker');
            } else {
                jQuery(this).find(".icon-up-marker").addClass('icon-down-marker').removeClass('icon-up-marker');
            }
        }

        if (jQuery(this).is(".active")) {
            jQuery(this).attr("title", txtHide);
        } else {
            jQuery(this).attr("title", txtShow);
        }

    });
};

Venda.Widget.Modal.anchor = function(anchorName){
	if(anchorName != ""){
		if(jQuery('#vModal .js-modalContent').find('.js-expander#'+anchorName).length > 0){
			jQuery('#vModal .js-modalContent').find('.js-expander#'+anchorName).click();
		}
	}
};
Venda.Widget.Modal.doWindowPopUp = function () {
	jQuery(document).on('click','.js-windowOpen',function(event) {
		event.preventDefault();
		var url         = jQuery(this).attr("href");
		var attrRel     = jQuery(this).attr("rel") || "";
		var attrRel = attrRel.split(',');
		var popupType = attrRel[0] || "standard";
		var popupWidth = attrRel[1] || '780';
		var popupHeight = attrRel[2] || '580';

	   if (popupType == "fullscreen") {
			popupWidth = screen.availWidth;
			popupHeight = screen.availHeight;
		}
		var tools = "";
		if (popupType == "standard") {
			tools = "resizable,toolbar=yes,location=yes,scrollbars=yes,menubar=yes,width=" + popupWidth + ",height=" + popupHeight + ",top=0,left=0";
		}
		if (popupType == "console" || popupType == "fullscreen") {
			tools = "resizable,toolbar=no,location=no,scrollbars=yes,width=" + popupWidth + ",height=" + popupHeight + ",left=0,top=0";
		}
		newWindow = window.open(url, 'newWin', tools);
		newWindow.focus();
	});
};

/**
 * Set up the DOM events.
 */
jQuery(function() {
    if(jQuery('.js-doDialog').length > 0) {
        Venda.Widget.Modal.doModal();
    }
	if(jQuery(".js-windowOpen").length > 0) {
		Venda.Widget.Modal.doWindowPopUp();
	}
    if (jQuery('.js-toggleContent').length > 0) {
        Venda.Widget.Modal.expandContent();
    }
});