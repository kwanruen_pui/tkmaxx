/**
* @fileoverview Venda.Widget.Megamenu
 * Megamenu.js provides the functionality required to generate Static and Dynamic menus for the tp navigation
 * Documentation: https://vendadocs.onconfluence.com/x/1YFH
 *
 * @author Juanjo Dominguez <juanjodominguez@venda.com>
*/

//create namespace for MegaMenu
Venda.namespace('Widget.MegaMenu');


Venda.Widget.MegaMenu.switched   = true;

 Venda.Widget.MegaMenu.setAccessibleMegamenu = function(){
     jQuery('.topnav').removeData('plugin_accessibleMegaMenu');
     jQuery('.topnav').accessibleMegaMenu({
            /* prefix for generated unique id attributes, which are required
               to indicate aria-owns, aria-controls and aria-labelledby */
            uuidPrefix: 'accessible-megamenu',
            /* css class used to define the megamenu styling */
            menuClass: 'mm_ul',
            /* css class for a top-level navigation item in the megamenu */
            topNavItemClass: 'js-nav-item',
            /* css class for a megamenu panel */
            panelClass: 'js-mm-sub',
            /* css class for a group of items within a megamenu panel */
            panelGroupClass: 'js-sub-nav-group',
            /* css class for the hover state */
            hoverClass: 'hover',
            /* css class for the focus state */
            focusClass: 'focus',
            /* css class for the open state */
            openClass: 'js-mm-liselected'
    });
 };

jQuery(window).on('load', function(){
    if (Modernizr.mq('only all and (max-width: 998px)')) {
        Venda.Widget.MegaMenu.switched   = false;
    }
});

jQuery(window).on('resize load', function(){
    if (Modernizr.mq('only all and (max-width: 998px)') && !Venda.Widget.MegaMenu.switched){
        //tablet and mobile screen
        Venda.Widget.MegaMenu.switched = true;
        Venda.Widget.MegaMenu.toggleMenu();
        jQuery('ul#mm_ul a.js-mm_icat').removeClass('js-mm-sub');
    } else if (Modernizr.mq('only all and (min-width: 999px)') && Venda.Widget.MegaMenu.switched  ){
        Venda.Widget.MegaMenu.switched = false;
        Venda.Widget.MegaMenu.hoverMenu();
        Venda.Widget.MegaMenu.menuWidth();
    }
});

 Venda.Widget.MegaMenu.toggleMenu = function(){
    jQuery('ul#mm_ul a.js-mm_icat, .js-mm-sub').removeClass('open js-mm-liselected').removeAttr('style');
    jQuery('ul#mm_ul').addClass('js-mobile_ul');
    if(Modernizr.touch) {
       jQuery('ul#mm_ul, ul#mm_ul a.js-mm_icat').off('click mouseover touchend touchstart');
    } else {
        Venda.Widget.MegaMenu.setAccessibleMegamenu();
        jQuery('ul#mm_ul, ul#mm_ul a.js-mm_icat').off('click focusin focusout mouseover mouseout');
    }
    /*jQuery('ul#mm_ul a.js-mm_icat').on('click', function (event) {
        if(jQuery(this).next('.js-mm-sub').children().length > 0) {
        // only toggle if there is sub category
            event.preventDefault();
            if(jQuery(this).next('.js-mm-sub').hasClass('is-open') ) {
                jQuery(this).next('.js-mm-sub').removeClass('is-open js-mm-liselected');
            } else {
                jQuery('ul#mm_ul .js-mm-sub').removeClass('is-open js-mm-liselected');
                jQuery(this).next('.js-mm-sub').addClass('is-open');
                jQuery('html, body').animate({ scrollTop: jQuery(this).offset().top }, 800);
            }
        }
    });*/
};

Venda.Widget.MegaMenu.hoverMenu = function(){
    jQuery('ul#mm_ul').removeClass('js-mobile_ul');
    jQuery('.js-mm-sub').removeClass('is-open');
    jQuery('ul#mm_ul, .js-mm-sub').removeAttr('style');
    jQuery('ul#mm_ul a.js-mm_icat').off('click');
    Venda.Widget.MegaMenu.setAccessibleMegamenu();
};

Venda.Widget.MegaMenu.menuWidth = function(){
    var $subMenu, subMenuWidth, subMenuOuterWidth, menuoffset, $this;
    $subMenu = jQuery('.mm_ul .js-mm-sub');
    megamenuWidth = parseFloat(jQuery('ul#mm_ul').width());

    $subMenu.each(function(){
        subMenuWidth = 0;
        $this = jQuery(this);
       if($this.find('.js-sub-nav-group').length > 1) {
            subMenuWidth = ($this.find('.js-sub-nav-group').length * 80) + '%';
       }
       else {
            subMenuWidth = '100%';
       }
       $this.css('width', subMenuWidth);

       subMenuOuterWidth =  $this.outerWidth();
       menuoffset = $this.parent().position();
        if ((menuoffset.left + subMenuOuterWidth) > megamenuWidth) {
            $this.addClass('js-mm-left');
        }
    });
};
