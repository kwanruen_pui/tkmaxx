/*!
 * MinicartDetail module
 * @dependancies  jQuery, Attributes.js
 * @dependants  Offcanvas.js, QuickBuy.js
 */
!function (global) {
  'use strict';

  /**
   * Define the minicart detail module and return it to the Venda namespace
   * @param   {Object}  Venda  Venda namespace
   * @param   {Object}  $      Query
   * @return  {Object}  MinicartDetail  Module
   */
  function defineModule(Venda, $) {

    var options, Atts;

    options = {

      endpoint: {
        details: '/page/home?layout=minicart',
        addProduct: '#tag-codehttp',
        multiFormUrl: [
          'bsref=shop&log=22&ex=co_disp-shopc&',
          'mode=addmul&param1=bulk&param2=invt&layout=minicart&'
        ],
        singleFormUrl: [
          'bsref=shop&log=22&ex=co_disp-shopc&',
          'mode=add&layout=minicart&'
        ]
      },

      node: {
        general: {
          wrapper: '.minicartDetailWrapper',
          details: '.basketholder'
        },
        dropdown: {
          attachedTo: '#basketSection, #box-basket',
          wrapper: '#basketSection .minicartDetailWrapper, #box-basket .minicartDetailWrapper',
          header: '#basketSection #minicart-header, #box-basket #minicart-header',
          footer: '#basketSection #minicart-footer, #box-basket #minicart-footer',
          content: '#basketSection #basketWrapper, #basketSection #minicart_empty, #box-basket #basketWrapper, #box-basket #minicart_empty',
          details: '#basketSection .basketholder, #box-basket .basketholder'
        },
        offCanvas: {
          wrapper: '#js-canvas-content-right .minicartDetailWrapper'
        },
        addingToBasket: '.js-addingtobasket',
        buttonUp: '.buttonUp',
        buttonDown: '.buttonDown',
        form: '#addproductform',
        totalItems: '#basketSection .js-updateItems, #box-basket .js-updateItems',
        totalItemsTiny: '.js-canvas-right .js-updateTotalMini',
        totalItemsText: '.js-updateItemsText',
        totalPrice: '#basketSection .js-updateTotal',
        addProduct: '.js-addproduct',
        minicartHeaderText: '.minicart-header-text',
        productUpdate: '.js-productUpdate',
        productUpdateMulti: '.js-buyControlsMulti',
        quantity: 'select[name="qty"]',
        notify: '#addproductform #notify',
        notifyTemplate: '#notifyTemplate',
        productName: '#tag-invtname',
        attStyles: '.js-attributesForm',
        labels: '#attributeNames'
      },
      device: util.checkDevice(),
      duration1: 250,
      duration2: 3000,
      duration3: 5000,
      duration4: 500,
      duration5: 750,
      isResponsive: false,

      // 'disabled', 'hover', or 'click'
      showMinicartDetail: 'hover',
      showAfterAddProd: false,
      showNotifications: true,
      appendNotify: false,
      highlight: true,
      scrollSpeed: 40,
      autoload: false,
      animOpen: true,
      largeDeviceSmoothScroll: true
    };

    Atts = Venda.Attributes;

    var MinicartDetail = {

      /**
       * Initialise the module, passing in the options above.
       * @param   {Object}  options  Options
       */
      init: function (options) {
        util.merge(this, options);
        this.minicartLoaded = false;
        this.pending = false;
        this.visible = false;
        this.productIds = {};
        this.multiForm = false;
        this.multiFormType = false;
        this.multiTest = ['productdetailSet', 'productset', 'productdetailMulti'];
        this.largeDevices = ['standard', 'large', 'tablet'];
        this.checkboxes = {};
        this.device = util.checkDevice();
        if (this.autoload && this.showMinicartDetail !== 'disabled') {
          this.loadMinicartHtml();
        }
        return this;
      },

      /**
       * Load the minicart content. Note that this loads the minicart data into
       * two sections identified by the general wrapper class specified in the
       * options - the dropdown minicart and the off-canvas minicart. The 'loaded'
       * variable is there to prevent from the dropdown minicart from closing on
       * immediately opening if the showAfterAddProd is true.
       * @param   {Boolean}    highlight  Highlight new added products
       * @param   {Boolean}    update     Update the minicart
       * @param   {Function}  callback    Function to call after minicart is
       *                                  loaded
       */
      loadMinicartHtml: function (highlight, update, callback) {
        var _this, loader, $wrapper, loaded;
        _this = this;
        loaded = 0;
        this.minicartLoaded = false;
        $('.js-minicart i.icon-shopping-cart').replaceWith('<i class="icon-spinner"></i>');
        loader = this.template('loader').join('');
        $wrapper = $(this.node.general.wrapper);
        if (this.device === 'mobile' || this.device === 'tablet') { $wrapper.empty().append(loader); }
        $wrapper.load(this.endpoint.details, function () {
          loaded++;
          _this.cacheProductIds();
          _this.minicartLoaded = true;
          if (loaded < 2 && update && _this.showAfterAddProd) {
            _this.toggleVisibility();
          }
          if (highlight && _this.highlight) {
            _this.highlightProduct(highlight);
          }
          $('.js-minicart i.icon-spinner').replaceWith('<i class="icon-shopping-cart"></i>');
          if (callback) { callback(); }
        });
        return this;
      },

      /**
       * Highlights the background of an added product in the minicart
       * if the option has been selected.
       * @param   {String}  id  Product id to be highlighted
       */
      highlightProduct: function (id) {
        var $el, position;
        $el = $(this.node.dropdown.details).find('li.minicart-' + id);
        $el.addClass('minicartHighlight');
        position = $el.position();
        if (this.showAfterAddProd) {
          this.scrollMinicartDetails(null, position.top);
        }
        return this;
      },

      /**
       * Cache the product ids when the page is first loaded.
       * This is necessary for the remove-tem functionality to work - once
       * the minicart is reloaded again (when an item is added or removed)
       * the new delete-line ids will not work. Caching the original ids and then
       * repopulating them (populateProductIds) prevents this. The cached ids are
       * stored under the product 'hash'. e.g. cla000bla7r.
       * @return  {[type]}  [description]
       */
      cacheProductIds: function () {
        var $hidden, obj, i;
        obj = {};
        $hidden = $(this.node.general.details).find('.hiddenfields');
        $hidden.each(function () {
          var hash, id;
          id = $(this).find('[name="line-id"]').val();
          hash = $(this).find('[name="hash"]').val();
          obj[hash] = id;
        });
        for (i in obj) {
          if (obj.hasOwnProperty(i)) {
            if (!this.productIds[i]) {
              this.productIds[i] = obj[i];
            }
          }
        }
        return this;
      },

      /**
       * Serves the minicartdetail templates. Used so that we don't have HTML
       * concatonation clogging up the code. Only used in conjunction with the
       * util.applyTemplate method.
       * @param   {String}  template  Template name
       * @return  {String}            HTML
       */
      template: function (template) {
        var obj;
        obj = {
          alert: [
            '<div id="notify" class="box box-section">',
            '<div class="box-header alert"></div>',
            '<div class="box-body alert">',
            '<div class="row">',
            '<div class="large-24 columns">',
            '#{alert}',
            '</div>',
            '</div>',
            '</div>',
            '</div>'
          ],
          detailsLine: [
            '<span class="bold">#{label}:</span> #{select}'
          ],
          multicheckbox: [
            '<label>#{text} ',
            '<input type="checkbox" id="checkAllProducts" />',
            '</label>'
          ],
          loader: [
            '<div class="canvas-loading">',
            '<i class="icon-spinner icon-spin icon-2x"></i>',
            '<span>Please wait...</span>',
            '</div>'
          ]
        };
        return obj[template];
      },

      /**
       * When the minicart summmary or the minicartdetail close button is
       * clicked, and the device is a desktop or greater, this works out
       * whether to open or close the minicartdetails.
       */
      toggleVisibility: function () {
        var isDesktop, _this, checkDevice, $cart;
        _this = this;
        checkDevice = util.checkDevice();
        this.device = (checkDevice === undefined) ? 'large' : checkDevice;
        isDesktop = this.largeDevices.contains(this.device);
        if (isDesktop) {
          if (this.visible) {
            this.toggleMinicart('close');
          } else {
            if (!this.minicartLoaded) {
              $cart = $('#showcart').clone();
              $('#showcart').html('<i class="loading icon-spinner"></i>');
              this.loadMinicartHtml(null, null, function () {
                _this.setPosition();
                _this.toggleMinicart('show');
                $('.loading').remove();
                $('#showcart').html($cart.html());
              });
            } else {
              this.setPosition();
              this.toggleMinicart('show');
            }
          }
        }
        else
        {
               dataLayer.push({'Device':'Mobile'});
        }
        return this;
      },

      /**
       * Prep elements for reveal.
       */
      prepareElements: function () {
        var elements, i, l;
        elements = Array.prototype.slice.call(arguments);
        for (i = 0, l = elements.length; i < l; i++) {
          elements[i].css({ opacity: 0, visibility: 'visible' });
        }
        return this;
      },

      /**
       * Displays the minicart either immediately, or using
       * a sliding animation.
       * @return  {[type]}  [description]
       */
      showMinicart: function () {
        var $content, $footer, $wrapper, _this, height;
        _this = this;
        $wrapper = $(this.node.dropdown.wrapper).show();
        $footer = $(this.node.dropdown.footer);
        $content = $(this.node.dropdown.content);
        height = $footer.position().top + $footer.outerHeight();
        if (this.animOpen) {
          this.prepareElements($wrapper);
          $wrapper.css({ opacity: 1.0 });
          $wrapper.animate({
            height: height
          }, this.duration1, function () {
            _this.setPosition('resize');
            _this.visible = true;
            _this.pending = false;
          });
        } else {
          $wrapper.css({ visibility: 'visible', height: height });
          this.setPosition('resize');
          this.visible = true;
          this.pending = false;
          $wrapper.css({ opacity: 1.0 });
        }
        return this;
      },
      /**
       * Hides the minicart either immediately, or using
       * a sliding animation.
       * @return  {[type]}  [description]
       */
      hideMinicart: function () {
        var $content, $footer, $wrapper, _this;
        _this = this;
        $wrapper = $(this.node.dropdown.wrapper);
        $footer = $(this.node.dropdown.footer);
        $content = $(this.node.dropdown.content);
        if (this.animOpen) {
          $wrapper.animate({
            height: 0
          }, this.duration1, function () {
            $wrapper.hide();
            _this.pending = false;
            _this.visible = false;
          });
        } else {
          $wrapper.hide();
          this.pending = false;
          this.visible = false;
        }
        return this;
      },

      /**
       * Opens or closes the minicartdetail
       * @param   {String}  type  Toggle type
       */
      toggleMinicart: function (type) {
        if (type === 'show') { this.showMinicart(); }
        if (type === 'close') { this.hideMinicart(); }
        return this;
      },

      /**
       * Set the position of the minicartdetail on the page
       * @param  {String}  type  Defines the type of positioning taking place -
       *                         can be either resize or removed.
       */
      setPosition: function (type) {
        this.positionContent();
        this.positionFooter();
        this.positionMinicartDetail(type);
        this.toggleDirectionButtons();
        return this;
      },

      /**
       * Positions the minicart content wrapper depending on the screen height and its
       * relation to the cart details div.
       */
      positionContent: function () {
        var $content, $header, detailsHeight, height, mainHeight, headerLowerPos;
        $content = $(this.node.dropdown.content);
        $header = $(this.node.dropdown.header);
        detailsHeight = $(this.node.dropdown.details).outerHeight() + 10;
        mainHeight = $(window).height() - $(this.node.dropdown.attachedTo).outerHeight() - 450;
        if (mainHeight > detailsHeight) {
          height = detailsHeight;
        } else {
            height = (mainHeight < 300) ? 300 : mainHeight;
        }
        headerLowerPos = $header.offset().top + $header.outerHeight();
        $content.offset({ top: headerLowerPos });
        $content.height(height);
        return this;
      },

      /**
       * Position the minicartdetail footer in relation to the minicartdetail
       * content wrapper.
       */
      positionFooter: function () {
        var contentLowerPos, $content, $footer;
        $footer = $(this.node.dropdown.footer);
        $content = $(this.node.dropdown.content);
        contentLowerPos = $content.offset().top + $content.height();
        $footer.offset({ top: contentLowerPos });
        return this;
      },

      /**
       * Position the minicartdetail wrapper along the x and y axis
       * immediately under the attachedTo wrapper. In this case the attachedTo
       * wrapper will be the showcart summary.
       * @param  {String}  type  Defines the type of positioning taking place -
       *                         can be either resize or removed.
       */
      positionMinicartDetail: function (type) {
        var $wrapper, $attachedTo, $footer, widthDiff;
        $wrapper = $(this.node.dropdown.wrapper);
        $attachedTo = $(this.node.dropdown.attachedTo);
        $footer = $(this.node.dropdown.footer);
        widthDiff = $wrapper.outerWidth() - $attachedTo.outerWidth() - 1;
        $wrapper.css({
          top: $attachedTo.position().top + $attachedTo.height(),
          left: -widthDiff
        });
        if (type === 'resize') {
          $wrapper.css({ height: $footer.position().top + $footer.outerHeight() });
        }
        if (type === 'removed') {
          $wrapper.animate({
            height: $footer.position().top + $footer.outerHeight()
          }, 500);
        }
        return this;
      },

      /**
       * Resets the form selects and disables the 'add to basket' button
       * after adding a product.
       */
      resetForm: function () {
        var $selects, text, $form;
        $form = $(this.node.form);
        text = $(this.node.attStyles).text();
        switch (text) {
          case 'dropdown':
            $selects = $form.find('select');
            $selects.each(function () {
              $(this).prop('selectedIndex', 0)
                .trigger('change')
                .trigger('liszt:updated');
            });
            break;
          case 'swatch':
            $selects = $form.find('select');
            break;
          case 'halfswatch':
            break;
        }
        return this;
      },

      /**
       * Enable a button.
       * @param  {DOM node} button  The button.
       * @param  {String} text      Value of the button.
       * @param  {String} type      Whether the button is input/anchor.
       */
      enableButton: function (button, text, type) {
        var $button;
        $button = $(button);
        if (type && type === 'anchor') {
          $button.html(text);
        } else {
          $button.val(text);
        }
        $button.removeAttr('disabled');
        $button.css({ opacity: 1.0 });
        $button.find('i').remove();
        return this;
      },

      /**
       * Disable a button.
       * @param  {DOM node} button  The button.
       * @param  {String} text      Value of the button.
       * @param  {String} type      Whether the button is input/anchor.
       */
      disableButton: function (button, text, type) {
        var $button;
        $button = $(button);
        if (type && type === 'anchor') {
          $button.html(text);
        } else {
          $button.val(text);
        }
        $button.append('<i class="icon-spinner icon-spin icon-large thinpad-side"></i>');
        $button.attr('disabled', 'disabled');
        $button.css({ opacity: 0.5 });
        return this;
      },

      /**
       * Submits the product form after a client has clicked the 'add to basket'
       * button. The button is disabled, the form serialised and the form submitted
       * to the url specified in options.endpoint.addProduct. The method calls
       * processReturnedHTML once the AJAX is concluded.
       */
      addProduct: function (ele) {
        var serialisedForm, $form, isValid;
        isValid = this.validate(ele);
        if (isValid) {
          //$form = $(this.node.form);
          $form = $(ele.form);
          $form.find('input[name="layout"]').val('minicart');
          $form.find('input[name="ex"]').val('co_disp-shopc');
          $(ele).data('addtobasket', $(ele).text());
          this.disableButton($(ele), $(this.node.addingToBasket).text(), 'anchor');
          this.removeNotify();
          serialisedForm = this.createSerialisedForm($form, $(ele));
          $.ajax({
            type: 'POST',
            global: false,
            url: $(this.endpoint.addProduct).html(),
            data: serialisedForm
          })
          .done(this.processReturnedHTML.bind(this, $(ele)))
          .error(function (jqXHR, status, error) {
            console.log(error);
          });
        }
        return this;
      },

      /**
       * Due to the difference in form styles between normal/multipage
       * products, the serialised forms need to be created differently.
       * @return  {String}  Serialised form
       */
      createSerialisedForm: function ($form, $addBtn) {
        var serialisedForm, formType, $inputs, params, $addItem, $allprod, isSingleAdd, isMultiAdd;
        formType = $('.js-attributesForm').attr('id');
        isSingleAdd = (formType === 'productdetailSet2' && $addBtn.is('.js-addproduct'));
        isMultiAdd = (formType === 'productdetailSet2' && $addBtn.is('.js-addproduct-multi'));

        if (this.multiTest.contains(formType)) {
          $inputs = $('input[name="qtylist"]:not(:disabled)');
          params = [];
          $.each($inputs, function (i, e) {
            params.push('itemlist=' + e.id.replace('qtylist_', ''));
            params.push('qtylist=' + e.value);
          });
          serialisedForm = this.endpoint.multiFormUrl.join('') + params.join('&');

        } else if (isMultiAdd) {
          // Productdetailset2 style - Add All To Basket
          $allprod = $('.js-oneProduct.js-prodMulti');
          params = [];
          params.push('invt='+ $('#addproductform').find('.js-addproduct-multi').data('sku'));

          $.each($allprod, function (i, e) {
            $addItem = $(e);
            params.push('itemlist=' + $addItem.find('[name="qty"]').data('sku'));
            params.push('qtylist=' + $addItem.find('[name="qty"]').val());
            if (Venda.Attributes.productArr[i]) {
              params.push('att1list=' + (Venda.Attributes.productArr[i].attSet['att1'].selectedValue || ''));
              params.push('att2list=' + (Venda.Attributes.productArr[i].attSet['att2'].selectedValue || ''));
              params.push('att3list=' + (Venda.Attributes.productArr[i].attSet['att3'].selectedValue || ''));
              params.push('att4list=' + (Venda.Attributes.productArr[i].attSet['att4'].selectedValue || ''));
            }
          });
          serialisedForm = this.endpoint.multiFormUrl.join('') + params.join('&');

        } else if (isSingleAdd) {
          // Productdetailset2 style- Add a single produt to basket
          params = [];
          $addItem = $addBtn.parents('.js-prodMulti');
          params.push('invt='+$addItem.find('#invtref').text());
          params.push('buy='+$addItem.find('#invtref').text());
          params.push('ivref='+$addItem.find('#invtref').text());
          params.push('qty='+$addItem.find('.js-qty').val());
          params.push('att1='+($addItem.find('#swatchList_att1 .js-selected').data('attvalue') || ''));
          params.push('att2='+($addItem.find('#swatchList_att2 .js-selected').data('attvalue') || ''));
          params.push('att3='+($addItem.find('#swatchList_att3 .js-selected').data('attvalue') || ''));
          params.push('att4='+($addItem.find('#swatchList_att4 .js-selected').data('attvalue') || ''));
          serialisedForm = this.endpoint.singleFormUrl.join('') + params.join('&');

        } else {
          // Productdetail style
          serialisedForm = $form.serializeToLatin1();
        }
        return serialisedForm;
      },

      /**
       * Processes the returned HTML sfter adding a new product to the basket.
       * On success the minicart and item totals are updated and,
       * if turned on, any notifications shown. The final
       * 'minicart-items-added' trigger is REQUIRED for tracking.
       * @param   {Node}  $form  The original form
       * @param   {String}  html   HTML
       */
      processReturnedHTML: function ($btn, html) {
        var highlight, hasAlert, addToBasketText, $form;
        $form = $(this.node.form);
        hasAlert = $(html).find('.alert-box').length > 0 ? true : false;
        if (this.showMinicartDetail !== 'disabled') {
          highlight = Atts.dataObj.atrsku;
          this.loadMinicartHtml(highlight, true);
          this.updateTotals(html);
          $('body').trigger('minicart-items-added');
        }
        this.processNotifications(html, $form, $btn);
        addToBasketText = $btn.data('addtobasket');
        this.enableButton($btn, addToBasketText, 'anchor');
        $form.find('input[name="layout"]').val('');
        $form.find('input[name="ex"]').val('co_wizr-shopcart');
        $('#js-aria')
          .attr('role', 'alert')
          .attr('aria-live', 'assertive')
          .hide()
          .show();
        $('#js-aria-viewbasket').focus();
        return this;
      },

      /**
       * Updates the item total and text on the main page after a product has
       * been added.
       * @param   {String}  html  The returned |HTML from addProductt
       */
      updateTotals: function (html) {
        var $html, obj;
        obj = {};
        $html = $(html);
        obj.totalItems = $html.find('.js-updateItems').text();
        obj.totalItemsTiny = $html.find('.js-updateTotalMini').text();
        obj.totalItemsText = $html.find('.js-updateItemsText').text();
        obj.totalPrice = $html.find('.js-updateTotal').text();
        $(this.node.totalItems).text(obj.totalItems);
        $(this.node.totalItemsTiny).text(obj.totalItems);
        $(this.node.totalItemsText).text(obj.totalItemsText);
        $(this.node.totalPrice).text(obj.totalPrice);
        return this;
      },

      /**
       * Takes the returned HTML from addProduct and extracting and showing
       * any warnings. If there are no warnings it 1) shows the minicart if that
       * option is turned on, and 2) shows the product notification, either
       * appended or prepended to the productUpdate node depending on the option.
       * @param   {String}  html  The returned |HTML from addProductt
       * @param   {Node}  $form  The submitted form
       */
      processNotifications: function (html, $form) {
        var $html, $alert, templateObject, hasAlert, isProductSet;
        $html = $(html);
        isProductSet = $('#productdetailMulti, #productdetailSet2').length;
        $alert = $html.find('.alert-box[data-alert]');
        hasAlert = $alert.length > 0 ? true : false;
        if (this.showNotifications) {
          if (hasAlert) {
            templateObject = {
              alert: $alert.html()
            };
            this.showNotify(templateObject, 'alert');
          } else {
            if (this.multiForm || isProductSet) {
              templateObject = { text: 'Items added to your basket.' };
            } else {
              templateObject = this.createProductNotify($form);
            }
            this.showNotify(templateObject, 'default');
          }
        }
        return this;
      },

      /**
       * Returns an array of labels for the notification
       * @return  {Array}  Labels
       */
      getLabels: function () {
        var labels;
        labels = [];
        $(this.node.labels).find('div').each(function () {
          var text;
          text = $(this).text();
          if (text) { labels.push(text); }
        });
        return labels;
      },

      /**
       * Creates the new product notification from the information contained in
       * the submitted addProduct form. It returns an template object that can
       * @param   {Node}  $form  The submitted form
       * @return  {Object}         Notification object
       */
      createProductNotify: function ($form) {
        var product, quantity, details, attribute, attr, labels, obj,
            $choice, select, label, i, l, o, template;
        product = $(this.node.productName).text();
        quantity = $form.find(this.node.quantity).val();
        labels = this.getLabels();
        if ($('.js-attributesForm').length > 0) {
          template = this.template('detailsLine');
          details = '';
          for (i = 0, l = labels.length; i < l; i++) {
            attr = i + 1;
            attribute = '[name="att' + attr + '"]';
            if (attribute && attribute.length > 0) {
              $choice = $form.find(attribute);
              select = $choice.val();
              label = labels[i];
              o = { label: label, select: select };
              details += util.applyTemplate(template, o);
              if (i < labels.length - 1) { details += ', '; }
            }
          }
          if (labels.length > 0) { details = '(' + details + ')'; }
          obj = {
            quantity: quantity,
            product: product,
            details: details
          };
        } else {
          obj = {
            quantity: quantity,
            product: product
          };
        }
        return obj;
      },

      /**
       * Removes the notification from the page.
       */
      removeNotify: function () {
        $(this.node.notify)
          .animate({ height: 0 }, {
            duration: this.duration1,
            complete: function () { $(this).remove(); }
          });
        return this;
      },

      /**
       * Shows a notification. Either a warning notification, or the
       * product notification. Appended or prepended to the productUpdate node as
       * specified in the options.
       * @param   {Object}  templateObject  Template object
       * @param   {String}  type            Template type
       */
      showNotify: function (templateObject, type) {
        var html, template, attachedTo, node, $template, isProductSet, isAttributeProduct;
        if (type === 'default') {
          $template = $(this.node.notifyTemplate);
          isAttributeProduct = $('.js-attributesForm').length;
          isProductSet = $('#productdetailMulti, #productdetailSet2').length;

          if (isProductSet) {
            $template.find('#js-notify-text, #js-notify-details, #js-notify-product').empty();
          }
          if (!this.multiForm && !isProductSet) {
            $template.find('#js-notify-text').empty();
          }
          if (this.multiForm || isProductSet || !isAttributeProduct) {
            $template.find('#js-notify-details').empty();
          }
          template = $template.html();
        } else {
          template = this.template(type);
        }

        html = util.applyTemplate(template, templateObject);
        node = this.node;
        attachedTo = isProductSet ? node.productUpdateMulti : node.productUpdate;
        if (this.appendNotify) {
          $(html).insertAfter($(attachedTo));
        } else {
          $(html).insertBefore($(attachedTo));
        }
        return this;
      },

      /**
       * Turns the minicart scroll buttons on/off depending on the position of the
       * minicart details.
       */
      toggleDirectionButtons: function () {
        var $content, position, height, scrollHeight, $up, $down, totalItems;
        $content = $(this.node.dropdown.content);
        $up = $(this.node.buttonUp);
        $down = $(this.node.buttonDown);
        position = $content.scrollTop();
        scrollHeight = $content[0].scrollHeight;
        height = $content.outerHeight();
        totalItems = parseInt($(this.node.totalItems).html(), 10);
        if (totalItems > 0) {
          $up.removeClass('off').addClass('on');
          $down.removeClass('off').addClass('on');
          if (position <= 0) {
            $up.removeClass('active').addClass('inactive');
            this.scrollStop();
          }
          if (position > 0) {
            $up.removeClass('inactive').addClass('active');
          }
          if (position >= scrollHeight - height) {
            $down.removeClass('active').addClass('inactive');
            this.scrollStop();
          }
          if (position < scrollHeight - height) {
            $down.removeClass('inactive').addClass('active');
          }
        } else {
          $up.removeClass('on').addClass('off');
          $down.removeClass('on').addClass('off');
        }
        return this;
      },

      /**
       * Scrolls the minicart details up or down depending on which of the scroll
       * buttons was clicked.
       * @param  {String} direction Up/down.
       * @param  {Integer} highlightPos  Position of the highlighted item
       */
      scrollMinicartDetails: function (direction, highlightPos) {
        var pos, $wrapper, speed;
        $wrapper = $(this.node.dropdown.content);
        pos = $wrapper.scrollTop();
        if (direction) {
          speed = this.scrollSpeed;
          $wrapper.scrollTop(direction === 'up' ? pos - speed : pos + speed);
        } else {
          $wrapper.scrollTop(highlightPos);
        }
        this.setPosition();
        return this;
      },

      /**
       * Required method to allow the scrolling to function while
       * keeping the mouse button held down.
       * @param   {String}  direction  Direction of scroll
       */
      scrollStart: function (direction) {
        var _this;
        _this = this;
        if (['mobile', 'tablet'].contains(this.device) || !this.largeDeviceSmoothScroll) {
          this.scrollMinicartDetails(direction);
        } else {
          (function scroller() {
            _this.scrollMinicartDetails(direction);
            _this.scrollInterval = setTimeout(scroller, 100);
          }());
        }
        return this;
      },

      /**
       * Stop scrolling
       */
      scrollStop: function () {
        if (this.largeDeviceSmoothScroll) {
          clearTimeout(this.scrollInterval);
        }
        return this;
      },

      /**
       * Populates the ids of each of the items in the minicart from the
       * copies cached when the page was first loaded in order to allow the
       * remove-item functionality to properly work. In summary, the method runs
       * through each of the inputs for each of an item's hiddenInputs div
       * replacing the current id found in [name="line-id"] with the cached copy
       * found in this.productids under the key represented by the
       * product hash [name="hash"], e.g. cla000bla7r.
       */
      populateProductIds: function () {
        var $hidden, _this;
        _this = this;
        $hidden = $(this.node.general.details).find('.hiddenfields');
        $hidden.each(function () {
          var hash, id, $this, $input;
          $this = $(this);
          id = $this.find('[name="line-id"]').val();
          hash = $this.find('[name="hash"]').val();
          if (_this.productIds[hash]) {
            $input = $this.find('input');
            $input.each(function () {
              var name, value, regex, $this;
              $this = $(this);
              name = $this.attr('name');
              value = $this.attr('value');
              regex = new RegExp('\\*|' + id, 'g');
              $this.attr('name', name.replace(regex, _this.productIds[hash]));
              $this.attr('value', value.replace(regex, _this.productIds[hash]));
            });
          }
        });
        return this;
      },

      /**
       * Builds a new form for the purposes of removing an item from the minicart.
       * This is done by creating a form element and appending the inputs from
       * the product's hiddenInput div, and appending new inputs to the form
       * representing each item of that product required.
       * @param   {Node}    el        Dom node of the item to be removed.
       * @param   {Function}  callback  Callback function
       */
      createItemRemovalForm: function (el, callback) {
        var $hidden, $normal, $dataline, numberStart, quantity, i, l,
            lineid, $form, template;
        $form = $('<form id="deleteitem"></form>');
        $hidden = $(el).closest('.prod-details').find('.hiddenfields');
        $normal = $hidden.find('input.normal');
        $dataline = $hidden.find('input.dataline');
        $normal.appendTo($form);
        $dataline.appendTo($form);
        lineid = $normal.filter('[name="line-id"]').val();
        numberStart = parseInt($normal.filter('[name="numberstart"]').val(), 10);
        quantity = parseInt($normal.filter('[name="quantity"]').val(), 10);
        template = '<input name="oirfnbr-id-#{lineid}" value="#{i}"/>';
        for (i = numberStart, l = numberStart + quantity; i < l; i++) {
          template.replace('#{lineid}', lineid).replace('#{i}', i);
          $(template).appendTo($form);
        }
        callback($form);
      },

      /**
       * Remove an item from the minicart. Populate the product ids from the cache,
       * create the a new form from the product's hiddenInputs div, and submit
       * the form. This ajax call must use async: false due to it being a
       * rediret. This blocks the execution of the code leading to an
       * interesting DOM related issue in IE and Chrome - the button does not
       * disable until after the ajax call which isn't particularly responsive.
       * Strangely, this doesn't happen in Firefox. To counteract this problem,
       * a brief timeout has been inserted.
       * @param   {Node}  el  Item to be removed
       */
      removeItem: function (el) {
        var _this;
        _this = this;
        this.disableButton(el, 'Removing', 'anchor');
        this.populateProductIds();
        setTimeout(function () {
          _this.createItemRemovalForm(el, function (form) {
            $.ajax({
              type: 'POST',
              async: false,
              url: '/bin/venda',
              data: form.serialize(),
              success: function (html) {
                _this.loadMinicartHtml(null, null, function () {
                  _this.setPosition('removed');
                  _this.updateTotals(html);
                });
              }
            });
          });
        }, 10);
        return this;
      },

      /**
       * Runs through the necessary steps if this is a multi-product; adding
       * and initialising new checkboxes, then resetting them.
       * @param   {String}  type  The attributeForm id / page type
       */
      checkMultipage: function (type) {
        var check;
        check = this.multiTest.contains(type);
        this.multiFormType = type;
        if (check) {
          this.multiForm = check;
          this.addCheckAllBox();
          this.initCheckboxes();
          this.toggleAllProducts(false);
        }
        return this;
      },

      /**
       * Displays the checkboxes in a multi-product page.
       * @return  {[type]}  [description]
       */
      initCheckboxes: function () {
        $('.js-addToCheckBoxLabel').css('display', 'block');
        $('.js-addToCheckBox').each(function () {
          $(this).removeAttr('checked');
        });
        $('#checkAllProducts').removeAttr('checked');
        return this;
      },

      /**
       * Add the checkAll box in a multi-product page.
       */
      addCheckAllBox: function () {
        var text, label, template;
        text = $('#attributes-addAllProduct').text();
        template = this.template('multicheckbox');
        label = util.applyTemplate(template, { text: text });
        $('.js-buyControlsMulti').prepend(label);
        return this;
      },

      /**
       * Return an object containing the id and unique id for a checkbox
       * depending on multipage-type.
       * @param   {Node}  el  Element
       * @return  {Object}  el  Element id and unique id
       */
      getCheckboxUid: function (el) {
        var id, uid, type, $el;
        $el = $(el);
        id = $el.attr('id');
        type = this.multiFormType;
        if (type !== 'productdetailMulti') {
          uid = $el.closest('.js-oneProduct').attr('id').substr(11);
        } else {
          uid = id;
        }
        return { id: id, uid: uid };
      },

      /**
       * Enables a checkbox and its attributes.
       * @param   {Node}  el  Element
       */
      enableCheckbox: function (el) {
        var obj;
        obj = this.getCheckboxUid(el);
        this.checkboxes[obj.uid] = true;
        $('#qtylist_' + obj.id).prop('disabled', false);
        if (el) {
          $(el).prop('checked', true);
        } else {
          $('.js-addToCheckBox').prop('checked', true);
        }
        return this;
      },

      /**
       * Disables a checkbox and its attributes.
       * @param   {Node}  el  Element
       */
      disableCheckbox: function (el) {
        var obj;
        obj = this.getCheckboxUid(el);
        this.checkboxes[obj.uid] = false;
        $('#qtylist_' + obj.id).prop('disabled', true);
        if (el) {
          $(el).prop('checked', false);
        } else {
          $('.js-addToCheckBox').prop('checked', false);
        }
        return this;
      },

      /**
       * Checks the status of the checkAll box after a checkbox has been
       * enabled or disabled. If the checkboxes are all checked/unchecked,
       * check/uncheck the box.
       */
      checkCheckAllBox: function () {
        var prop, check;
        check = true;
        for (prop in this.checkboxes) {
          if (this.checkboxes.hasOwnProperty(prop)) {
            if (!this.checkboxes[prop]) { check = false; }
          }
        }
        if (check) {
          $('#checkAllProducts').attr('checked', true);
        } else {
          $('#checkAllProducts').removeAttr('checked');
        }
        return this;
      },

      /**
       * Returns a boolean on whether there are any selected checkboxes.
       * @return {Boolean} Are there any checkboxes checked.
       */
      itemsSelected: function () {
        var prop, check;
        check = false;
        for (prop in this.checkboxes) {
          if (this.checkboxes.hasOwnProperty(prop)) {
            if (this.checkboxes[prop]) { check = true; }
          }
        }
        return check;
      },

      /**
       * If the checkAll box is checked/unchecked, check/uncheck all of the
       * product checkboxes.
       * @param  {[type]} box   Either the checkAll box, or false if initialising.
       */
      toggleAllProducts: function (box) {
        var $checkboxes, $box, _this, checked;
        _this = this;
        $checkboxes = $('input.js-addToCheckBox');
        if (!box) {
          $checkboxes.each(function () { _this.disableCheckbox(this); });
        } else {
          $box = $(box);
          checked = $box.prop('checked');// === 'checked' ? true : false;
          if (checked) {
            $checkboxes.attr('checked', true);
            $checkboxes.each(function () { _this.enableCheckbox(this); });
          } else {
            $checkboxes.prop('checked', false);
            $checkboxes.each(function () { _this.disableCheckbox(this); });
          }
        }
        return this;
      },

      /**
       * Copied wholesale from the original minicart script and refactored to
       * improve line-length.
       * @param  {DOM node} uID       ID of the element.
       * @return {Boolean}  isValid   Is the selection valid.
       */
      validateRoutine: function (uID) {
        var i, l, arr, att1, att2, att3, att4, isValid;
        isValid = false;
        if (Atts.productArr.length === 0) {
          isValid = true;
        } else {
          switch (this.multiFormType) {
            case 'productdetail':
            case 'quickBuyFast':
            case 'quickBuyDetails':
            case 'productdetailSet':
            case 'productdetailSet2':
            case 'productset':
            case 'quickShop':
              for (i = 0, l = Atts.productArr.length; i < l; i++) {
                if ($('#oneProduct_'+uID+' #stockstatus').length > 0){
                  arr = Atts.productArr[i];
                  att1 = arr.attSet.att1.selected;
                  att2 = arr.attSet.att2.selected;
                  att3 = arr.attSet.att3.selected;
                  att4 = arr.attSet.att4.selected;
                  if (uID === arr.attSet.id) {
                    if (Atts.IsAllSelected(att1, att2, att3, att4, uID)) {
                      switch (Atts.Get('stockstatus')) {
                        case 'Out of stock':
                          alert($('#attributes-stockOut').text());
                          isValid = false;
                          break;
                        case 'Not Available':
                          alert($('#attributes-stockNA').text());
                          isValid = false;
                          break;
                        default:
                          isValid = true;
                          break;
                      }
                    }
                  }
                } else{
                  isValid = true;
                }
              }
              break;
          }
        }
        return isValid;
      },

      /**
       * Calls the validation routine if the product is a multiproduct.
       * @return {Boolean}      Is the selection valid.
       */
      validate: function (ele) {
        var _this, arr, uID;
        _this = this;
        arr = [];
        if (!$(ele).parents('form').valid()) { return false; }
        if (this.multiForm && !this.itemsSelected()) { return false; }
        if ($('.js-attributesForm').length === 0) { return true; }

        if($(ele).is('.js-addproduct-multi')) {
          $('.js-oneProduct').each(function () {
            uID = $(this).attr('id').substr(11);
            arr.push(_this.validateRoutine(uID));
          });
        } else {
          uID = $(ele).attr('id');
          arr.push(_this.validateRoutine(uID));
        }
        if (arr.contains(false)) { return false; }
        return true;
      },

      /**
       * Shows or hides the minicart detail depending on what what device
       * the customer is using.
       */
      updateDisplay: function () {
        var isDesktop;
        this.device = util.checkDevice();
        isDesktop = this.largeDevices.contains(this.device);
        if (isDesktop) {
          this.setPosition('resize');
          $(this.node.el).fadeIn(this.duration1);
        } else {
          $(this.node.el).fadeOut(this.duration1);
        }
      },

      hoverBasketSection: function () {
        //console.log($('.js-addproduct').attr('disabled'));
        if (!$('.js-addproduct').attr('disabled')) {
          var _this = this;
          if (this.showMinicartDetail === 'hover') {
            if (this.visible && !this.pending) {
              clearTimeout(this.timerout);
            }
            if (!this.visible && !this.pending) {
              this.pending = true;
              this.timerin = setTimeout(function () {
                _this.toggleVisibility();
              }, this.duration4);
            }
          }
        }
      },

      unhoverBasketSection: function () {
        var _this = this;
        if (this.pending) {
          clearTimeout(this.timerin);
          this.pending = !this.pending;
        } else {
          this.timerout = setTimeout(function () {
            if (_this.visible
                && !_this.pending
                && _this.showMinicartDetail === 'hover') {
              _this.pending = true;
              _this.toggleVisibility();
            }
          }, this.duration4);
        }
      },

      /**
       * This is a revealing module and as such we can limit its API to
       * whatever methods we choose.
       * @return {object} API methods
       */
      revealAPI: function () {
        return {
          minicartLoaded: this.minicartLoaded,
          loadMinicartHtml: this.loadMinicartHtml.bind(this),
          checkMultipage: this.checkMultipage.bind(this)
        };
      }

    };

    $(window).on('load', function () {
      $('.js-minicart a').on('click');
      $(this).bind('resize', function () {
        if (MinicartDetail.visible) { MinicartDetail.updateDisplay(); }
      });

      if (jQuery("#box-basket").length > 0) {
        if (jQuery("#box-basket .basketholder li").length > 3) {
          jQuery('.minicartDetailWrapper-checkout').addClass('minicartDetailWrapper');
          MinicartDetail.showMinicart();
        } else {
          jQuery('.minicart-inner .scroll').hide();
          jQuery('.minicart-inner').css('overflow', 'hidden');
        }
          jQuery("#box-basket").css('visibility', 'visible');
      }
    });

    $(document).ready(function () {
      $('.js-minicart a').off('click');

      var formType;

      formType = $('.js-attributesForm').attr('id');
      MinicartDetail.checkMultipage(formType);

      $(document)
        .on('touchstart', '.buttonUp', function () { MinicartDetail.scrollStart('up'); })
        .on('touchstart', '.buttonDown', function () { MinicartDetail.scrollStart('down'); })
        .on('touchend', '.buttonDown, .buttonUp', function () { MinicartDetail.scrollStop(); })
        .on('mousedown', '.buttonUp', function () { MinicartDetail.scrollStart('up'); })
        .on('mousedown', '.buttonDown', function () { MinicartDetail.scrollStart('down'); })
        .on('mouseup', '.buttonDown, .buttonUp', function () { MinicartDetail.scrollStop(); })
        .on('click', '.js-offCanvasClose', function () { MinicartDetail.toggleVisibility(); })
        .on('click', '#continue', function () {
          MinicartDetail.removeNotify();
          MinicartDetail.resetForm();
        });

      $('#basketSection').click(function (e) {
        if (MinicartDetail.showMinicartDetail === 'click') {
          e.preventDefault();
          MinicartDetail.toggleVisibility();
        } else {
          return true;
        }
      });

      $('#basketSection').hover(function () {
        MinicartDetail.hoverBasketSection();
      }, function () {
        MinicartDetail.unhoverBasketSection();
      });

      $(document).on('click', function (e) {
        if ($('#basketSection').length > 0) {
          var nodeInBasket = $.contains($('#basketSection')[0], e.target);
          if (!nodeInBasket
              && MinicartDetail.visible
              && MinicartDetail.showMinicartDetail === 'click') {
            MinicartDetail.toggleVisibility();
          }
        }
      });

      $(document).on('keypress', '#qty', function (e) {
        if (e.which === '13') {
          $('.js-addproduct').trigger('click');
          return false;
        }
      });

      $(document).on('click', '.js-addproduct, .js-buynow', function (e) {
        var $productStatus;
        if (MinicartDetail.validate(this)) {
          if ($(this).is('.js-addproduct')) {
            e.preventDefault();
            MinicartDetail.addProduct(this);
          }
        } else {
          e.preventDefault();
          $productStatus = $('.js-stockFeedbackBox #productstatus');
          if ($productStatus.length > 0 && $productStatus.is(':visible')) {
            $productStatus.effect('pulsate', { times: 2 }, 700);
          }
          return false;
        }
      });

      $(document).on('change', '#addproductform select', function () {
        if ($('#addproductform #notify').length > 0) { MinicartDetail.removeNotify(); }
      });

      $(document).on('click', '#addproductform .js-attributeSwatch', function () {
        if ($('#addproductform #notify').length > 0) { MinicartDetail.removeNotify(); }
      });

      $('.minicartDetailWrapper').on('click', '.js-removeItem', function (e) {
        e.preventDefault();
        MinicartDetail.removeItem(this);
      });

      $(document).on('click', '#checkAllProducts', function () {
        MinicartDetail.toggleAllProducts(this);
      });

      $(document).on('click', '.js-addToCheckBox', function () {
        var checked;
        checked = $(this).prop('checked');
        if (checked) {
          MinicartDetail.enableCheckbox(this);
        } else {
          MinicartDetail.disableCheckbox(this);
        }
      });

    });

    return MinicartDetail.init(options).revealAPI();

  }

  global.Venda = global.Venda || {};
  global.Venda.MinicartDetail = defineModule(global.Venda, global.jQuery);

}(this);
