/*global Venda, jQuery, alert, console, $ */
/*jslint browser: true */
/*jslint white: true */
/*jslint bitwise: true */
/*jslint plusplus: true */
/*jslint indent: 2 */
/*jslint maxlen: 100 */
/*jslint vars: false */
/*jslint todo: true */

/**
 * Venda.Widget.HandleSection.js
 * This script provides us with the ability to hide tab if content is empty.
 * It will be handle after the foundation.section is loaded.
 * ---------
 * REVISIONS
 * ---------
 * 221013 - Bow <bowc@venda.com> - Modified from CreateTab.js
 * Legacy - Auii <sakp@venda.com> - Created
 * ---------
 * DEPENDANCIES
 * ---------
 * jQuery.1.10.2.js, foundation.section.js.
 * ---------
 */

(function (Venda, $) {
    "use strict";

    /**
     * Set up the namespace.
     */
    Venda.namespace('Widget.HandleSection');

    /**
    * Init globals.
    */
    Venda.Widget.HandleSection.tabID = '';
    Venda.Widget.HandleSection.tabNav = null;
    Venda.Widget.HandleSection.tabElement = null;
    Venda.Widget.HandleSection.totalHide = null;
    Venda.Widget.HandleSection.activeIndex = 0;

    /**
     * Sets up the tab
     * @param  {Object} self - refers to a DOM element
     */
    Venda.Widget.HandleSection.init = function (self) {
        this.tabID = $(self).attr('id');
        this.tabNav = $(self).find('.title');
        this.tabElement = $(self).find('.content');

        this.hideEmptyTab('#' + this.tabID);
        if (this.totalHide > this.getTotalTab()) {
            $(this.tabID).hide();
        }
        this.checkIEversion('#' + this.tabID);
    };

    /**
     * Hiding if content is empty
     * @param  {String} ID - A tab id
     */
    Venda.Widget.HandleSection.hideEmptyTab = function (ID) {
        var totalHide = 0, activeTab = 0, txt = '', setNewactiveTab = false;

        $(ID).find('.content').each(function (index) {
            txt = $(ID).find('.content').eq(index).text();
            txt = $.trim(txt);
            if (0 === txt.length) {
                totalHide++;
                $(ID).find('.title').eq(index).hide();
                $(ID).find('.content').eq(index).hide();
                if (index === activeTab) {
                    setNewactiveTab = true;
                }
            }
        });
        if (setNewactiveTab) {
            this.activeIndex = $(ID).find('.title:visible').index(ID + ' .title');
            this.setActiveTab(this.activeIndex);
        }
        this.totalHide = totalHide;
    };

    /**
     * Setting an active tab
     * @param {Integer} index - get the first visible tab
     */
    Venda.Widget.HandleSection.setActiveTab = function (index) {
        this.tabNav.parent().removeClass('active');
        this.tabNav.eq(index).parent().addClass('active');
    };

    /**
     * Get the tab number
     * @return {Integer} tab amount
     */
    Venda.Widget.HandleSection.getTotalTab = function () {
        return this.tabNav.length;
    };

    /**
     * Adjust IE lower than 9 behavior and force a particular format for 'tabs' section
     * @param  {String} ID - A tab id
     */
    Venda.Widget.HandleSection.checkIEversion = function (ID) {
        if ($('html').hasClass('lt-ie9')) {
            $(ID).removeClass('auto').addClass('tabs');
            $(ID).attr('data-section', 'tabs');
            $(ID + ' .section').each(function () {
                if(!$(this).is('.active')) {
                    $(this).addClass('nopad');
                }
            });
            $(document).foundation('section', 'reflow');
        }
    };

    /**
     * Set up the DOM events.
     */
    $(document).ready(function () {
        if(jQuery('.section-container').length > 0) {
            jQuery('.section-container').each(function() {
                Venda.Widget.HandleSection.init(this);
            });
        }
    });
}(Venda, jQuery));