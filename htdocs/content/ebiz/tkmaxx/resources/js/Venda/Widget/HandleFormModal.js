/**
 * Venda.Widget.HandleFormModal.js
 * ---------
 * REVISIONS
 * ---------
 * Legacy - May <mayk@venda.com> - Created
 * ---------
 * DEPENDANCIES
 * ---------
 * jQuery.1.10.2.js
 * ---------
 */

/**
 * Set up the namespace.
 */
Venda.namespace('Widget.HandleFormModal');

Venda.Widget.HandleFormModal.submitForm = function(formName) {
    var obj = jQuery('form[name='+formName+']');
    var URL = Venda.Ebiz.doProtocal(obj.attr('action'));
    obj.find('input[name="layout"]').val('noheaders');
    var params = obj.find("input, select, textarea").serialize();/* get the value from all input type*/
    jQuery.ajax({
        type : "GET",
        url : URL,
        dataType : "html",
        data : params,
        cache : false,
        error : function () {
            jQuery(".js-modalContent").html('Error!');
        },
        success : function (data) {
            jQuery(".js-modalContent").html(data);
        }
    });
    return false;
};