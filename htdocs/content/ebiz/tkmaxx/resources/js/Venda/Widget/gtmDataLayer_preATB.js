/*global Venda, jQuery, $, dataLayer */

/**
 * @fileoverview gtmDataLayer.js - DataLayer for Google Tag Manager. See https://developers.google.com/tag-manager/reference
 *
 * @requires /resources/js/Venda/Widget/Tracking.js
 * @requires /resources/js/external/jquery-1.8.3.min.js (http://api.jquery.com)
 * @requires /resources/js/external/modernizr.js - with localStorage included in build (http://modernizr.com)
 * @requires /resources/js/Venda/Search.js
 * @requires /resources/js/Venda/Widget/MinicartDetail.js
 * @requires /templates/tracking/gtmSnippet
 *
 * @author Oliver Secluna <oliversecluna@venda.com>
 */

(function (Venda, $) {

  /*Wrapper function ensures variables are not global*/
  var step, ord, items, user, srch, sess, load3DS,icaturl;
  step = Venda.Widget.Tracking.Step();
  ord = Venda.Widget.Tracking.orderJSON;
  items = Venda.Widget.Tracking.orditemsArray;
  user = Venda.Widget.Tracking.Ses();
  srch = Venda.Widget.Tracking.Search();
  load3DS = false;
  icaturl = '/icat/privatesale';


  if (step) {

    /*If this is a workflow step*/
    dataLayer.push({
      'event': 'pageURL',
      'pageURL': step.replace('//','/')
    });
  }

    /*If this is /icat/privatesale*/
    if ($('body#icat_privatesale').length > 0) {
      if (user.ustype == 'Registered') {
          icaturl = icaturl + '-listing';
      }
      else {
        icaturl = icaturl + '-welcome';
      }
      dataLayer.push({
        'event': 'pageURL',
        'pageURL': icaturl
      });
    }



  if (typeof ord !== 'undefined') {

    var i, gtmItems, atts, cat, pay;

    /*If an order has been placed. Create array for order items*/
    gtmItems = [];

    for (i = 0; i < items.length; i++) {

      /**
       * Loop through order items
       *
       * Google Analytics has a single column for product variant data
       * Send as categoryref: attribute values brand product type giftwrap
       */
      prodname = items[i].name,
      atts = items[i].attributes,
      cat = items[i].type;

      /* Append attributes to product name */
      if (atts.att1 !== '') {prodname += ' ' + atts.att1; }
      if (atts.att2 !== '') {prodname += ' ' + atts.att2; }
      if (atts.att3 !== '') {prodname += ' ' + atts.att3; }
      if (atts.att4 !== '') {prodname += ' ' + atts.att4; }

      /* Custom to add giftwrap info to product name */
      if (items[i].giftwrap !== '') {prodname += ' Gift Wrapped (' + items[i].giftwrap + ')';}

      /* Add brand to category name */
      if (items[i].brand) {cat += ' by '+ items[i].brand; }

      /* Add "in" + category to category name */
     cat += ' in ' + items[i].category;

      var obj = {
        'id': items[i].sku,
        'sku': items[i].sku,
        'name': prodname,
        'category': cat,
        'price': items[i].price,
        'discount': items[i].discount,
        'quantity': items[i].qty
      };
      gtmItems.push(obj);
    }

    pay = ord.payment;
    switch (ord.payment) {

      /* Translate venda_ohpaytype values into English*/
      case '0':
        pay = 'Credit Card';
        break;
      case '5':
        pay = 'Manual Payment';
        break;
      case '8':
        pay = 'Free Order';
        break;
      case '12':
        pay = 'PayPal';
        break;
      default:
        pay = 'Payment Type' + pay;
    }

    dataLayer.push({
      'event': 'transaction',
      'transactionId': ord.ref,
      'transactionAffiliation': ord.store,
      'transactionTotal': ord.total,
      'transactionTax': ord.tax,
      'transactionShipping': ord.shipping,
      'transactionDiscount': ord.discount,
      'transactionPaymentType': pay,
      'transactionCurrency': ord.currency,
      'transactionProducts': gtmItems,
      'transactionQTY': gtmItems.length
    });
  }

  /*Track Session data*/
  sess = {
      'event': 'updateUser',
      'Language': user.lang,
      'Region': user.locn,
      'visitorGroup': user.group,
      'visitorId': user.usid,
      'visitorType': user.ustype
    };

  /* Use session storage to ensure session data is only sent to the dataLayer once per session, or when it changes */
  if (Modernizr.sessionstorage) {

    /* window.localStorage is available */
    var stored = sessionStorage.gtmSess;
    if (!stored) {

      /* new session data */
      dataLayer.push(sess);
      sessionStorage.gtmSess = JSON.stringify(sess);
    } else {

      /* stored session data */
      if (JSON.stringify(sess) !== stored) {

        /* session data has changed */
        dataLayer.push(sess);
        sessionStorage.gtmSess = JSON.stringify(sess);
      }
    }
  } else {

    /* window.localStorage is not available so send session data always */
    dataLayer.push(sess);
  }

  /* Track Search (N.B. not required for Google Analytics) */
  $('#content-search').bind('search-loading-end', function () {
    dataLayer.push({
        'event': 'sitesearch',
        'siteSearchTerm': srch.term,
        'siteSearchResults': srch.results
      });
  });

  /* Track jQuery AJAX requests. See www.alfajango.com/blog/track-jquery-ajax-requests-in-google-analytics*/
  $(document).ajaxSend(function (event, xhr, settings) {
         if ((settings.url != '/page/irecsdisplay') && (settings.url != '/bin/venda?ex=co_wizr-checkout&step=ordersummary')) {
    dataLayer.push({
      'event': 'pageURL',
      'pageURL': settings.url
    });
  }
  });

  /*Track Add to Cart*/
  $('body').bind('minicart-items-added', function () {
    dataLayer.push({
      'event': 'gaEvent',
      'eventCategory': 'Shopping',
      'eventAction': 'Add to Cart',
      'eventLabel': $('#tag-invtname').text(),
      'Cart':'Created'
    });
  });

  /* Track the promotion code when applied */
  $('body').bind('promotion-applied', function () {
    dataLayer.push({
      'event': 'gaEvent',
      'eventCategory': 'Promotional code',
      'eventAction': 'Promotion applied',
      'eventLabel': jQuery.trim(jQuery('#vcode').val())
    });
  });


/* Track homepage boxes as Events - used for Google Analytics event tracking */
  $('body').on('click', '[data-trackHomePage] a', function (ev) {
    var nav, action, label;
    nav = $(ev.target).closest('[data-trackHomePage]'); // Find the parent
    action = nav.attr('data-trackHomePage'); // Get the value of the data- param
    if (this.host !== window.location.host) {
      label = this.href; // For an external link record the absolute url
    } else {
      label = this.pathname; // For an internal link record the relative path
    }
    //alert('event: gaEvent, eventCategory: HomePageBoxes, eventAction: '+action+', eventLabel: '+label)
    dataLayer.push({
      'event': 'gaEvent',
      'eventCategory': 'HomePageBoxes',
      'eventAction': action,
      'eventLabel': label
    });
  });

/* Track Attraqt Facet Filtering as Events - used for Google Analytics event tracking */
  $('body').on('click', '[data-role="fsmfacetitem"]', function (ev) {
    
    var nav, action, label;
    //nav = $(ev.target).closest('[data-trackHomePage]'); // Find the parent
    
    action=$('#parentCatRef').text()+'_'+$(ev.target).attr('data-title');
    label =$(ev.target).attr('data-value'); // Get the value of the data- param
    
 
    //alert('event: gaEvent, eventCategory: FacetFilter, eventAction: '+action+', eventLabel: '+label)
    
    dataLayer.push({
      'event': 'gaEvent',
      'eventCategory': 'FacetFilter',
      'eventAction': action,
      'eventLabel': label
    });
  });

/* Track PLP invt clicks to see if they have size facet filtering*/
  $('body').on('click', '.prod-search-results a', function (ev) {
    	var action, label;
    	if(window.location.href.indexOf("icat") > -1) {
     		action=$('#parentCatRef').text();	
     		label = $('#term a').attr('data-field')=='Size';
     		dataLayer.push({
      			'event': 'gaEvent',
      			'eventCategory': 'PLPFacetSizeSelection',
      			'eventAction': action,
      			'eventLabel': label
    		});
		}
  });



  /* Track all links within a container as Events - used for Google Analytics event tracking */
  $('body').on('click', '[data-trackNav] a', function (ev) {
    var nav, category, label;
    nav = $(ev.target).closest('[data-trackNav]'); // Find the parent
    category = nav.attr('data-trackNav'); // Get the value of the data- param
    if (this.host !== window.location.host) {
      label = this.href; // For an external link record the absolute url
    } else {
      label = this.pathname; // For an internal link record the relative path
    }
    dataLayer.push({
      'event': 'gaEvent',
      'eventCategory': category,
      'eventAction': 'Navigation',
      'eventLabel': label
    });
  });

  /* Track click on any element as Event - used for Google Analytics event tracking  */
  $('body').on('click', '[data-trackEvent]', function (ev) {
    var btn, category, tag, label, action;
    btn = $(ev.target);
    category = btn.attr('data-trackEvent');
    tag = btn.prop('tagName');

    switch (tag) {
    case 'IMG':
      label = btn.attr('src');
      action = 'Image';
      break;
    case 'A':
      label = btn.text();
      action = 'Link';
      break;
    case 'INPUT':
      label = btn.val();
      action = 'Input';
      break;
    default:
      label = btn.attr('id');
      action = tag;
    }
    dataLayer.push({
      'event': 'gaEvent',
      'eventCategory': category,
      'eventAction': action,
      'eventLabel': label
    });
  });

  // Track 3Dsecure iframe
  $('#iframe').load(function () {
    if (!load3DS) {
      load3DS = true;
      dataLayer.push({
        'event': 'gaEvent',
        'eventCategory': '3DSecure',
        'eventAction': 'load3DSecure',
        'eventLabel': 'iFrame Document'
      });
    } else {
      dataLayer.push({
        'event': 'gaEvent',
        'eventCategory': '3DSecure',
        'eventAction': 'bankResponse',
        'eventLabel': 'iFrame Document'
      });
    }
  });

}(window.Venda || {}, jQuery));
