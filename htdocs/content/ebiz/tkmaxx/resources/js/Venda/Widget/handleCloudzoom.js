/**
* @fileoverview Venda.Widget.handleCloudzoom
 * This script provides us with the ability to enable/disable Cloudzoom functionality based on screen size
 * Documentation: https://docs.venda.com/x/AQBWB
 * @author Arunee Keyourawong (May) <mayk@venda.com>
 * @edited Issawararat Chumchinda (Bow) <bowc@venda.com>
*/

Venda.namespace('Widget.handleCloudzoom');

Venda.Widget.handleCloudzoom.switched = true;
Venda.Widget.handleCloudzoom.setCloudzoom = function() {
    if ((jQuery('.js-quickbuyDetails').length > 0) || (Modernizr.mq('only screen and (max-width: 997px)') && !Venda.Widget.handleCloudzoom.switched)) {
        // Disable cloudzoom on a small screen and Quickbuy function
        Venda.Widget.handleCloudzoom.switched = true;
        if(jQuery('#productdetail-image .cloudzoom').hasClass('activeZoom')){
            jQuery('#productdetail-image .cloudzoom').data('CloudZoom').destroy();
            jQuery('#productdetail-image .cloudzoom').removeClass('activeZoom');
            jQuery('#vModal').removeClass('small').addClass('large');
        }
        jQuery(swipeEvent);
    } else if (Venda.Widget.handleCloudzoom.switched && Modernizr.mq('only screen and (min-width: 998px)')) {
        // Enable cloudzoom on a large screen
        jQuery('#vModal').removeClass('large').addClass('small');
        Venda.Widget.handleCloudzoom.switched = false;
        if(Venda.Attributes.howManyLargeImgs > 0) {
            jQuery('#productdetail-image .cloudzoom').addClass('activeZoom');
            jQuery('.js-productdetail-swipe').hide();
            var options = {
                zoomSizeMode: 'zoom',
                zoomPosition: 3,
                variableMagnification: false,
                disableZoom: 'auto',
                zoomFlyOut: false,
                tintColor: "transparent",
                zoomOffsetX: 75
            };

            jQuery('#productdetail-image .cloudzoom, #productdetail-altview .cloudzoom-gallery').CloudZoom(options);

            jQuery('#productdetail-altview .cloudzoom-gallery').on('click', function() {
                jQuery("#productdetail-viewlarge").html('<a href="#" onclick="javascript: Venda.Attributes.ViewLargeImg(' + Venda.Attributes.imgParam + ', ' + jQuery(this).data('itemnum')+ ');">View Large Image</a>');
            });

            jQuery('#productdetail-image').bind('cloudzoom_ready',function(){
                jQuery(this).css('min-height', jQuery('#productdetail-image img').height());
            });
        }
    }
};

Venda.Widget.handleCloudzoom.ViewAlternativeImg = function(obj) {
    jQuery('#productdetail-altview').find('a').on('click', function(event) {
        event.preventDefault();
        var strJSON = '{'+ jQuery(this).attr('data-cloudzoom') +'}';
        var objJSON = eval("(function(){return " + strJSON + ";})()");
        jQuery(objJSON.useZoom).attr('src', objJSON.image);
        jQuery.each(jQuery('#productdetail-altview a'),function(){
            jQuery(this).removeClass('cloudzoom-gallery-active');
        });
        jQuery(this).addClass('cloudzoom-gallery-active');
        jQuery("#productdetail-viewlarge").html('<a href="#" onclick="javascript: Venda.Attributes.ViewLargeImg(' + Venda.Attributes.imgParam + ', ' + jQuery(this).data('itemnum')+ ');">View Large Image</a>');
    });
};
jQuery(window).load(function(){
    if(jQuery('html').hasClass('lt-ie9')) {
        var options = {zoomSizeMode: 'image',zoomPosition: 3,variableMagnification: false,disableZoom: 'auto',zoomFlyOut: false};
        jQuery('.cloudzoom, .cloudzoom-gallery').CloudZoom(options);
    }

	Venda.Widget.handleCloudzoom.setSwitched();
	//jQuery('#productdetail-altview').jqSlider({isTouch: Modernizr.touch,sliderEnable: 1});
	Venda.Widget.handleCloudzoom.setCloudzoom();
	jQuery(window).on('resize',function(){
        jQuery('.cloudzoom-blank').detach();
        Venda.Widget.handleCloudzoom.setCloudzoom();
	});
    jQuery('#productdetail-image').on('click', function(){
        jQuery('#productdetail-viewlarge a').trigger('click');
    });
});
Venda.Widget.handleCloudzoom.setSwitched = function() {
	if (Modernizr.mq('only screen and (min-width: 998px)')) {
		Venda.Widget.handleCloudzoom.switched = true;
	} else  if (Modernizr.mq('only screen and (max-width: 997px)')) {
		Venda.Widget.handleCloudzoom.switched = false;
	}
};
// Events
var swipeEvent = function(){
    var objAltview = jQuery('#productdetail-altview');
    var howManyAlt = parseInt(objAltview.find('a').length);
    var itemnum = (objAltview.find('a.cloudzoom-gallery-active').data('itemnum')) ? objAltview.find('a.cloudzoom-gallery-active').data('itemnum') : 0;

    if(howManyAlt <= 1){
        jQuery('.js-productdetail-swipe').hide();
        return false;
    } else {
        jQuery('.js-productdetail-swipe').fadeIn();
        setTimeout('jQuery(".swipetext").fadeOut();',3000);
        Venda.Widget.handleCloudzoom.ViewAlternativeImg();
        var current = itemnum;
        jQuery('.js-productdetail-swipe').swipe({
            swipeLeft: function(){
                ++current;
                current = (current >= howManyAlt) ? 0 : current;
                itemnum = current;
                objAltview.find('a').eq(current).trigger('click');
                return false;
			},
			swipeRight: function(){
                --current;
                current = (current < 0) ? howManyAlt-1 : current;
                itemnum = current;
                objAltview.find('a').eq(current).trigger('click');
                return false;
			},
            click: function(){
                jQuery(".swipetext").hide();
            }
        });
    }
};