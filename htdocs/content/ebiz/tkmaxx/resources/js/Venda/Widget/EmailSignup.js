/**
 * Venda.Widget.EmailSignup.js
 *
 * ---------
 * REVISIONS
 * ---------
 * Legacy
 * ---------
 * DEPENDANCIES
 * ---------
 * jQuery.1.10.2.js
 * ---------
 */

//create namespace
Venda.namespace('Widget.EmailSignup');

Venda.Widget.EmailSignup.checkEmail = function() {
	var userEmail = Venda.Platform.getUrlParam(location.href, 'email');
	var displyEmail = Venda.Platform.escapeHTML(userEmail);
	var sesUsEmail = jQuery('#sesUsEmailDiv').html();
	var userType = jQuery('#userType').html();
	if ((sesUsEmail == userEmail) || (sesUsEmail == '')) {
		document.emailsonly.usemail.value = userEmail;
		document.getElementById("newsignup").style.display ="block";
		document.getElementById("newsignupemail").innerHTML =  displyEmail ;
	} else {
		document.emailsonly.usemail.value = userEmail;
		document.emailsonly.log.value = '4';
		if (userType != 'G') {
			document.getElementById("alreadysignup").style.display ="block";
			document.getElementById("sesUsEmail").innerHTML =  sesUsEmail ;
			document.getElementById("displyEmail").innerHTML =  displyEmail ;
		} else {
			document.getElementById("newsignup").style.display ="block";
			document.getElementById("newsignupemail").innerHTML =  displyEmail ;
		}
	}
};

/**
 * Set up the DOM events.
 */
jQuery(function() {
	 if(jQuery('#emailSignup').length > 0) {
        Venda.Widget.EmailSignup.checkEmail();
    }
});