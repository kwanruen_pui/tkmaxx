/**
* @fileoverview Venda.storeloc
 * Venda's storeloc storelocator: This functionlaity  is a storelocator for store search and DTS
 *
 * @requires js/external/jquery-1.7.2.min.js
 * @requires js/external/jquery-tmpl-min.js
 * @requires js/external/jquery-ui-1.8.14.custom.min.js
 * @requires Google Maps API
 *
 * @author Alby Barber <abarber@venda.com>
*/

Venda.namespace('storeloc');
Venda.storeloc = function () {};
var errorMsg = "";
	var directionsDisplay = new google.maps.DirectionsRenderer();
	var directionsService = new google.maps.DirectionsService();

jQuery(document).ready(function() {

	/**
	* Config options
	*/
	Venda.storeloc.unitsInMiles = true;

	/**
	* Declares global vars
	*/
	Venda.storeloc.limit = 10;
	Venda.storeloc.storeDetailNearestLimit = 10;
	Venda.storeloc.maxDistance = 50; // cr unit is mile
	// Conversion rate for METERS to MILES
	Venda.storeloc.METERS_TO_MILES_CONV = 0.000621371192;
	Venda.storeloc.geocoder;
	Venda.storeloc.map;
	Venda.storeloc.marker;
	Venda.storeloc.bounds;
	Venda.storeloc.myLocation;
	Venda.storeloc.mylat;
	Venda.storeloc.mylng;
	Venda.storeloc.stores = [];
	Venda.storeloc.storesTK = [];
	Venda.storeloc.storesHS = [];
	Venda.storeloc.storesQueryTK = [];
	Venda.storeloc.storesQueryHS = [];
	Venda.storeloc.storeMarkers = [];
	Venda.storeloc.storeMarkersTK = [];
	Venda.storeloc.storeMarkersHS = [];
	Venda.storeloc.shownStores = [];
	Venda.storeloc.infowindow = [];
	Venda.storeloc.infowindowTK = [];
	Venda.storeloc.infowindowHS = [];
	Venda.storeloc.infowindowActive = false;
	Venda.storeloc.URL = new Uri(window.location);
	Venda.storeloc.URI;
	Venda.storeloc.naved = false; // This setting is used by the back buttons to work out if it is navigated or not
	Venda.storeloc.queryString = "";
	Venda.storeloc.searchOptionIsTK = false;
	Venda.storeloc.searchOptionIsHS = false;
	Venda.storeloc.markerDirectionsArray = [];
	Venda.storeloc.isRegion = false;
	Venda.storeloc.isAutocompleteThinking = false;
	Venda.storeloc.stepDisplay = new google.maps.InfoWindow({maxWidth:242});
	Venda.storeloc.currentStoreMarker = {};
	/**
	* Sets the marker images to use the desired image e.g. Venda Logo
	* Venda.storeloc.image  - image
	* Venda.storeloc.shadow - image shadow
	* Venda.storeloc.shape  - image shape
	*/
	Venda.storeloc.image = {
		'tk' : new google.maps.MarkerImage(
				'/content/ebiz/tkmaxx/resources/images/marker-images/image.png',
				new google.maps.Size(29,36),
				new google.maps.Point(0,0),
				new google.maps.Point(12,22)
			),
		'hs' : new google.maps.MarkerImage(
				'/content/ebiz/tkmaxx/resources/images/marker-images/hs_image.gif',
				new google.maps.Size(29,36),
				new google.maps.Point(0,0),
				new google.maps.Point(12,22)
			)
	}
	Venda.storeloc.region = {
		'east_on': {
			'regionid':'6',
			'lat': '52.648896',
			'lng' : '0.142822',
			'regionname' : 'East'
		},
		'london_on': {
			'regionid':'13',
			'lat': '51.536086',
			'lng' : '-0.065918',
			'regionname' : 'London'
		},
		'north_ire_on': {
			'regionid':'4',
			'lat': '54.661124',
			'lng' : '-6.674194',
			'regionname' : 'Northern Ireland'
		},
		'northeast_on': {
			'regionid':'5',
			'lat': '54.699234',
			'lng' : '-1.483154',
			'regionname' : 'North East'
		},
		'northwest_on': {
			'regionid':'9',
			'lat': '54.316523',
			'lng' : '-2.669678',
			'regionname' : 'North West'
		},
		'repub_ire_on': {
			'regionid':'3',
			'lat': '53.14677',
			'lng' : '-8.031006',
			'regionname' : 'Republic of Ireland'
		},
		'scotland_on': {
			'regionid':'1',
			'lat': '56.860986',
			'lng' : '-4.141846',
			'regionname' : 'Scotland'
		},
		'southeast_on': {
			'regionid':'12',
			'lat': '51.618017',
			'lng' : '-0.53833',
			'regionname' : 'South East'
		},
		'southwest_on': {
			'regionid':'7',
			'lat': '50.889174',
			'lng' : '-3.438721',
			'regionname' : 'South West'
		},
		'wales_on': {
			'regionid':'8',
			'lat': '52.160455',
			'lng' : '-3.944092',
			'regionname' : 'Wales'
		},
		'west_mid_on': {
			'regionid':'11',
			'lat': '52.456009',
			'lng' : '-1.549072',
			'regionname' : 'West Midlands'
		},
		'yorkshire_on': {
			'regionid':'10',
			'lat': '53.956086',
			'lng' : '-1.043701',
			'regionname' : 'Yorkshire'
		}
	}
/*
	Venda.storeloc.shadow = new google.maps.MarkerImage(
		'/content/ebiz/tkmaxx/resources/images/marker-images/shadow.png',
		new google.maps.Size(37,22),
		new google.maps.Point(0,0),
		new google.maps.Point(12,22)
	);
*/
	Venda.storeloc.shape = {
		coord: [16,0,17,1,18,2,19,3,20,4,21,5,21,6,21,7,22,8,22,9,22,10,22,11,22,12,21,13,21,14,21,15,20,16,20,17,19,18,18,19,16,20,14,21,8,21,6,20,4,19,3,18,3,17,2,16,1,15,1,14,1,13,1,12,0,11,0,10,1,9,1,8,1,7,1,6,2,5,2,4,3,3,4,2,5,1,7,0,16,0],
		type: 'poly'
	};

	jQuery('#storelocatorresults').hide();
	jQuery('#map_canvas').css('opacity',0);
	//jQuery('#mapContent').css('height',0);
	jQuery('#mapContent').addClass('js-mapContent');
	Venda.storeloc.ajaxGetStores('storeloc');
	Venda.storeloc.Initialize();

	jQuery(function() {
		jQuery('head meta[name=viewport]').attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0');
		jQuery.fn.extend({
			propAttr: $.fn.prop || $.fn.attr
		});
		jQuery("#address").autocomplete({
			//This bit uses the geocoder to fetch address values
			autoFocus: false,
			minLength: 3,
			mustMatch:true,
			open: function() {
				$('.ui-menu').width(925);
				$('.ui-menu').width($('#storelocatorform').width());
				Venda.storeloc.isAutocompleteThinking = false;
				clearTimeout(thinkingTimeout);
			},
			source: function(request, response) {
				if($.trim(jQuery("#address").val()) != ""){
				Venda.storeloc.geocoder.geocode( {'address': request.term , 'componentRestrictions':{'country':jQuery("input:radio[name='locationoption']:checked").val().toUpperCase()}}, function(results, status) {
					response(jQuery.map(results, function(item) {
						/**/
						var arrAddress = item.address_components;
						var itemLocality='';
						// iterate through address_component array
						$.each(arrAddress, function (i, address_component) {
							if (address_component.types[0] == "locality"){
								itemLocality = address_component.long_name;
							}
						});
						/**/
						return {
							label:  item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							locality: itemLocality
						}
					}));
				});
				}
			},
			select: function(event, ui) {
				if (jQuery('.selectgroup input[type=checkbox]:checked').length > 0) {
					Venda.storeloc.mylat = ui.item.latitude;
					Venda.storeloc.mylng = ui.item.longitude;
					jQuery('#input-enter_location').text(ui.item.label);
					Venda.storeloc.moveMapToSearchResultPanel();
					Venda.storeloc.checkSearchOption();
					//Venda.storeloc.checkIsRegion();
					/*if (Venda.storeloc.isRegion) {
						Venda.storeloc.ajaxGetStores('storeloc',"yes");
						Venda.storeloc.isRegion = false;
					}*/
					Venda.storeloc.updateMap();
					jQuery('#storelocatorresults').show();
					jQuery('#mapContent').removeClass('js-mapContent');
					jQuery('#map_canvas').animate({ opacity: 1 }, 1000 ,function(){ jQuery('#js-loading').hide();	});
					jQuery('#nav').animate({ "margin-top": "0px"}, 1000);
					setTimeout(function() {Venda.storeloc.updateUriQueries('0');}, 1000);
					Venda.storeloc.map.setCenter(Venda.storeloc.marker.position);
					//Venda.storeloc.infowindow[0].open(Venda.storeloc.map, Venda.storeloc.storeMarkers[0]); //not show infowindow on mobile
					//Venda.storeloc.infowindowActive = true; //not show infowindow on mobile
					Venda.storeloc.switchStoreLocHeader(ui.item.label);
					Venda.storeloc.showMap();
				} else {
					alert(jQuery("#tag-nokeyword").text());
				}
			}
		}).keypress(function(e) {
			if ((jQuery('#address').val().length > 0) && (jQuery('.selectgroup input[type=checkbox]:checked').length > 0)) {
				if (e.keyCode === 13){
					e.preventDefault();
					if (Venda.storeloc.isAutocompleteThinking) {
						jQuery( "#address" ).one( "autocompleteopen", function( event, ui ) {
							Venda.storeloc.moveMapToSearchResultPanel();
							jQuery('.ui-autocomplete li:first-child a').trigger('mouseover').trigger('click');
							Venda.storeloc.showMap();
						} );
					} else {
						Venda.storeloc.moveMapToSearchResultPanel();
						jQuery('.ui-autocomplete li:first-child a').trigger('mouseover').trigger('click');
						Venda.storeloc.showMap();
					}
				}
			} else {
				if (e.keyCode === 13){
					alert(jQuery("#tag-nokeyword").text());
				}
			}
			//Venda.storeloc.modifyAutoCompleteUI();
		}).autocomplete("widget").addClass("ui-autocomplete-custom")/*.data("ui-autocomplete")._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( "<a>" + item.label + "<br>" + item.desc + "</a>" )
			.appendTo( ul );
		};*/

		// Track the URL changes so we can make sure the user back and forward buttons work
		History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
			var State = History.getState(); // Note: We are using History.getState() instead of event.state
			// This does not track the changing of tabs via the back button

			if (!Venda.storeloc.naved) {
				//window.location = jQuery('#tag-ebizurl').text().replace('http:',window.location.protocol) + '/page/storelocator/'
				/*
				if (Venda.storeloc.checkIsRegion) {
					Venda.storeloc.ajaxGetStores('storeloc',false);
				} else {
					Venda.storeloc.restoreState();
				}
				*/
			}
			Venda.storeloc.naved = false;
		});
	});

	var thinkingTimeout;
	$('#address').each(function () {
	    var elem = $(this);
	    // Save current value of element
	    elem.data('oldVal', elem.val());
	    // Look for changes in the value
	    elem.bind("propertychange keyup input paste", function (event) {
	        // If value has changed...
	        if (elem.data('oldVal') != elem.val()) {
	            // Updated stored value
	            elem.data('oldVal', elem.val());

				Venda.storeloc.isAutocompleteThinking = true;
				clearTimeout(thinkingTimeout);
				thinkingTimeout = setTimeout(function() {Venda.storeloc.isAutocompleteThinking = false;}, 1000);
	        }
	    });
	});

	jQuery('#address').bind('paste', function() {
		setTimeout(function() {jQuery('#address').trigger('keydown');}, 100);
	});

	google.maps.event.trigger(Venda.storeloc.map, 'resize');
});

/**
* Uses Ajax to get a list of stores and generate a dropdown containing those stores
* @param{string} stryid is reference of the story category that contains the stores
* @author Alby Barber <abarber@venda.com>
**/
Venda.storeloc.ajaxGetStores = function(stryid,mustReload,do_not_redraw) {
	jQuery('#js-loading').show();
	jQuery('#address_holder').addClass("hide");
	var currentUri = new Uri(window.location);
	//if click regions map link
	if (currentUri.getQueryParamValues('regionid').toString() && !(mustReload)){
		stryid = currentUri.getQueryParamValues('regionid').toString();
		Venda.storeloc.isRegion = true;
		// clear everything to delete all overlay and marker and store list
	} else {
		Venda.storeloc.isRegion = false;
	}
	jQuery.get( jQuery('#tag-ebizurl').text().replace('http:',window.location.protocol) + "/scat/" + stryid + "&temp=storesjson&layout=blank", function(dataString) {
		dataString = dataString.indexOf(',') == 0 ? dataString.substring(1) : dataString;
		Venda.storeloc.stores = jQuery.parseJSON('[' + dataString + ']');
		var options = '<option value="">' + jQuery('#tag-selectastore').text() + '</option>';

		Venda.storeloc.stores = Venda.storeloc.stores.sort(function(a, b){
			if(a.StoreName < b.StoreName) return -1;
			if(a.StoreName > b.StoreName) return 1;
			return 0;
		})

		for (var i = 0; i < Venda.storeloc.stores.length; i++) {
			options += '<option data-StoreID="'+ Venda.storeloc.stores[i].StoreID +'" value="' + Venda.storeloc.stores[i].optionValue + '">' + Venda.storeloc.stores[i].optionDisplay + '</option>';
		}

		Venda.storeloc.separateStoresArray();

		if (!do_not_redraw) {
			Venda.storeloc.restoreState();

			jQuery('.js-loading').hide();
			jQuery('#address_holder').removeClass("hide");
			jQuery('.js-storeLocSelect').html(options).show();
			jQuery('.js-storeLocSelectHolder').addClass('js-custom');

			if (!(Venda.storeloc.storeID)) {
				if (Venda.storeloc.bounds&&Venda.storeloc.map) {Venda.storeloc.map.fitBounds(Venda.storeloc.bounds)}
				jQuery('.js-storeLocSelectHolder.js-custom').delay(3000,function(){	Venda.Ebiz.customSelect()});
			}
		}

	});
}

/**
* Sets settings for the maps, layers and markers
**/
Venda.storeloc.Initialize = function(){

	//SET CENTER
	Venda.storeloc.map = new google.maps.Map(document.getElementById('map_canvas'), {
		center: new google.maps.LatLng(53.085364,-3.991528), // Default map location
		zoom: 15,
		scrollwheel:false,
		mapTypeControl: true,
		streetViewControl: false,
		overviewMapControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
		},
	// CONTROLS
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		},
		mapTypeId: 'roadmap'
	});

	//GEOCODER
	Venda.storeloc.geocoder = new google.maps.Geocoder();

	Venda.storeloc.marker = new google.maps.Marker({
		map: Venda.storeloc.map,
		draggable: false
	});
}

/**
* Updates all elements of the map and populates the accordion list of stores
**/
Venda.storeloc.updateMap = function(){
	jQuery('#pcsubmit').addClass("again");

	Venda.storeloc.storesQueryTK = [];
	Venda.storeloc.storesQueryHS = [];
	Venda.storeloc.bounds = [];

	// get the markers to all clear!
	Venda.storeloc.deleteOverlays();

	// This hides store options that are related to store location
	jQuery('.js-storeLocSelectHolder, #altStoreView').hide();
		jQuery('#js-loading').show();
		jQuery('#address_holder').addClass("hide");
	Venda.storeloc.myLocation = new google.maps.LatLng(Venda.storeloc.mylat, Venda.storeloc.mylng);
	Venda.storeloc.marker.setPosition(Venda.storeloc.myLocation);
	Venda.storeloc.map.setCenter(Venda.storeloc.myLocation);
	Venda.storeloc.bounds = new google.maps.LatLngBounds();

	// This adds the point of the marker to the bounds of the view
	var point = new google.maps.LatLng(Venda.storeloc.marker.getPosition().lat() , Venda.storeloc.marker.getPosition().lng());
	Venda.storeloc.bounds.extend(point);

	Venda.storeloc.calculateDistance();

	// This sorts by distance from point
	if (Venda.storeloc.isRegion) {
		Venda.storeloc.stores.sort();
		Venda.storeloc.stores = Venda.storeloc.stores.sort(function(a, b){
			if(a.StoreName < b.StoreName) return -1;
			if(a.StoreName > b.StoreName) return 1;
			return 0;
		})
		Venda.storeloc.storesTK = Venda.storeloc.storesTK.sort(function(a, b){
			if(a.StoreName < b.StoreName) return -1;
			if(a.StoreName > b.StoreName) return 1;
			return 0;
		})
		Venda.storeloc.storesHS = Venda.storeloc.storesHS.sort(function(a, b){
			if(a.StoreName < b.StoreName) return -1;
			if(a.StoreName > b.StoreName) return 1;
			return 0;
		})
	} else {
		Venda.storeloc.stores = Venda.storeloc.stores.sort(function(a,b) { return parseFloat(a.Dist) - parseFloat(b.Dist) } );
		Venda.storeloc.storesTK = Venda.storeloc.storesTK.sort(function(a,b) { return parseFloat(a.Dist) - parseFloat(b.Dist) } );
		Venda.storeloc.storesHS = Venda.storeloc.storesHS.sort(function(a,b) { return parseFloat(a.Dist) - parseFloat(b.Dist) } );
	}

	// if this function call from store detail, do not perform the below function
	Venda.storeloc.populateMarkers();
	// Zoom to fit
	Venda.storeloc.map.fitBounds(Venda.storeloc.bounds);

	// jQuery templates
	jQuery('#storelocatorresults-tkmaxx').empty();
	jQuery('#storelocatorresults-homesense').empty();
	if (Venda.storeloc.searchOptionIsTK) {
		jQuery('#tkmaxx-head-tmpl').tmpl().appendTo('#storelocatorresults-tkmaxx');
		jQuery('#store-tmpl').tmpl(Venda.storeloc.storesQueryTK).appendTo('#storelocatorresults-tkmaxx #tk-storecontent');
		if (Venda.storeloc.storesQueryTK.length==0) {jQuery('#tkmaxx-foot-tmpl').tmpl().appendTo('#storelocatorresults-tkmaxx #tk-storecontent');}
	}
	if (Venda.storeloc.searchOptionIsHS) {
		jQuery('#homesense-head-tmpl').tmpl().appendTo('#storelocatorresults-homesense');
		jQuery('#store-tmpl').tmpl(Venda.storeloc.storesQueryHS).appendTo('#storelocatorresults-homesense #hs-storecontent');
		if (Venda.storeloc.storesQueryHS.length==0) {jQuery('#homesense-foot-tmpl').tmpl().appendTo('#storelocatorresults-homesense #hs-storecontent ');}
	}
	Venda.storeloc.changeResultURL();
	//jQuery("#storelocatorresults-tkmaxx").trigger("create");

	// jQuery UI accordion
	//jQuery('#storelocatorresults').accordion('destroy').accordion({autoHeight: false, clearStyle: true, collapsible: true});
	jQuery('#js-loading').hide();
	jQuery('#address_holder').removeClass("hide");
}
/**
* change result url to be able to right click
**/
Venda.storeloc.changeResultURL = function() {
	if (jQuery(".go_to_store a").length>0) {
		jQuery(".go_to_store a").each(function(index) {
			var templat = jQuery(this).attr("data-lat");
			var templng = jQuery(this).attr("data-lng");
			var tempdist = escape(jQuery(this).attr("data-dist"));
			var tempid = jQuery(this).attr("data-id");
			var temploc;
			if (jQuery('#input-enter_location').text().length>0) {
				temploc = escape(jQuery('#input-enter_location').text());
			} else {
				temploc = escape(jQuery('#storelookup #address').val());
			}
			var tempactive = 0; // no active tab // var currentUri = new Uri(window.location);parseInt(currentUri.getQueryParamValue('active'));
			var temptk = Venda.storeloc.searchOptionIsTK.toString();
			var temphs = Venda.storeloc.searchOptionIsHS.toString();
			jQuery(this).attr("href",
				jQuery('#tag-ebizurl').text().replace('http:',window.location.protocol)
				+ '/page/storelocator/'
				+ "?loc=" + temploc
				+ "&lat=" + templat
				+ "&lng=" + templng
				+ "&active=" + tempactive
				+ "&tk=" + temptk
				+ "&hs=" + temphs
				+ "&storeid=" + tempid
				+ "&dist=" + tempdist
			);
		});
	}
}
/**
* Populates markers an array the list of stores and makes sure that it zooms to fit
**/
Venda.storeloc.populateMarkers = function(){
	var markerLimitTK = 0;
	var markerLimitHS = 0;
	if (Venda.storeloc.isRegion) {
		markerLimitTK = Venda.storeloc.storesTK.length;
		markerLimitHS = Venda.storeloc.storesHS.length;
	} else {
		markerLimitTK = (Venda.storeloc.storesTK.length < Venda.storeloc.limit) ? Venda.storeloc.storesTK.length : Venda.storeloc.limit;
		markerLimitHS = (Venda.storeloc.storesHS.length < Venda.storeloc.limit) ? Venda.storeloc.storesHS.length : Venda.storeloc.limit;
	}
	for (var i=0; i<Venda.storeloc.storesTK.length; i++){
		// This is for search result that chose TK checkbox
		if (Venda.storeloc.searchOptionIsTK) {
			if (Venda.storeloc.storesQueryTK.length < markerLimitTK) {
				if (Venda.storeloc.isRegion) {
					Venda.storeloc.storesQueryTK.push(Venda.storeloc.storesTK[i]);
				} else if (Venda.storeloc.storesTK[i].Dist <= Venda.storeloc.maxDistance) {
					Venda.storeloc.storesQueryTK.push(Venda.storeloc.storesTK[i]);
				}
			}
		}
	}
	for (var i=0; i<Venda.storeloc.storesQueryTK.length; i++){
		var point = new google.maps.LatLng(Venda.storeloc.storesQueryTK[i].Latitude, Venda.storeloc.storesQueryTK[i].Longitude);  // point of the current store
		// This extends the bounds of the maps view by passing it the lat and lng of each stores points
		Venda.storeloc.bounds.extend(point);
		Venda.storeloc.addMarker(point, i, 'tk');
	}
	for (var i=0; i<Venda.storeloc.storesHS.length; i++){
		// This is for search result that chose HS checkbox
		if (Venda.storeloc.searchOptionIsHS) {
			if (Venda.storeloc.storesQueryHS.length < markerLimitHS) {
				if (Venda.storeloc.isRegion) {
					Venda.storeloc.storesQueryHS.push(Venda.storeloc.storesHS[i]);
				} else if (Venda.storeloc.storesHS[i].Dist <= Venda.storeloc.maxDistance) {
					Venda.storeloc.storesQueryHS.push(Venda.storeloc.storesHS[i]);
				}
			}
		}
	}
	for (var i=0; i<Venda.storeloc.storesQueryHS.length; i++){
		var point = new google.maps.LatLng(Venda.storeloc.storesQueryHS[i].Latitude, Venda.storeloc.storesQueryHS[i].Longitude);  // point of the current store
		// This extends the bounds of the maps view by passing it the lat and lng of each stores points
		Venda.storeloc.bounds.extend(point);
		Venda.storeloc.addMarker(point, i, 'hs');
	}
}

/**
* Adds attributes to the marker and events that are triggered when that marker is clicked
* @param{string} point the google point data of the position on the map
* @param{string} i is the stores array number reference of the marker
* @author Alby Barber <abarber@venda.com>
**/
Venda.storeloc.addMarker = function(point, i, storeType) {
	Venda.storeloc.shownStores = [];
	if (storeType == "tk") {
		Venda.storeloc.shownStores = Venda.storeloc.storesQueryTK;
	} else if (storeType == "hs"){
		Venda.storeloc.shownStores = Venda.storeloc.storesQueryHS;
	}
	var marker = new google.maps.Marker({
		draggable: false,
		raiseOnDrag: false,
		icon: Venda.storeloc.image[storeType],
		//shadow: Venda.storeloc.shadow,
		shape: Venda.storeloc.shape,
		animation: google.maps.Animation.DROP,
		position: point,
		map: Venda.storeloc.map,
	});
	var infowindowButton = jQuery('#dtsStorelocator').length > 0 ? jQuery('#tag-selectthisstore').text() : jQuery('#tag-storedetails').text();

	var storeOpentingTimes = "";
	var storeDept = "";

	if(jQuery.trim(Venda.storeloc.shownStores[i].StoreOpeningTimes) != ''){
		storeOpentingTimes = '<h2>'+ jQuery('#tag-openingtimes').text() + '</h2>' +
		'<p class="js-storeloc-openingtimes">' + Venda.storeloc.shownStores[i].StoreOpeningTimes + '</p>';
	}
	if(jQuery.trim(Venda.storeloc.shownStores[i].StoreDept) != ''){
		storeDept = '<h2>'+ jQuery('#tag-dept').text() + '</h2>' +
		'<p class="js-storeloc-dept">'+ Venda.storeloc.shownStores[i].StoreDept + '</p>';
	}
	var contentString =
		'<div class="js-storeloc-infowindow">'+
			'<div class="js-storeloc-logo"></div>'+
			'<div class="js-storeloc-header">'+
				'<h2>' + Venda.storeloc.shownStores[i].StoreName + '</h2>'+
				'<p class="js-storeloc-address">' +
					Venda.storeloc.shownStores[i].AddressName + " " + Venda.storeloc.shownStores[i].Address + " " + Venda.storeloc.shownStores[i].Address2 + " " +
					Venda.storeloc.shownStores[i].PostCode +
				'</p>'+
				'<p class="js-storeloc-tel">'+ jQuery('#tag-tel').text() + Venda.storeloc.shownStores[i].Phone + '</p>'+
				'<p class="js-storeloc-directions"><form name="directionform" id="directionform" class="directionform" onsubmit="return false;" data-ajax="false">' +
					'<input id="startaddress" class="startaddress" name="startaddress" type="text" value="' + jQuery('#tag-directions').text() + '">' +
					'<div id="destinationLat" class="hide">' + Venda.storeloc.shownStores[i].Latitude + '</div>' +
					'<div id="destinationLng" class="hide">' + Venda.storeloc.shownStores[i].Longitude + '</div>' +
					'<div id="storename" class="hide">' + Venda.storeloc.shownStores[i].StoreName + '</div>' +
					'<div id="startaddress_result"></div>' +
				'</form></p>'+
			'</div>'+
			'<div class="js-storeloc-body">'+
				storeOpentingTimes + storeDept +
			'</div>'+
		'</div>';
	if (storeType == "tk") {
		Venda.storeloc.infowindowTK[i] = new google.maps.InfoWindow({content: contentString, maxWidth: 300, isHidden: false});
		Venda.storeloc.storeMarkersTK.push(marker);
	} else if (storeType == "hs"){
		Venda.storeloc.infowindowHS[i] = new google.maps.InfoWindow({content: contentString, maxWidth: 300, isHidden: false});
		Venda.storeloc.storeMarkersHS.push(marker);
	}

	// This is the Event for the markers
	google.maps.event.addListener(marker, 'mouseover', function() {
		// Closes other info windows
		Venda.storeloc.closeInfoWindows();
		if ((marker.icon.url).toString().search("hs") > -1) {
			Venda.storeloc.infowindowHS[i].open(Venda.storeloc.map, marker);
		} else {
			Venda.storeloc.infowindowTK[i].open(Venda.storeloc.map, marker);
		}
		Venda.storeloc.getDirections();
	});
}


/**
* Works out the distance between the stores returned and the current location
**/
Venda.storeloc.calculateDistance = function(fromStoreDetail){
	if (!(fromStoreDetail)) {
		var LatLng = new google.maps.LatLng(Venda.storeloc.marker.getPosition().lat() , Venda.storeloc.marker.getPosition().lng()); // current location
	} else {
		var LatLng = fromStoreDetail;
	}

	for (i=0;i<Venda.storeloc.stores.length;i++){

		var point = new google.maps.LatLng(Venda.storeloc.stores[i].Latitude, Venda.storeloc.stores[i].Longitude);  // point of the current store
		var meters = google.maps.geometry.spherical.computeDistanceBetween(LatLng, point)       // distance between the two points

		if(Venda.storeloc.unitsInMiles){
			Venda.storeloc.stores[i].Unit = 'Miles';
			var miles  = meters * Venda.storeloc.METERS_TO_MILES_CONV;                                  // convert from meters to miles
			Venda.storeloc.stores[i].Dist = miles.toFixed(1);                                           // update stores object
		}
		else{
			Venda.storeloc.stores[i].Unit = 'km';
			var kilometers = (meters / 1000);
			Venda.storeloc.stores[i].Dist = kilometers.toFixed(1);
		}
	}
	for (i=0;i<Venda.storeloc.storesTK.length;i++){

		var point = new google.maps.LatLng(Venda.storeloc.storesTK[i].Latitude, Venda.storeloc.storesTK[i].Longitude);  // point of the current store
		var meters = google.maps.geometry.spherical.computeDistanceBetween(LatLng, point)       // distance between the two points

		if(Venda.storeloc.unitsInMiles){
			Venda.storeloc.storesTK[i].Unit = 'Miles';
			var miles  = meters * Venda.storeloc.METERS_TO_MILES_CONV;                                  // convert from meters to miles
			Venda.storeloc.storesTK[i].Dist = miles.toFixed(1);                                           // update stores object
		}
		else{
			Venda.storeloc.storesTK[i].Unit = 'km';
			var kilometers = (meters / 1000);
			Venda.storeloc.storesTK[i].Dist = kilometers.toFixed(1);
		}
	}
	for (i=0;i<Venda.storeloc.storesHS.length;i++){

		var point = new google.maps.LatLng(Venda.storeloc.storesHS[i].Latitude, Venda.storeloc.storesHS[i].Longitude);  // point of the current store
		var meters = google.maps.geometry.spherical.computeDistanceBetween(LatLng, point)       // distance between the two points

		if(Venda.storeloc.unitsInMiles){
			Venda.storeloc.storesHS[i].Unit = 'Miles';
			var miles  = meters * Venda.storeloc.METERS_TO_MILES_CONV;                                  // convert from meters to miles
			Venda.storeloc.storesHS[i].Dist = miles.toFixed(1);                                           // update stores object
		}
		else{
			Venda.storeloc.storesHS[i].Unit = 'km';
			var kilometers = (meters / 1000);
			Venda.storeloc.storesHS[i].Dist = kilometers.toFixed(1);
		}
	}
}

/**
* looks at the url and restores the page based on those params
**/
Venda.storeloc.restoreState = function() {
	var currentUri = new Uri(window.location);
	Venda.storeloc.searchOptionIsTK = false;
	Venda.storeloc.searchOptionIsHS = false;

	if (currentUri.getQueryParamValues('loc').toString()){
		Venda.storeloc.mylat    = currentUri.getQueryParamValues('lat').toString();
		Venda.storeloc.mylng    = currentUri.getQueryParamValues('lng').toString();
		Venda.storeloc.storeID 	= currentUri.getQueryParamValues('storeid').toString();
		var storeDist = unescape(currentUri.getQueryParamValues('dist').toString());

		Venda.storeloc.searchOptionIsTK = ((currentUri.getQueryParamValues('tk').toString()) === "true");
		Venda.storeloc.searchOptionIsHS = ((currentUri.getQueryParamValues('hs').toString()) === "true");

		jQuery("input#tkmaxxstore").prop('checked', Venda.storeloc.searchOptionIsTK)
		jQuery("input#homesensestore").prop('checked', Venda.storeloc.searchOptionIsHS)

		var activeTab		= parseInt(currentUri.getQueryParamValue('active')),
			cleanAddress	= unescape(currentUri.getQueryParamValues('loc').toString());

		jQuery('#address').val(Venda.storeloc.isRegion ? jQuery("#tag-enter_location").text() : cleanAddress);
		Venda.storeloc.switchStoreLocHeader(cleanAddress); // change header to be search keyword
		jQuery('#mapContent').removeClass('js-mapContent');

		Venda.storeloc.updateMap();

		if (!(Venda.storeloc.storeID)){
			Venda.storeloc.moveMapToSearchResultPanel();
		}
		/*
		jQuery('#nav').animate({ "margin-top": "0px"}, 1000, function(){

			if (activeTab > 0){
				jQuery('#storelocatorresults').accordion( "activate" , activeTab);
			}

			//Venda.storeloc.infowindow[activeTab].open(Venda.storeloc.map, Venda.storeloc.storeMarkers[activeTab]); //not show infowindow on mobile
			//Venda.storeloc.infowindowActive = true; //not show infowindow on mobile
			Venda.storeloc.updateUriQueries(activeTab);

		});*/
		if (!(Venda.storeloc.storeID)){
			Venda.storeloc.showMap();
		}
		Venda.storeloc.map.fitBounds(Venda.storeloc.bounds);
	} else {
		Venda.storeloc.hideAllPanel();
		jQuery("#store_main").fadeIn();
	}

	//if storeid is not empty then show store detail
	if (Venda.storeloc.storeID){
		Venda.storeloc.loadStore(jQuery('#tag-ebizurl').text().replace('http:',window.location.protocol) + "/stry/" + Venda.storeloc.storeID,storeDist,Venda.storeloc.storeID);
	}

	if (Venda.storeloc.isRegion) {
		Venda.storeloc.ajaxGetStores('storeloc',"yes", true);
		Venda.storeloc.isRegion = false;
	}
//close history bind function in restorestate */
}


/**
* Updates the URL #! with the status of the stores page
**/
Venda.storeloc.updateUriQueries = function(index,storeID, storeName){
	if (!storeID) {
		var uri = new Uri().replaceQueryParam('loc', escape(jQuery('#storelookup #address').val())).replaceQueryParam('lat', Venda.storeloc.mylat).replaceQueryParam('lng', Venda.storeloc.mylng).replaceQueryParam('active', index).replaceQueryParam('tk', Venda.storeloc.searchOptionIsTK).replaceQueryParam('hs', Venda.storeloc.searchOptionIsHS);
		Venda.storeloc.URI = uri.toString();
		var additionalTitle = "";
		Venda.storeloc.naved = true;
		History.pushState({ path: this.path }, document.title + additionalTitle, uri.toString());
	}
}

/**
* Populates the hidden form with the values from the selected store
* @param{string} that is the unique store id
* @author Alby Barber <abarber@venda.com>
**/
Venda.storeloc.fillForm = function(that){

	for (i=0;i<Venda.storeloc.stores.length;i++){

		if (Venda.storeloc.stores[i].StoreID == that){
			jQuery('input[name="num"]').val(Venda.storeloc.stores[i].StoreName).parent().find('span').html(Venda.storeloc.stores[i].StoreName);
			jQuery('input[name="addr1"]').val(Venda.storeloc.stores[i].Address).parent().find('span').html(Venda.storeloc.stores[i].Address);
			jQuery('input[name="addr2"]').val(Venda.storeloc.stores[i].Address2).parent().find('span').html(Venda.storeloc.stores[i].Address2);
			jQuery('input[name="city"]').val(Venda.storeloc.stores[i].City).parent().find('span').html(Venda.storeloc.stores[i].City);
			jQuery('input[name="cntry"]').val(Venda.storeloc.stores[i].Country).parent().find('span').html(Venda.storeloc.stores[i].Country);
			jQuery('input[name="zipc"]').val(Venda.storeloc.stores[i].PostCode).parent().find('span').html(Venda.storeloc.stores[i].PostCode);
			jQuery('input[name="state"]').val(Venda.storeloc.stores[i].State).parent().find('span').html(Venda.storeloc.stores[i].State);
			// This sets 2 hidden inputs that are used in conditions on the order summary screen
			jQuery('input[name="addrname"]').val(Venda.storeloc.stores[i].StoreName).parent().find('span').html(Venda.storeloc.stores[i].StoreName);
			//jQuery('input[name="fax"]').val(Venda.storeloc.stores[i].StoreID);

		}
	}
	// Show the DTS 'Select Store' button
	jQuery("#DTSchangeStore, #dtsSubmit, #storedeliveryaddress").show();
}

/**
* HELPER FUNCTIONS
**/
// Deletes all markers in the array by removing references to them
Venda.storeloc.deleteOverlays = function() {
	if (Venda.storeloc.storeMarkers) {
	//convert to a simple for loop instead, which works in all major browsers
	for (var i=0; i<Venda.storeloc.storeMarkers.length; i++){
		Venda.storeloc.storeMarkers[i].setMap(null);
	}
	Venda.storeloc.storeMarkers.length = 0;
	}
	if (Venda.storeloc.storeMarkersTK) {
	//convert to a simple for loop instead, which works in all major browsers
	for (var i=0; i<Venda.storeloc.storeMarkersTK.length; i++){
		Venda.storeloc.storeMarkersTK[i].setMap(null);
	}
	Venda.storeloc.storeMarkersTK.length = 0;
	}
	if (Venda.storeloc.storeMarkersHS) {
	//convert to a simple for loop instead, which works in all major browsers
	for (var i=0; i<Venda.storeloc.storeMarkersHS.length; i++){
		Venda.storeloc.storeMarkersHS[i].setMap(null);
	}
	Venda.storeloc.storeMarkersHS.length = 0;
	}
}

//make overlay visible
Venda.storeloc.visibleOverlays = function() {
	if (Venda.storeloc.storeMarkers) {
		//convert to a simple for loop instead, which works in all major browsers
		for (var i=0; i<Venda.storeloc.storeMarkers.length; i++){
			Venda.storeloc.storeMarkers[i].setVisible(true)
		}
	}
	if (Venda.storeloc.storeMarkersTK) {
		//convert to a simple for loop instead, which works in all major browsers
		for (var i=0; i<Venda.storeloc.storeMarkersTK.length; i++){
			Venda.storeloc.storeMarkersTK[i].setVisible(true)
		}
	}
	if (Venda.storeloc.storeMarkersHS) {
		//convert to a simple for loop instead, which works in all major browsers
		for (var i=0; i<Venda.storeloc.storeMarkersHS.length; i++){
			Venda.storeloc.storeMarkersHS[i].setVisible(true)
		}
	}
}

// This looks for infowindows and close them
Venda.storeloc.closeInfoWindows = function() {
	if (Venda.storeloc.infowindowActive){
		for(var j=0;j<Venda.storeloc.infowindowTK.length;j++){
			Venda.storeloc.infowindowTK[j].close();
		}
		for(var k=0;k<Venda.storeloc.infowindowHS.length;k++){
			Venda.storeloc.infowindowHS[k].close();
		}
	}
	Venda.storeloc.infowindowActive = true;
}
// This loads in the store to the mainHolder frame
Venda.storeloc.loadStore = function(storeUrl,storeDist,storeID,storeType){
	jQuery('#js-loading').show();
	jQuery('#address_holder').removeClass("hide");
	//initialize and clean the page
	if (!(jQuery.isEmptyObject(Venda.storeloc.currentStoreMarker))) {
		Venda.storeloc.currentStoreMarker.setMap(null);
	}
	var validatedform = jQuery('form[name=storelocatorform]').validate();
	validatedform.resetForm();
	Venda.storeloc.closeInfoWindows();
	Venda.storeloc.deleteOverlays();
	Venda.storeloc.marker.setVisible(false);
	Venda.storeloc.moveMapToLeftPanel();
	jQuery("#storeNameHeading").empty();
	jQuery("#buttonContainer").empty();


	//start declaring the current store
	var currentStore = {};
	for (var i=0; i < Venda.storeloc.stores.length; i++) {
		if (((Venda.storeloc.stores[i].StoreID).toString()) == storeID) {
			currentStore = Venda.storeloc.stores[i];
			break;
		}
	}
	Venda.storeloc.updateUriQueries('0',storeID, currentStore.StoreName);
	var br = "<br />";

	// check if each stxt is having content
	var storeDist = (storeDist)? ' - '+ storeDist : "";
	var currentStoreName = (currentStore.StoreName)?'<h3>' + currentStore.StoreName + storeDist +'</h3>':"";
	var currentStoreAddressName = (currentStore.AddressName)?currentStore.AddressName + br:"";
	var currentStoreAddress = (currentStore.Address)?currentStore.Address + br:"";
	var currentStoreAddress2 = (currentStore.Address2)?currentStore.Address2 + br:"";
	var currentStoreCity = (currentStore.City)?currentStore.City + br:"";
	var currentStoreCountry = (currentStore.Country)?currentStore.Country + br:"";
	var currentStorePostCode = (currentStore.PostCode)?currentStore.PostCode:"";
	// if address is not totally empty, then show it
	var currentStoreAddressHeader = ((currentStoreAddressName)||(currentStoreAddress)||(currentStoreAddress2)||(currentStoreCity)||(currentStoreCountry)||(currentStorePostCode))?'<h3>' + jQuery("#tag-storedetail-head-address").text() + '</h3>': "";

	var currentStoreAddressBody = (currentStoreAddressHeader)?
		'<p>' + currentStoreAddressName + currentStoreAddress + currentStoreAddress2 + currentStoreCity + currentStoreCountry + currentStorePostCode + '</p>'
		: "";
	// --
	var currentStorePhone = (currentStore.Phone)? '<h3>' + jQuery("#tag-storedetail-head-phone").text() + '</h3>'
		+ '<p>'	+ currentStore.Phone + '</p>' : "";
	var currentStoreFacilities = (currentStore.Facilities)? '<h3>' + jQuery("#tag-storedetail-head-facility").text() + '</h3>'
		+ '<p>'	+ currentStore.Facilities + '</p>' : "";
	var currentStoreStoreDept = (currentStore.StoreDept) ? '<h3>' + jQuery("#tag-storedetail-head-departments").text() + '</h3>'
		+ '<p>'	+ currentStore.StoreDept + '</p>' : "";
	var currentStoreStoreOpeningTimes = (currentStore.StoreOpeningTimes) ?
		'<div class="js-storedetail-opening">'
			+ '<h3>' + jQuery("#tag-storedetail-head-opening").text() + '</h3>'
			+ '<p class="openingTimes">' + currentStore.StoreOpeningTimes + '</p>'
		+ '</div>'
		: "";
	// do not have to check StoreDirection coz it must always show
	var currentStoreStoreDirection = '<h3>' + jQuery("#tag-storedetail-head-storedirection").text() + '</h3>'
		+ '<p>' + currentStore.StoreDirection + '</p>'
		+ '<p class="getdirectionText">' + 'Get directions to this store from' + '(enter address, town, or postcode):' + '</p>'
		+ '<div class="js-storeloc-directions"><form name="directionform" id="directionform" class="directionform" onsubmit="return false;" data-ajax="false">'
			+ '<div class="row"><div class="large-14 small-14 columns">'
			+ '<input id="startaddress" name="startaddress" class="startaddress" type="text" value="' + jQuery('#tag-directions').text() + '">'
			+ '</div><div class="large-10 small-10 columns btn-wrapper">'
			+ '<input id="getDrivingStoreDetail" class="button small" type="submit" value="' + jQuery('#tag-storedetail-getdirection').text() + '" class="getDirectionsButton">'
			+ '<div id="destinationLat" class="hide">' + currentStore.Latitude + '</div>'
			+ '<div id="destinationLng" class="hide">' + currentStore.Longitude + '</div>'
			+'<div id="storename" class="hide">' + currentStore.StoreName + '</div>'
			+ '<div id="startaddress_result"></div>'
			+ '</div></div>'
		+ '</form></div>'
		;
	var currentStoreStoreComment = (currentStore.StoreComment) ? '<h3>' + jQuery("#tag-storedetail-head-comment").text() + '</h3>'
		+ '<p>' + currentStore.StoreComment + '</p>' : "";

	// append all store detail into the page
	jQuery("#storeNameHeading").append(currentStoreName).show();
	jQuery("#storeDetail_1").append(
		'<div class="js-storedetail-address">'
			+ currentStoreAddressHeader
			+ currentStoreAddressBody
			+ currentStorePhone
			+ currentStoreFacilities
			+ currentStoreStoreDept
			+ '<br />'
		+ '</div>'
		+ currentStoreStoreOpeningTimes
	);
	jQuery("#storeDetail_2").append(
		'<div class="js-storedetail-detail">'
			+ currentStoreStoreDirection
			+ currentStoreStoreComment
		+ '</div>'
	);
	jQuery("#buttonContainer").append(
		'<div class="js-storeview"><a class="button secondary"><strong>'+ jQuery('#tag-back').text() +'</strong></a></div>'
	).show();
	// start plotting current store marker onto maps
	var textPoint = currentStore.Latitude + " , " + currentStore.Longitude + " , " + currentStore.StoreID + " , " + currentStore.StoreName + " , " + currentStore.Address + " , " + currentStore.Address2;
	var point = new google.maps.LatLng(currentStore.Latitude, currentStore.Longitude);  // point of the current store
	Venda.storeloc.currentStoreMarker = new google.maps.Marker({
		draggable: false,
		raiseOnDrag: false,
		icon: Venda.storeloc.image[currentStore.StoreType],
		shape: Venda.storeloc.shape,
		animation: google.maps.Animation.DROP,
		position: point,
		map: Venda.storeloc.map
	});
	jQuery('#js-loading').hide();
	jQuery('#address_holder').removeClass("hide");
	//get 5 nearest stores to the main store detail page
	Venda.storeloc.GetNearestStoreDetail(currentStore.Latitude,currentStore.Longitude,currentStore.StoreType);
	jQuery("#mapContent").removeClass("large-24").addClass("large-12");
	Venda.storeloc.showMap();
	Venda.storeloc.map.setCenter(point);
	Venda.storeloc.map.setZoom(16);
	Venda.storeloc.triggerMobileSearch();
}

Venda.storeloc.GetNearestStoreDetail = function (currentLat, currentLng, currentStoreType) {
	var currentLatLng = new google.maps.LatLng(currentLat, currentLng);
	var nearestStoresTK = [];
	var nearestStoresHS = [];
	var storesLimitTK = 0;
	var storesLimitHS = 0;
	//recalculate distance from current store to the nearest stores
	Venda.storeloc.calculateDistance(currentLatLng);
	Venda.storeloc.stores = Venda.storeloc.stores.sort(function(a,b) { return parseFloat(a.Dist) - parseFloat(b.Dist) } );
	Venda.storeloc.storesTK = Venda.storeloc.storesTK.sort(function(a,b) { return parseFloat(a.Dist) - parseFloat(b.Dist) } );
	Venda.storeloc.storesHS = Venda.storeloc.storesHS.sort(function(a,b) { return parseFloat(a.Dist) - parseFloat(b.Dist) } );

	storesLimitTK = (Venda.storeloc.storesTK.length < Venda.storeloc.storeDetailNearestLimit) ? Venda.storeloc.storesTK.length : Venda.storeloc.storeDetailNearestLimit;
	storesLimitHS = (Venda.storeloc.storesHS.length < Venda.storeloc.storeDetailNearestLimit) ? Venda.storeloc.storesHS.length : Venda.storeloc.storeDetailNearestLimit;
	Venda.storeloc.checkSearchOption();

	if (Venda.storeloc.searchOptionIsTK) {// This is for search result that TK checkbox is chosen
		for (i=0; i<Venda.storeloc.storesTK.length; i++){
			if ((nearestStoresTK.length <= storesLimitTK) && (Venda.storeloc.storesTK[i].Dist <= Venda.storeloc.maxDistance)) {
				nearestStoresTK.push(Venda.storeloc.storesTK[i]);
			}
		}
	}
	if (Venda.storeloc.searchOptionIsHS) {// This is for search result that HS checkbox is chosen
		for (i=0; i<Venda.storeloc.storesHS.length; i++){
			if ((nearestStoresHS.length <= storesLimitHS) && (Venda.storeloc.storesHS[i].Dist <= Venda.storeloc.maxDistance)) {
				nearestStoresHS.push(Venda.storeloc.storesHS[i]);
			}
		}
	}
	//cut itself out of the nearest shop list
	switch (currentStoreType) {
		case "tk":
			nearestStoresTK.splice(0,1);
			nearestStoresHS.splice(nearestStoresHS.length-1,1);
			break;
		case "hs":
			nearestStoresHS.splice(0,1);
			nearestStoresTK.splice(nearestStoresHS.length-1,1);
			break;
	}
	// jQuery templates
	jQuery('#storelocatorresults-tkmaxx').empty();
	jQuery('#storelocatorresults-homesense').empty();
	if (Venda.storeloc.searchOptionIsTK) {
		jQuery('#tkmaxx-head-tmpl').tmpl().appendTo('#storelocatorresults-tkmaxx');
		jQuery('#store-tmpl').tmpl(nearestStoresTK).appendTo('#storelocatorresults-tkmaxx #tk-storecontent');
		if (nearestStoresTK.length==0) {jQuery('#tkmaxx-foot-tmpl').tmpl().appendTo('#storelocatorresults-tkmaxx #tk-storecontent');}
	}
	if (Venda.storeloc.searchOptionIsHS) {
		jQuery('#homesense-head-tmpl').tmpl().appendTo('#storelocatorresults-homesense');
		jQuery('#store-tmpl').tmpl(nearestStoresHS).appendTo('#storelocatorresults-homesense #hs-storecontent');
		if (nearestStoresHS.length==0) {jQuery('#homesense-foot-tmpl').tmpl().appendTo('#storelocatorresults-homesense #hs-storecontent ');}
	}
	Venda.storeloc.changeResultURL();
}

/**
* EVENTS - General
**/
jQuery(document).on({
	"focus":function(){
		if (this.value == jQuery('#tag-directions').text()) {
			this.value = '';
		}
	},
	"click":function(){
		Venda.storeloc.getDirections();
	},
	"blur":function(){
		if (this.value == '') {
			this.value = jQuery('#tag-directions').text();
		}
	}
},"#startaddress");


jQuery(function() {
// Highlight address seach content on click
jQuery('#address').on("click", function() {
	jQuery(this).select();
});

//get driving direction from store detail
jQuery( document ).on( "click", "#getDrivingStoreDetail", function() {
	if ((jQuery('#address').val().length > 0) && (jQuery('.selectgroup input[type=checkbox]:checked').length > 0)) {
		setTimeout(function(){jQuery('div#startaddress_result .ui-autocomplete li:first-child a').trigger('mouseover').trigger('click');},1000);
	} else {
		alert(jQuery("#tag-nokeyword").text());
	}
});


// Search Go button
jQuery("#address_holder #pcsubmit").on("click", function(){
	if ((jQuery('#address').val().length > 0) && (jQuery('.selectgroup input[type=checkbox]:checked').length > 0)) {
		if (jQuery('#address').val() == jQuery("#tag-enter_location").text()) {
			alert(jQuery("#tag-nokeyword").text());
		} else {
			jQuery( "#address" ).one( "autocompleteopen", function( event, ui ) {
				jQuery('.ui-autocomplete li:first-child a').trigger('mouseover').trigger('click');
			});
			jQuery( "#address" ).autocomplete("search");
			Venda.storeloc.triggerMobileSearch();
		}
	} else {
		alert(jQuery("#tag-nokeyword").text());
	}
});

Venda.storeloc.triggerMobileSearch = function (){
	if(jQuery(window).width() < 768){
		jQuery(".icon-plus").click();
		jQuery(window).scrollTop(jQuery("#address_holder").offset().top);
	}
}

// Select quick store navigation
jQuery("#page_storelocator .js-storeLocSelect").on("change", function(){
	Venda.storeloc.loadStore(this.value);
});

/// General store locator store link
jQuery("#storelocatorresults .go_to_store a").on("click", function(e){
	jQuery('#map_canvas').css('opacity',0);
	e.preventDefault();
	var selectedStoreUrl = jQuery('#tag-ebizurl').text().replace('http:',window.location.protocol) + "/stry/" + jQuery(this).attr("data-id")
	Venda.storeloc.naved = true;
//	Venda.storeloc.updateUriQueries('0',jQuery(this).attr("data-id"));
	Venda.storeloc.loadStore(jQuery(this).attr("data-url"),jQuery(this).attr("data-dist"),jQuery(this).attr("data-id"),jQuery(this).attr("data-storetype"));
	window.scrollTo(0, 0);
})

// General store locator back button
jQuery(document).on("click","#buttonContainer .js-storeview .button", function(){
	window.history.back();
});

// clear search box and search result list when clicking on locationoption radio button
jQuery(document).on("change","input:radio[name='locationoption']", function() {
	jQuery("#address").val(jQuery('#tag-enter_location').text());
	jQuery("#locationlist .ui-autocomplete").hide();
});

/**
* EVENTS - DTS
**/
// DTS 'Select Store' link
jQuery("#dtsStorelocator .js-linkstore a").on("click", function(e){

	e.preventDefault();

	Venda.storeloc.fillForm(this.id);
	jQuery("#dtsStorelocator").fadeOut();

})

// DTS dropdown select store
jQuery("#dtsStorelocator .js-storeLocSelect").on("change", function(){

	var storeID = jQuery("#dtsStorelocator .js-storeLocSelect option:selected").attr('data-storeid');
	Venda.storeloc.fillForm(storeID);
	// hide store locator DTS
	jQuery("#dtsStorelocator").fadeOut();
});

// DTS show store locator DTS
jQuery("#DTSchangeStore").on("click", function(){
	jQuery("#dtsStorelocator").fadeIn();
	jQuery("#DTSchangeStore, #dtsSubmit, #storedeliveryaddress").hide();
})
});
/* Part of Storelocator V2
jQuery("#optionsCheckboxes").on("click", function(){
	if (jQuery('.js-arrowOn').length){
		jQuery('.js-arrowIcon').removeClass('js-arrowOn');
	}
	else {
		jQuery('.js-arrowIcon').addClass('js-arrowOn');
	}
	jQuery("#storelookup-input").fadeToggle("fast", "linear");
	});

jQuery("#optionsCheckbox1, #optionsCheckbox2, #optionsCheckbox3").on("click", function(){
	if (jQuery('#address').val().length > 0){
		Venda.storeloc.updateMap();
	}
});
*/

/*
Custom Function
*/
Venda.storeloc.switchStoreLocHeader = function(reqAddress) {
	jQuery("#search_info h2").text(reqAddress);
	jQuery("#address").val(reqAddress);
	jQuery("#intro_text").hide();
	jQuery("#store_main").hide();
}

Venda.storeloc.showMap = function() {
	jQuery('#map_canvas').show();
	jQuery('#map_canvas').animate({ opacity: 1 }, 1500 );
	google.maps.event.trigger(Venda.storeloc.map, 'resize');
	if (Venda.storeloc.bounds&&Venda.storeloc.map) {Venda.storeloc.map.fitBounds(Venda.storeloc.bounds)}
}

Venda.storeloc.hideAllPanel = function(){ //<-- hide all panels without clear their value
	jQuery("#js-loading").show();
	jQuery('#mapContent').addClass('js-mapContent');
	jQuery('#map_canvas').css('opacity',0);
	jQuery("#mapDirectionsDetail").hide();
	jQuery("#buttonContainer").hide();
	jQuery("#storeDetail_1").hide();
	jQuery("#storeDetail_2").hide();
	jQuery("#storeNameHeading").hide();
	jQuery("#storelocatorresults").hide();
	jQuery("#js-loading").hide();
}

Venda.storeloc.moveMapToLeftPanel = function() { // <-- naming a function to toggle the map to be on the sider
	jQuery("#js-loading").show();
	jQuery("#mapContent").addClass("mapContent_small").removeClass("mapContent_half");
	jQuery('#map_canvas').css({'opacity':'0'}).addClass("map_canvas_small").removeClass("map_canvas_half");
	Venda.storeloc.marker.setVisible(false);
	jQuery("mapDirectionsDetail").hide();
	jQuery("#storelocatorresults").fadeIn();
	jQuery("#storeDetail_1").empty().fadeIn();
	jQuery("#storeDetail_2").empty().fadeIn();
	jQuery("#mapContent.large-12").removeClass("large-12").addClass("large-24");
	if (jQuery.browser.chrome) { jQuery("#map_canvas").hide(0).delay(1).show(0); }
}

Venda.storeloc.moveMapToRightPanel = function() { // <-- naming a function to toggle the map to be on the sider
	jQuery("#js-loading").show();
	jQuery("#mapContent").addClass("mapContent_half").removeClass("mapContent_small");
	jQuery('#map_canvas').css({'opacity':'0'}).addClass("map_canvas_half").removeClass("map_canvas_small");
	Venda.storeloc.marker.setVisible(false);
	//google.maps.event.trigger(Venda.storeloc.map, 'resize');
	jQuery("#storelocatorresults").hide();
	jQuery("#mapDirectionsDetail").show();
	//jQuery("#buttonContainer").empty().hide();
	jQuery("#storeNameHeading").empty().hide();
	jQuery("#storeDetail_1").empty().hide();
	jQuery("#storeDetail_2").empty().hide();
	jQuery("#mapContent").removeClass("large-24").addClass("large-12");
	if (!(jQuery.isEmptyObject(Venda.storeloc.currentStoreMarker))) {
		Venda.storeloc.currentStoreMarker.setMap(null);
	}
	if (jQuery.browser.chrome) { jQuery("#map_canvas").hide(0).delay(1).show(0); }
}

Venda.storeloc.moveMapToSearchResultPanel = function(mustReload) { // <-- restore map search result layout to normal
	jQuery("#js-loading").show();
	jQuery('#address_holder').addClass("hide");
	// move panel to normal state
	jQuery("#mapContent").removeClass("mapContent_half").removeClass("mapContent_small");
	jQuery('#map_canvas').css('opacity',0).removeClass("map_canvas_small").removeClass("map_canvas_half");
	jQuery("#mapDirectionsDetail").hide();
	jQuery("#mapContent.large-12").removeClass("large-12").addClass("large-24");
	jQuery("#buttonContainer").empty().hide();
	jQuery("#storeDetail_1").empty().hide();
	jQuery("#storeDetail_2").empty().hide();
	jQuery("#storeNameHeading").empty().hide();
	if (!(jQuery.isEmptyObject(Venda.storeloc.currentStoreMarker))) {
		Venda.storeloc.currentStoreMarker.setMap(null);
	}
	//delete all direction and store detail markers
	directionsDisplay.setMap(null);
	Venda.storeloc.stepDisplay.close()

	if (Venda.storeloc.markerDirectionsArray){
		for (var i = 0; i < Venda.storeloc.markerDirectionsArray.length; i++) {
			if (Venda.storeloc.markerDirectionsArray[i]) {
				Venda.storeloc.markerDirectionsArray[i].setMap(null);
			}
		}
	}
	if (!(mustReload)) {
		// show normal result, markers and every normal search resutl if it is not a reload page
		Venda.storeloc.visibleOverlays();
		Venda.storeloc.marker.setVisible(true);
		jQuery('#storelocatorresults').fadeIn();
		jQuery('#map_canvas').animate({ opacity: 1 }, 1500 );
		jQuery("#js-loading").hide();
		jQuery('#address_holder').removeClass("hide");
	}
}

Venda.storeloc.showDirections = function(startLat,startLng,destinationLat,destinationLng,storename){
	//initial & reset everything
	Venda.storeloc.moveMapToRightPanel();
	Venda.storeloc.closeInfoWindows();
	if (Venda.storeloc.markerDirectionsArray){
		for (var i = 0; i < Venda.storeloc.markerDirectionsArray.length; i++) {
			if (Venda.storeloc.markerDirectionsArray[i]) {
				Venda.storeloc.markerDirectionsArray[i].setMap(null);
			}
		}
	}
	if (Venda.storeloc.storeMarkers) {
		//convert to a simple for loop instead, which works in all major browsers
		for (var i=0; i<Venda.storeloc.storeMarkers.length; i++){
			Venda.storeloc.storeMarkers[i].setVisible(false)
		}
	}
	if (Venda.storeloc.storeMarkersTK) {
		//convert to a simple for loop instead, which works in all major browsers
		for (var i=0; i<Venda.storeloc.storeMarkersTK.length; i++){
			Venda.storeloc.storeMarkersTK[i].setVisible(false)
		}
	}
	if (Venda.storeloc.storeMarkersHS) {
		//convert to a simple for loop instead, which works in all major browsers
		for (var i=0; i<Venda.storeloc.storeMarkersHS.length; i++){
			Venda.storeloc.storeMarkersHS[i].setVisible(false)
		}
	}
	var originLatLng = new google.maps.LatLng( startLat, startLng ); // current location
	var destinationLatLng = new google.maps.LatLng( destinationLat, destinationLng );
	var rendererOptions = {
		map: Venda.storeloc.map,
		suppressMarkers: true,
		infoWindow: Venda.storeloc.stepDisplay
	}

	directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
	directionsService = new google.maps.DirectionsService();

	jQuery('#store_info_primary').empty();
	directionsDisplay.setMap(null);
	directionsDisplay.setMap(Venda.storeloc.map);
	directionsDisplay.setPanel(document.getElementById('store_info_primary'));

	var request = {
		origin: originLatLng,
		destination: destinationLatLng,
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.IMPERIAL
	};
	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			jQuery("#direction_print_top").show();
			jQuery("#direction_print_bottom").show();
			var warnings = document.getElementById("store_info_primary_warning");
			warnings.innerHTML = "" + result.routes[0].warnings + "";
			directionsDisplay.setDirections(result);

			//showSteps(result); Instead of creating another function to be called, embed them here below

			//start working on showSteps function
			var myRoute = result.routes[0].legs[0];
			for (var i = 0; i < myRoute.steps.length; i++) {
				if ((i==0) || (i==myRoute.steps.length-1)) {
					var iconImageDirections;
					if (i==0) {
						iconImageDirections = new google.maps.MarkerImage(
							'http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-a.png&text=A&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48&scale=1',
							new google.maps.Size(22,44),
							new google.maps.Point(0,0)
						)
					}
					if (i==myRoute.steps.length-1) {
						iconImageDirections = new google.maps.MarkerImage(
							'http://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-b.png&text=B&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48&scale=1',
							new google.maps.Size(22,44),
							new google.maps.Point(0,0)
						)
					}
					var marker = new google.maps.Marker({
						position: myRoute.steps[i].start_point,
						icon:iconImageDirections,
						map: Venda.storeloc.map
					});
					Venda.storeloc.attachInstructionText(marker, myRoute.steps[i].instructions);
					Venda.storeloc.markerDirectionsArray[i] = marker;
				}
			}
		}
	});

	//show distance
	var LatLng = new google.maps.LatLng(destinationLat, destinationLng); // current location
	var point = new google.maps.LatLng(startLat, startLng);  // point of the current store
	var meters = google.maps.geometry.spherical.computeDistanceBetween(LatLng, point)       // distance between the two points
	if(Venda.storeloc.unitsInMiles){
		var miles  = meters * Venda.storeloc.METERS_TO_MILES_CONV;                                  // convert from meters to miles
		var Dist = miles.toFixed(1) + ' Miles';                                           // update stores object
	}
	else{
		var kilometers = (meters / 1000);
		var Dist = kilometers.toFixed(1) + ' km';
	}
	var storename = storename;
	var currentStoreName = (storename)?'<h3>' + storename + ' - ' + Dist +'</h3>':"";
	jQuery("#storeNameHeading").append(currentStoreName).show();


	jQuery('#map_canvas').animate({ opacity: 1 }, 1500 );
	Venda.storeloc.showMap();
	jQuery("#mapDirectionsDetail").show();
	google.maps.event.addDomListener(window, 'load', Venda.storeloc.showDirections);
	jQuery("#js-loading").hide();
	jQuery('#address_holder').removeClass("hide");

	return(false);
}

Venda.storeloc.attachInstructionText = function(marker, text) {
	google.maps.event.addListener(marker, 'click', function() {
		Venda.storeloc.stepDisplay.close();
		Venda.storeloc.stepDisplay.setContent(text);
		Venda.storeloc.stepDisplay.open(Venda.storeloc.map, marker);
	});
}

jQuery(function() {
	jQuery("#m_all area").on({
		mouseover: function () {
			var delayTime = 300;
			if ($.browser.msie && $.browser.version*1 < 9) { delayTime = 0 };
			jQuery('#showmap').addClass(jQuery(this).attr("regionid"));
			jQuery('#showmap').animate( { opacity: 1, display: 'block' }, delayTime );
		},
		mouseout: function () {
			var delayTime = 300;
			jQuery('#showmap').css("opacity", "0").removeClass(jQuery(this).attr("regionid"));
		},
		click: function () {
			var currentUri = new Uri(window.location);
			Venda.storeloc.checkSearchOption();
			jQuery("#input-enter_location").text(Venda.storeloc.region[jQuery(this).attr("regionid")].regionname);
			var uri = new Uri().replaceQueryParam('loc', Venda.storeloc.region[jQuery(this).attr("regionid")].regionname).replaceQueryParam('lat', Venda.storeloc.region[jQuery(this).attr("regionid")].lat).replaceQueryParam('lng', Venda.storeloc.region[jQuery(this).attr("regionid")].lng).replaceQueryParam('active', 0).replaceQueryParam('tk', Venda.storeloc.searchOptionIsTK).replaceQueryParam('hs', Venda.storeloc.searchOptionIsHS).replaceQueryParam('regionid', Venda.storeloc.region[jQuery(this).attr("regionid")].regionid);

			window.location = uri.toString();

			//Venda.storeloc.ajaxGetStores(Venda.storeloc.region[jQuery(this).attr("regionid")]);
		}
	});
});


Venda.storeloc.checkSearchOption = function () {
	Venda.storeloc.searchOptionIsTK = jQuery("#tkmaxxstore").is(':checked');
	Venda.storeloc.searchOptionIsHS = jQuery("#homesensestore").is(':checked');
}
Venda.storeloc.checkIsRegion = function () {
	var currentUri = new Uri(window.location);
	if (currentUri.getQueryParamValues('regionid').toString()){
		Venda.storeloc.isRegion = true;
	} else {
		Venda.storeloc.isRegion = false;
	}
}
Venda.storeloc.separateStoresArray = function(){
	Venda.storeloc.storesTK = [];
	Venda.storeloc.storesHS = [];
	for (var hh = 0; hh < Venda.storeloc.stores.length; hh++) {
		if (Venda.storeloc.stores[hh].Key1.toLowerCase()==("TK Maxx").toLowerCase()) {
			Venda.storeloc.stores[hh].StoreType="tk";
			Venda.storeloc.storesTK.push(Venda.storeloc.stores[hh]);
		}
		if (Venda.storeloc.stores[hh].Key1.toLowerCase()==("Homesense").toLowerCase()) {
			Venda.storeloc.stores[hh].StoreType="hs";
			Venda.storeloc.storesHS.push(Venda.storeloc.stores[hh]);
		}
	}
	Venda.storeloc.storesTK = Venda.storeloc.storesTK.sort(function(a, b){
		if(a.StoreName < b.StoreName) return -1;
		if(a.StoreName > b.StoreName) return 1;
		return 0;
	})
	Venda.storeloc.storesHS = Venda.storeloc.storesHS.sort(function(a, b){
		if(a.StoreName < b.StoreName) return -1;
		if(a.StoreName > b.StoreName) return 1;
		return 0;
	})
}

Venda.storeloc.getDirections = function () {
	/* Start Custom Get Direction Autocomplete form */
	jQuery("#startaddress").autocomplete({
		//This bit uses the geocoder to fetch address values
		autoFocus: true,
		minLength: 3,
		mustMatch:true,
		delay:100,
		appendTo: "div#startaddress_result",
		position: {
			at: "left top",
			//of: "#locationlist",
			collision: "fit",
			within: "div#startaddress_result"
		},
		source: function(request, response) {
			Venda.storeloc.geocoder.geocode( {'address': request.term , 'componentRestrictions':{'country':jQuery("input:radio[name='locationoption']:checked").val().toUpperCase()}}, function(results, status) {
				response(jQuery.map(results, function(item) {
					var arrAddress = item.address_components;
					var itemLocality='';
					// iterate through address_component array
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "locality"){
							itemLocality = address_component.long_name;
						}
					});
					return {
						label:  item.formatted_address,
						value: item.formatted_address,
						latitude: item.geometry.location.lat(),
						longitude: item.geometry.location.lng(),
						locality: itemLocality
					}
				}));
			})
		},
		select: function(event, ui) {
			var startLat = ui.item.latitude;
			var startLng = ui.item.longitude;
			var destinationLat = jQuery('#destinationLat').text();
			var destinationLng = jQuery('#destinationLng').text();
			var storename = jQuery('#storename').text();;
			Venda.storeloc.showDirections(startLat, startLng, destinationLat, destinationLng , storename );
		}
	}).keypress(function(event) {
		if (jQuery('#startaddress').val().length > 0){
			try{ //fix andriod key enter
				var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
				if (keyCode === 13){
					setTimeout(function(){jQuery('div#startaddress_result .ui-autocomplete li:first-child a').trigger('mouseover').trigger('click');},1500);
				}
			} catch (e) {}
		}
	}).autocomplete("widget").addClass("ui-autocomplete-custom")

	/* End Custom Get Direction Autocomplete form  */
	jQuery('#startaddress').bind('paste', function() {
		setTimeout(function() {jQuery('#startaddress').trigger('keydown');}, 100);
	});

}

Venda.storeloc.findNearestShop = function() {
	if (navigator.geolocation){
		navigator.geolocation.getCurrentPosition(showPosition,showError);
	} else {
		errorMsg = "Geolocation is not supported by this browser.";
		console.log(errorMsg);
	}
}
function showError(err){
	console.log(err);

}
function showPosition(position) {
	var temptk = jQuery("#tkmaxxstore").is(':checked');
	var temphs = jQuery("#homesensestore").is(':checked');
	Venda.storeloc.mylat = position.coords.latitude;
	Venda.storeloc.mylng = position.coords.longitude;

	var redirectLink =
				jQuery('#tag-ebizurl').text().replace('http:',window.location.protocol)
				+ '/page/storelocator/'
				+ "?loc=*"
				+ "&lat=" + Venda.storeloc.mylat
				+ "&lng=" + Venda.storeloc.mylng
				+ "&active=0"
				+ "&tk=" + temptk
				+ "&hs=" + temphs;

	window.location.replace(redirectLink);
}

jQuery(document).on("click", "#findnearest" ,function(){
	Venda.storeloc.findNearestShop();
});
