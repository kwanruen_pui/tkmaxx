//Declare namespace for ebiz
Venda.namespace("Ebiz");

//Round down 'you save' to nearest pound
function recalSavingPrice(sellprice){
    var spricerel = parseInt(sellprice);
    return spricerel;
}

Venda.Ebiz.calculateSaving = function() {
    jQuery('.calculateSaving').each(function() {
        var msrpBox = jQuery(this).find('.product-pricemsrp');
        var saveBox = jQuery(this).find('.product-pricesaving');
        var savePrice = parseInt(jQuery(this).find('#parentsavemsrp').text());
        var savePercent = parseInt(jQuery(this).find('#parentsavemsrppercent').text());

        if (!isNaN(savePrice)) {
            saveBox.html('Save <span id="atrsaving">&pound;' + parseInt(savePrice) + '</span> (<span id="atrsavingpercent">' + parseInt(savePercent) + '%</span>)');
        }
        if (msrpBox.text()=="") {
            msrpBox.hide();
            saveBox.hide();
        }
    });

};

/*jQuery(function() {
    jQuery('.calculateSaving-<venda_invtref>').find('.saveprice').html('<venda_text id=site.save> <span id="atrsaving"><venda_currsym>' + recalSavingPrice('<venda_invtsavingonmsrp>') + '</span> (<span id="atrsavingpercent"><venda_invtsavingonmsrppercent>%</span>)');
    if(jQuery('.product-pricemsrp').text() == ""){
        jQuery('.product-pricemsrp, .product-pricesaving').hide();
    }
});*/

// show/hide for mobile
Venda.Ebiz.togglebox = function() {
  jQuery(document).on("click", ".js-togglebox", function(event) {

   if (Modernizr.mq('only all and (max-width: 767px)')) {
        var toggleClass = '.'+  jQuery(this).attr('data-toggle');
        event.preventDefault();
        if($(toggleClass).find('#passwordresetreveal').length == 1) {
            $('#passwordreset').removeClass('passwordresetdown');
        }
        if(jQuery(toggleClass).hasClass("hide-for-small")){
            jQuery(toggleClass).removeClass("hide-for-small");
            jQuery(toggleClass).css('display', 'none');
            jQuery(toggleClass).slideDown(150);
        }else {
            jQuery(toggleClass).slideUp(150, function(){
                 jQuery(toggleClass).addClass("hide-for-small");
            });
        }

        jQuery(this).toggleClass('active');

        if(jQuery(this).attr('data-toggle-icon')) {
            if(jQuery(this).hasClass('active')) {
                jQuery(this).find('i').removeClass(jQuery(this).attr('data-default-icon')).addClass(jQuery(this).attr('data-toggle-icon'));
            } else {
                jQuery(this).find('i').addClass(jQuery(this).attr('data-default-icon')).removeClass(jQuery(this).attr('data-toggle-icon'));
            }
        }

    }
    else { // duplicate of toggle box function for large screen
        var toggleClass = '.'+  jQuery(this).attr('data-toggle');

        if(jQuery(toggleClass).hasClass("hide")){
            jQuery(toggleClass).removeClass("hide");
            jQuery(toggleClass).css('display', 'none');
            jQuery(toggleClass).slideDown(150);
        }else {
            jQuery(toggleClass).slideUp(150, function(){
                 jQuery(toggleClass).addClass("hide");
            });
        }

        jQuery(this).toggleClass('active');

        if(jQuery(this).attr('data-toggle-icon')) {
            if(jQuery(this).hasClass('active')) {
                jQuery(this).find('i').removeClass(jQuery(this).attr('data-default-icon')).addClass(jQuery(this).attr('data-toggle-icon'));
            } else {
                jQuery(this).find('i').addClass(jQuery(this).attr('data-default-icon')).removeClass(jQuery(this).attr('data-toggle-icon'));
            }
        }
    }
  });
};

// toggle show/hide
Venda.Ebiz.slidetoggle = function() {
  jQuery(document).on("click tap", ".js-slidetoggle", function(event) {
    var toggleClass = '.'+  jQuery(this).attr('data-slidetoggle');
    jQuery(toggleClass).slideToggle('slow');
    jQuery(this).toggleClass('active');

    if(jQuery(this).attr('data-slidetoggle-icon')) {
        if(jQuery(this).hasClass('active')) {
            jQuery(this).find('i').removeClass(jQuery(this).attr('data-default-icon')).addClass(jQuery(this).attr('data-slidetoggle-icon'));
        } else {
            jQuery(this).find('i').addClass(jQuery(this).attr('data-default-icon')).removeClass(jQuery(this).attr('data-slidetoggle-icon'));
        }
    }
  });
};

Venda.Ebiz.setRefineListToggle = function () {

    var $refineBy = jQuery('#box-refinelist h2.js-slidetoggle');
    var $refineList = jQuery('.js-refinelist h3.js-slidetoggle');

    $refineList.removeClass('active');
    $refineBy.removeClass('active');

    if (Modernizr.mq('only all and (max-width: 997px)')) {
        $refineList.removeClass('active');
        $refineList.find('i').removeClass($refineList.attr('data-slidetoggle-icon'));
        $refineList.parent().next().hide();

        $refineBy.find('i').removeClass($refineBy.attr('data-slidetoggle-icon'));
        $refineBy.parents('#box-refinelist').next().hide();
    }
    if (Modernizr.mq('only all and (min-width: 998px)')) {
        $refineList.addClass('active');
        $refineList.find('i').addClass($refineList.attr('data-slidetoggle-icon'));
        $refineList.parent().next().show();

        $refineBy.parents('#box-refinelist').next().show();
    }
};

Venda.Ebiz.ExecuteDialogOpen = function() {
    if(jQuery('#vModal').find('form').length > 0) {
        if(jQuery('#emailmeback').length > 0){
            var attributeSku = Venda.Attributes.Get('atrsku');
            if(attributeSku != ""){
                document.emailmebackform.invtref.value = attributeSku;
            }
        }
        var  modalFormName = jQuery('#vModal').find('form').attr('name');
        jQuery('form[name='+modalFormName+']').find('input[name="layout"]').val('noheaders');
        Venda.Widget.HandleFormModal.submitForm(modalFormName);
    } else {
        document.form.submit();
    }
};

Venda.namespace("Ebiz.BKList");
Venda.Ebiz.BKList.jq = jQuery;
Venda.Ebiz.BKList.configBKList = {
    bklist : "",
    divArray: ['#sortby','.sort_results', '.searchpsel', '.pagn', '#refinelist','.prods li'],
        removeDivArray:['.categorytree'],
        enableBklist: true
};

Venda.Ebiz.BKList.init = function(settings) {
    for (var eachProp in settings) {
        this.configBKList[eachProp] = settings[eachProp];
    }
};

Venda.Ebiz.BKList.getUrl = function () {
    var curUrl = document.location.href;
    if (curUrl.indexOf("&amp;") != -1) {
        curUrl = curUrl.replace(/&amp;/gi, '&');
    }
    return Venda.Platform.getUrlParam(curUrl, "bklist");
}

Venda.Ebiz.BKList.ChangeLink = function(){
var divArray = Venda.Ebiz.BKList.configBKList.divArray;
var removeDivArray = Venda.Ebiz.BKList.configBKList.removeDivArray;
var bklist = Venda.Ebiz.BKList.configBKList.bklist || Venda.Ebiz.BKList.getUrl() || "";
var strBklist = "&bklist";
    if (bklist != "") {
        var addBklist =  strBklist + "=" + bklist;
        for (var i = 0; i < divArray.length; i++) {

            if(Venda.Ebiz.BKList.jq(divArray[i]+ " a").attr("href")){
                Venda.Ebiz.BKList.jq(divArray[i]+ " a").attr("href", function() {
                    return Venda.Ebiz.BKList.jq(this).attr("href") + addBklist;
                });
            }

            if(Venda.Ebiz.BKList.jq(divArray[i] + " option").attr("value")){
                Venda.Ebiz.BKList.jq(divArray[i] + " option").attr("value", function() {
                    return Venda.Ebiz.BKList.jq(this).attr("value") + addBklist;
                });
            }
        }

        for (var i = 0; i < removeDivArray.length; i++) {
            Venda.Ebiz.BKList.jq(removeDivArray[i]+" a").attr("href", function() {
                var currentLink = Venda.Ebiz.BKList.jq(this).attr("href");
                var newLink = currentLink;
                if (newLink.length > 2) {
                    if (currentLink.indexOf("&bklist=") != -1) {
                        newLink = currentLink.substring(0, currentLink.indexOf("&bklist="));
                    }
                    if (newLink.indexOf("&view=") != -1) {
                        newLink = newLink.substring(0, newLink.indexOf("&view="));
                    }
                }
                return newLink;
            });
        }
        if(Venda.Ebiz.BKList.jq("#tag-pageSearchurlFull")){
            var newUrl = Venda.Ebiz.BKList.jq("#tag-pageSearchurlFull").text()+ addBklist ;
            Venda.Ebiz.BKList.jq("#tag-pageSearchurlFull").text(newUrl);
        }
        if(Venda.Ebiz.BKList.jq("#tag-pageSearchurl")){
            var newUrl = Venda.Ebiz.BKList.jq("#tag-pageSearchurl").text()+ addBklist ;
            Venda.Ebiz.BKList.jq("#tag-pageSearchurl").text(newUrl);
        }
        if(Venda.Ebiz.BKList.jq("#tag-pageIcaturl")){
            var newUrl = Venda.Ebiz.BKList.jq("#tag-pageIcaturl").text()+ addBklist ;
            Venda.Ebiz.BKList.jq("#tag-pageIcaturl").text(newUrl);
        }
    }

}

Venda.Ebiz.CookieJar = new CookieJar({
    expires : 3600 * 24 * 7,
    path : '/'
});

jQuery.fn.popupIframe = function () {
    if (jQuery.browser.msie && jQuery.browser.version < "7.0") {
        var src = 'javascript:false;';
        html = '<iframe class="js-popup-iframe" src="' + src + '" style="-moz-opacity: .10;filter: alpha(opacity=1);height:expression(this.parentNode.offsetHeight+\'px\');width:expression(this.parentNode.offsetWidth+\'px\');' + '"></iframe>';
        if (jQuery(this).find('.js-popup-iframe').length == 0) {
            this.prepend(html);
        }
    }
};

/**
 * simple popup
 */
Venda.Ebiz.doProtocal = function (url) {
    var protocal = document.location.protocol;
    if (url.indexOf("http:") == 0 && protocal == "https:") {
        url = url.replace("http:", "https:");
    }
    return url;
};

/**
 * Remove any special characterSet
 * @param {string} str - string with any special characters
 * @return {string} str - string WITHOUT any special characters
 */
Venda.Ebiz.clearText = function (str) {
    var iChars = /\$| |,|@|#|~|`|\%|\*|\^|\&|\(|\)|\+|\=|\/|\[|\-|\_|\]|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
    return str.replace(iChars, "");
};

/**
 *  addEvent script from http://www.accessify.com/features/tutorials/the-perfect-popup/
 *  This function is moved from 'sitewide.js', so avoid the error.
 */
function addEvent(elm, evType, fn, useCapture) {
    if (elm.addEventListener) {
        elm.addEventListener(evType, fn, useCapture);
        return true;
    } else if (elm.attachEvent) {
        var r = elm.attachEvent('on' + evType, fn);
        return r;
    } else {
        elm['on' + evType] = fn;
    }
};

/**
* simple color swatch on productlist/searchresult
* @requires jQuery v1.7.1 or greater
* @param {object} - configutation
*   - contentID: search content ID
*/
Venda.namespace("Ebiz.colorSwatch");
Venda.Ebiz.colorSwatch = function(conf){
    var defaults = {
        contentID: '#content-search'
    };
    this.conf=jQuery.extend(defaults, conf);
};

Venda.Ebiz.colorSwatch.prototype = {
    init: function(){
            jQuery(this.conf.contentID).on("click", ".js-swatchContainer a", function(){

            var $this = jQuery(this);

            var mainImg      = $this.data("setimage"),
                mainImgObj   = jQuery("#"+ $this.data("prodid") ),
                mediumImg    = $this.data("setimagemedium"),
                detailsObj   = jQuery("#"+ $this.data("detailsid") ),
                prodLink     = mainImgObj.find("a:first").data("prodLink");

            if(mainImg == ""){
                mainImg = mainImgObj.find("a:first").data("defaultImage");
            }

            mainImgObj.find('img:first')
                .attr("src", mainImg)
                .end()
                .find("a:first")
                .attr("href", this.href || "");

            $this.parent("").find("a").removeClass("js-sw-selected");
            $this.addClass("js-sw-selected");
            detailsObj.find(".js-imgSource").html(mediumImg); //reset image source value to corresponding with colour swatch selected
            return false;

        });
    }
};

/**
* prevent missing euro sign if use ajax on IE
* @requires jQuery v1.5 or greater
*/
jQuery(document).ajaxComplete(function() {
    if(jQuery.browser.msie){
        jQuery('#term .js-refine-list, .js-prod-price, #pricerangevalues, #updateTotal').html(function(idx, price) {
            return price.replace(/\u0080([\d.]+)/g, "\u20ac$1");
        });
    }
});

/**
* remove product from quick shop page
* @param {string} the sku removed
*/
Venda.Ebiz.qshopRemove = function(invtref){
    var curUrl = document.location.href;
    var newUrl = curUrl.replace(invtref,'');
    var dialogOpts = {modal: true,autoOpen: false,width:'320px'};
    jQuery("#waitMesg").dialog(dialogOpts);
    jQuery("#waitMesg").dialog("open");
    document.location.href = newUrl;
};

Venda.Ebiz.customSelect = function(){
    if (!jQuery('html').hasClass('lt-ie8')) {
        if(jQuery('.js-custom select').length > 0) {
            jQuery('.js-custom select').each(function(){
                if(jQuery(this).next('.js-select').length == 0){
                    var title = jQuery(this).find('option:first').text() || '';
                    if( jQuery('option:selected', this).val() != ''  ) {
                        title = jQuery('option:selected',this).text();
                    }
                    jQuery(this).css({'z-index':10,'opacity':0,'-khtml-appearance':'none'}).after('<span class="js-select"><span class="js-selected">' + title + '</span><i class="icon-down-marker icon-large marg-side right"></i></span>');

                    jQuery(this).change(function(){
                        val = jQuery('option:selected',this).text();
                        jQuery(this).next().find('.js-selected').text(val);
                    });
                }
                else {
                    if( jQuery('option:selected', this).val() != ''  ) {
                        jQuery(this).next().find('.js-selected').text(jQuery('option:selected',this).text());
                    }
                }
            });
        }
    }
};

/**
* http://n33.co/2013/03/23/browser-on-jquery-19x-for-legacy-ie-detection
* The following snippet will bring back $.browser.msie and $.browser.version on jQuery 1.9.x
**/
jQuery.browser={};
(function(){
    jQuery.browser.msie=false;
    jQuery.browser.version=0;
    if(navigator.userAgent.match(/MSIE ([0-9]+)\./)){
        jQuery.browser.msie=true;
        jQuery.browser.version=RegExp.$1;
    }
})();

jQuery(window).on('load resize', function(e) {
    if(!jQuery('html').hasClass('lt-ie9')) {
        if (Modernizr.mq('only all and (max-width: 767px)')) {
            // turn modal off if you are on a small device
            jQuery('.js-doDialog').each(function() {
                jQuery(this).attr('target','_blank');
            });
        }
    }
    if(e.type === 'load') {
        //for fixing next/previous button of orbit home banner
        if((jQuery('.orbit-container').length > 0) && (jQuery('html').hasClass('lt-ie9'))) {
            jQuery('.orbit-prev, .orbit-next').append('<span></span>');
        }
        if (jQuery('.content-search-body .js-refinelist').length > 0 ) {
            Venda.Ebiz.setRefineListToggle();
        }
    }

    // set height for Inspiration banner
    if(typeof Venda.Platform.EqualHeight != "undefined") {
        var classtoset1 = new Array ('.image-column');
        if ((jQuery('.homepage-inspiration').length > 0 && Modernizr.mq('only all and (min-width: 998px)')) || (Modernizr.mq('only all and (max-width: 997px) and (min-width: 767px)') && jQuery('.cat-inspiration').length > 0)) {
            jQuery('.image-column').css('min-height','');
            Venda.Platform.EqualHeight.init(classtoset1);
        } else {
            jQuery('.image-column').css('min-height','');
        }
    }
});

/**
 * Set up the DOM events.
 */
jQuery(function() {

    if (jQuery('#paymentdetails').length > 0 ) {
        jQuery('#startmonth option:first, #month option:first').text('Month');
        jQuery('#startyear option:first, #year option:first').text('Year');
    }

    jQuery(document).bind('quickview-loaded', function() {
        Venda.Attributes.Initialize();
    });

    /**
     * Media Code
     * Hide noscript comment
     * Add listeners to media code form elements
     * This class not used on any templates/page/css
     */
    jQuery(".nonjs").css("display", "none");

    //view itemperpage/ sort by  dropdown changed a duplicate id
    if (jQuery("#pagnBtm")) {
        if (jQuery("#pagnBtm .js-pagnPerpage label")) {
            //view itemperpage
            jQuery("#pagnBtm .js-pagnPerpage label").attr('for', 'perpagedpdBT');
            jQuery("#pagnBtm .js-pagnPerpage select").attr({
                id : 'perpagedpdBT',
                name : 'perpagedpdBT'
            });
            //sort by
            jQuery("#pagnBtm .js-sort label").attr('for', 'sortbyBT');
            jQuery("#pagnBtm .js-sort select").attr({
                id : 'sortbyBT',
                name : 'sortbyBT'
            });
        }
    }

    // Close alert boxes sitewide
    jQuery(document).on("click", ".alert-box a.close", function(event) {
        event.preventDefault();
        jQuery(this).closest(".alert-box").fadeOut(function(event){
            jQuery(this).remove();
        });
    });

    // show/hide nav for tablet and under
    jQuery(document).on("click", "#nav-browse,#show-small-nav", function(event) {
        event.preventDefault();
        jQuery("#mm_ul").slideToggle("slow");
        jQuery("#nav-browse").toggleClass("js-nav-browse-active");
    });
    jQuery('#nav-browse').on('keydown',function(event){
        if(event.keyCode == '9') {
            if(jQuery("#mm_ul").is(':hidden')){
            jQuery('#nav-browse').trigger('click');
        }
        }
    });

    // show/hide search for mobile
    jQuery(document).on("click", "#show-small-search", function(event) {
        jQuery("#header-row-two").toggleClass("js-search-active");
    });

    if (jQuery('.js-togglebox').length > 0 ) {
        Venda.Ebiz.togglebox();
    }

    if (jQuery('.js-slidetoggle').length > 0 ) {
        Venda.Ebiz.slidetoggle();
    }



    //workaround solution: "Add to Basket" button should be disabled when product is out of stock
    if (jQuery('.quickshop').length > 0) {
        if (jQuery('.js-oneProduct').length == jQuery('.js-prodoutofstock').length) {
            jQuery('.js-addproduct').attr('disabled', 'true');
            jQuery('.js-addproduct').css('opacity','0.5');
        }
    }

    // customize Flash Events
 
    if(jQuery("#privatesale").length > 0) {
        var usertype = jQuery("#tag-ustype").text();
    /*     if(usertype != "R"){
            $(".ps-img img").attr("src", jQuery("#tag-serveradd").text() + "/content/ebiz/" + jQuery("#tag-bsref").text() + "/resources/images/flashevents-signin.png")
        }*/
        $(".ps-img img").show();
    }
    

    //the hidden slider will display if there is 1 or more items
    if (jQuery('.product-slider').length > 0) {
        jQuery('.product-slider').each(function() {
            if (jQuery(this).find('ul li').length > 0) {
                jQuery(this).parent().show();
            }
        });
    }

    Venda.Ebiz.customSelect();
});

Venda.Ebiz.loadAfterVBM = function() {
    Venda.jqSlider.sliderObj = jQuery('.product-slider .js-slider');

    if(jQuery('html').hasClass('lt-ie9')) {
        Venda.jqSlider.sliderObj.jqSlider({isTouch: Modernizr.touch});
    }else {
        if(Modernizr.mq('only screen and (max-width: 767px)') ){
            Venda.jqSlider.sliderObj.addClass('js-small');
        }else {
            Venda.jqSlider.sliderObj.removeClass('js-small');
        }
        Venda.jqSlider.loadSlider();
    }

    jQuery('.product-slider').each(function() {
        if (jQuery(this).find('ul li').length > 0) {
            jQuery(this).parent().show();
        }
    });
};

/**
* Get total reviews and stars
*/
Venda.Ebiz.showSumReview = function(){
    var sku = jQuery("#tag-invtref").text();
    var revUrl = jQuery("#tag-ebizurl").html()+'/scat/'+sku;
    var reviewtotal = 0;
    var reviewstars = '';
    var avgstars = 0;
    var total_star = 0;
    //get it via ajax
    jQuery.ajax({
        url: revUrl,
        }).done(function(result) {
          // show total reviews
        reviewtotal = jQuery(result).find(".review-element li").length;
        var suffic = "Review";
        if(reviewtotal > 1)
            suffic += "s";
        jQuery("#readreview_link").html("( Read " + reviewtotal + " "+suffic+")");

        // show review stars
        jQuery(result).find(".star-count").each(function(){
            total_star += parseInt($(this).text());
        });
        avgstars = Math.ceil(total_star/reviewtotal);
        //show star image
        jQuery("#reviewStar span").html("<img src="+jQuery("#tag-ebizurl").text()+"/content/ebiz/"+jQuery("#tag-ebizref").text()+"/resources/images/rating"+avgstars+".gif>");

    });
};

// JS from previous site
// Display switching between delivery address and store address when the delivery option is Click & Collect
Venda.Ebiz.refreshAddress = function() {
    var storeSelected = $('#storename').val();
    var storeAddress = jQuery("div.storeAddressBox#"+storeSelected).html();
    if (storeSelected == "" || storeSelected == null) {
        jQuery(".shipping-address .js-ehbox-storeaddress").hide();
        jQuery(".shipping-address .js-ehbox, .shipping-address .button").show();
    }
    else {
        jQuery(".shipping-address .js-ehbox-storeaddress").html(storeAddress);
        jQuery(".shipping-address .js-ehbox-storeaddress").show();
        jQuery(".shipping-address .js-ehbox, .shipping-address .button, .shipping-address .js-ehbox-storeaddress .link").hide();


        var billingBoxHeight = $('.js-billingaddress .box-body').innerHeight();
        var shippingBoxHeight = $('.shipping-address .box-body').innerHeight();
        var addressBoxHeight = (billingBoxHeight > shippingBoxHeight) ? billingBoxHeight : shippingBoxHeight;
        $('.js-billingaddress .box-body, .shipping-address .box-body').innerHeight(addressBoxHeight);
    }
};


jQuery(function() {
    //3 lines product detail tab
    if ($('.productdetail').length) {
        $('.productdetail #infotab .descriptionTab .tab-invtdesc1').expander({
            slicePoint: 250,
            expandSpeed: 0,
            collapseSpeed: 0,
            expandEffect: 'show',
            collapseEffect: 'hide'
        });
    }

    //show reviews;
    if(jQuery("#addproductform #readreview_link").length){
        Venda.Ebiz.showSumReview();
    }

    if(jQuery('form[name="ordersummaryform-giftcert"]').length){
        var $giftcert = jQuery('form[name="ordersummaryform-giftcert"] input[type="text"]');
        var defaultText = $giftcert.attr('placeholder');
        jQuery(window).on('ready resize', function(e) {
            if (Modernizr.mq('only all and (max-width: 767px)') && (!$giftcert.hasClass('small'))) {
                $giftcert.attr('placeholder', $giftcert.data('placeholder'));
                $giftcert.addClass('small');
            }
            else if(Modernizr.mq('only all and (min-width: 768px)') && ($giftcert.hasClass('small'))) {
                $giftcert.attr('placeholder', defaultText);
                $giftcert.removeClass('small');
            }
        });
    }

    //calculate and write saving price
    Venda.Ebiz.calculateSaving();

    if( $('.js-shopcart').length > 0) {

        var rrpTot = 0;
        var saveTot = 0;
        //Extract RRP and saving from each basket line item
        $.each( $('.js-product-pricesaving'), function() {
            var rrp = parseFloat( $(this).data('rrp') );
            var save = parseFloat( $(this).data('price') );
            var qty = parseInt( $(this).data('qty') );
            rrpTot = rrpTot+rrp;
            saveTot = saveTot + (save * qty);
        });

        //Test if number is a float or integer
        function is_int(value){
          if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
              return true;
          } else {
              return false;
          }
        }

        //Update the banner vlaues
        if (is_int(rrpTot)) {$('#totalRRP').text(rrpTot);} else {$('#totalRRP').text( rrpTot.toFixed(2) );}
        if (is_int(saveTot)) {$('#tkTotSaving').text(saveTot);} else {$('#tkTotSaving').text( saveTot.toFixed(2) );}


        //Only show the banner if not a mixed basket and all items have RRP savings.
        //If number of rows equals the number of rrp prices in the basket.
        if( $('.js-shopcart .js-product-pricesaving').length == $('.js-shopcart .js-prod-rrp').length ) {$('.js-basket-promo').show();}

        if( $('.spent .less').length > 0) {
            var less =  50 -  jQuery('.spent .less').data('extsub');
           jQuery('.spent .less').find('span').text(less.toFixed(2));
           jQuery('#free_delivery_threshold').val(less.toFixed(2));
        }


        var jewelleryIteminBasket = false;
        //See if there are any jewellery product types in the basket
        $.each( $('.js-product-type'), function() {
            if ( $(this).html() == '76' ) {
                jewelleryIteminBasket = true;
                return false;
            }
        });
        //Show the banner if there is a jewellery product type in the basket.
        if( jewelleryIteminBasket === true ) {$('.jewellery-basket-promo').show();}

        Venda.Ebiz.createQty();

    }
    if($('.js-prod-rrp span').length == 0){ jQuery('.js-rrp').css('visibility', 'hidden'); }

    /* Gift Certificate */
    jQuery('#giftcertificatesform').find('#comment').on('keyup', function(){
        var _charUse = jQuery(this).val().length;
        if(_charUse > 250){
            var value = jQuery(this).val();
            jQuery(this).val(value.substr(0,250));
        }

        jQuery('.js-textMsgCount-' + jQuery('#comment').data('textmsg')).html((250 - _charUse));
    });

    //Remove the Please Select from Country List by client request
    $('#cntrylist option[value=""]').remove();

    // Click & Collect
    // Put store information from STXT into ORXT
    $("#ordersummary #buttons").on('click', function() {
        var storeSelected = $("#storename").val();
        if(storeSelected != "" || storeSelected != null){
            var storename = $("div.storeAddressBox#"+storeSelected+" .name").text();
            var storenum = $("div.storeAddressBox#"+storeSelected+" .num").text();
            var storeaddr1 = $("div.storeAddressBox#"+storeSelected+" .addr1").text();
            var storeaddr2 = $("div.storeAddressBox#"+storeSelected+" .addr2").text();
            var storeaddr3 = $("div.storeAddressBox#"+storeSelected+" .addr3").text();
            var storecounty = $("div.storeAddressBox#"+storeSelected+" .county").text();
            var storepcode = $("div.storeAddressBox#"+storeSelected+" .pcode").text();
            var storecountry = $("div.storeAddressBox#"+storeSelected+" .country").text();
            var storephone = $("div.storeAddressBox#"+storeSelected+" .phone").text();
            var custtitle = $(".storeInfo select[name=orxorxtcustitle]").val();
            var custfname = $(".storeInfo input[name=orxorxtcusfname]").val();
            var custlname = $(".storeInfo input[name=orxorxtcuslname]").val();
            var custphone = $(".storeInfo input[name=orxorxtcusphone]").val();

            $('input[name=orxorxtccstname]').val(storename);
            $('input[name=orxorxtccstnum]').val(storenum);
            $('input[name=orxorxtccstadd1]').val(storeaddr1);
            $('input[name=orxorxtccstadd2]').val(storeaddr2);
            $('input[name=orxorxtccstadd3]').val(storeaddr3);
            $('input[name=orxorxtccstcounty]').val(storecounty);
            $('input[name=orxorxtccstpcode]').val(storepcode);
            $('input[name=orxorxtccstcountry]').val(storecountry);
            $('input[name=orxorxtccstphone]').val(storephone);
            $('form[name=ordersummaryform] input[name=orxorxtcustitle]').val(custtitle);
            $('form[name=ordersummaryform] input[name=orxorxtcusfname]').val(custfname);
            $('form[name=ordersummaryform] input[name=orxorxtcuslname]').val(custlname);
            $('form[name=ordersummaryform] input[name=orxorxtcusphone]').val(custphone);
            $('form[name=storeInfo] input[type=submit]').trigger('click');
        }
    });

    $('#storename').on('change', function() {
        var storeSelected = $(this).val();
        $('.storecollectdetail .storeAddressBox').slideUp();
        if ((storeSelected != "") && (storeSelected != null)) {
            $('.storecollectdetail .storeInfo, .storecollectdetail #' + storeSelected).slideDown();
        } else {
            $('.storecollectdetail .storeInfo').slideUp();
        }
        Venda.Ebiz.refreshAddress();
    });


});

Venda.Ebiz.createQty = function () {
var $single, invtQty = 0;
$('input[name="oioqty"]').each(function(){
    if($(this).val() > invtQty){
        invtQty = parseInt($(this).val());
    }
});
if(invtQty > 9) {
  jQuery('.js-shopcart .js-update').each(function(){
    $single = jQuery(this);
    $single.empty();
    for(var i = 1, j = invtQty ; i <= j; i++){
      $single.append('<option value="'+i+'">'+i+'</option>');
    };
    $single.find('option[value="'+$single.data('qty')+'"]').prop('selected', true);
  });
}
};